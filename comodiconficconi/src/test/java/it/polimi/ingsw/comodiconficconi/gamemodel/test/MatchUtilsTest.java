package it.polimi.ingsw.comodiconficconi.gamemodel.test;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.MatchUtils;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.Deck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.ItemDeck;

public class MatchUtilsTest {
	
	@Test
	public void newSectorDeck(){
		assertFalse(MatchUtils.getNewSectorDeck().isEmpty());
	}
	
	@Test
	public void itemDeckShuffle(){
		Deck graveyard = new ItemDeck(true);
		Deck item = new ItemDeck(true);
		
		Card itemCard = FactoryCard.getCard(ConstantCards.ADRENALINECODE);
		
		graveyard.addCard(itemCard);
		MatchUtils.shuffleItemDeck(item, graveyard);
		assertTrue(item.draw().toString().equals(itemCard.toString()));
		assertTrue(graveyard.isEmpty());
	}
}
