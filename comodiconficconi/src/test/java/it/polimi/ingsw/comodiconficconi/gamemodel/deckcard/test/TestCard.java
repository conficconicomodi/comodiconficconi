package it.polimi.ingsw.comodiconficconi.gamemodel.deckcard.test;

import static org.junit.Assert.*;
import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.CardCodeException;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.HatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.ItemCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.SectorCard;

import org.junit.Test;

public class TestCard {
	@Test
	public void testItemCreation() {
		for (int i = 10; i < 16; i++) {
			Card c;
			try {
				c = FactoryCard.getCard(i);
			} catch (CardCodeException e) {
				break;
			}
			assertNotNull(c);
			assertTrue(c instanceof ItemCard);
		}
	}

	@Test(expected = CardCodeException.class)
	public void testItemCreationFailure() throws CardCodeException {
		for (int i = 16; i < 20; i++) {
			Card c = FactoryCard.getCard(i);
			assertNull(c);
		}
	}

	@Test
	public void testSectorCreation() {
		for (int i = 20; i < 25; i++) {
			try {
				Card c = FactoryCard.getCard(i);
				assertNotNull(c);
				assertTrue(c instanceof SectorCard);
			} catch (CardCodeException e) {
				break;
			}
		}
	}

	@Test(expected = CardCodeException.class)
	public void testSectorCreationFailure() throws CardCodeException {
		for (int i = 25; i < 30; i++) {
			Card c = FactoryCard.getCard(i);
			assertNull(c);
		}
	}

	@Test
	public void testCharacterCreation() throws CardCodeException {
		for (int i = 30; i < 32; i++) {
			Card c = FactoryCard.getCard(i);
			assertNotNull(c);
		}

	}

	@Test(expected = CardCodeException.class)
	public void testCharacterCreationFailure() throws CardCodeException {
		for (int i = 32; i < 40; i++) {
			Card c = FactoryCard.getCard(i);
			assertNull(c);
		}

	}

	@Test
	public void testHatchCreation() throws CardCodeException {
		for (int i = 40; i < 42; i++) {
			Card c = FactoryCard.getCard(i);
			assertNotNull(c);
			assertTrue(c instanceof HatchCard);
		}
	}

	@Test(expected = CardCodeException.class)
	public void testHatchCreationFailure() throws CardCodeException {
		for (int i = 42; i < 50; i++) {
			Card c = FactoryCard.getCard(i);
			assertNull(c);
		}
	}

	@Test
	public void testBlockedHatchString() throws CardCodeException {
		assertTrue(ConstantCards.BLOCKEDHATCH.equals(FactoryCard.getCard(
				ConstantCards.BLOCKEDHATCHCODE).toString()));
	}

	@Test
	public void testOpenHatchString() throws CardCodeException {
		assertTrue(ConstantCards.OPENHATCH.equals(FactoryCard.getCard(
				ConstantCards.OPENHATCHCODE).toString()));
	}

	@Test
	public void testAdrenalineString() throws CardCodeException {
		assertTrue(ConstantCards.ADRENALINE.equals(FactoryCard.getCard(
				ConstantCards.ADRENALINECODE).toString()));
	}

	@Test
	public void testAttackString() throws CardCodeException {
		assertTrue(ConstantCards.ATTACK.equals(FactoryCard.getCard(
				ConstantCards.ATTACKCODE).toString()));
	}

	@Test
	public void testDefenseString() throws CardCodeException {
		assertTrue(ConstantCards.DEFENSE.equals(FactoryCard.getCard(
				ConstantCards.DEFENSECODE).toString()));
	}

	@Test
	public void testNoiseAnywhereString() throws CardCodeException {
		assertTrue(ConstantCards.NOISEANYWHERE.equals(FactoryCard.getCard(
				ConstantCards.NOISEANYWHERECODE).toString()));
	}

	@Test
	public void testNoiseAnywhereitemString() throws CardCodeException {
		assertTrue(ConstantCards.NOISEANYWHERE.equals(FactoryCard.getCard(
				ConstantCards.NOISEANYWHEREWITHITEMCODE).toString()));
	}

	@Test
	public void testNoiseinSectorString() throws CardCodeException {
		assertTrue(ConstantCards.NOISEINSECTOR.equals(FactoryCard.getCard(
				ConstantCards.NOISEINSECTORCODE).toString()));
	}

	@Test
	public void testNoiseinSectorWithItemString() throws CardCodeException {
		assertTrue(ConstantCards.NOISEINSECTOR.equals(FactoryCard.getCard(
				ConstantCards.NOISEINSECTORWITHITEMCODE).toString()));
	}

	@Test
	public void testSedativeString() throws CardCodeException {
		assertTrue(ConstantCards.SEDATIVE.equals(FactoryCard.getCard(
				ConstantCards.SEDATIVECODE).toString()));
	}

	@Test
	public void testSilenceString() throws CardCodeException {
		assertTrue(ConstantCards.SILENCE.equals(FactoryCard.getCard(
				ConstantCards.SILENCECODE).toString()));
	}

	@Test
	public void testSpotlightString() throws CardCodeException {

		assertTrue(ConstantCards.SPOTLIGHT.equals(FactoryCard.getCard(
				ConstantCards.SPOTLIGHTCODE).toString()));
	}

	@Test
	public void testTeleportString() throws CardCodeException {
		assertTrue(ConstantCards.TELEPORT.equals(FactoryCard.getCard(
				ConstantCards.TELEPORTCODE).toString()));
	}
}
