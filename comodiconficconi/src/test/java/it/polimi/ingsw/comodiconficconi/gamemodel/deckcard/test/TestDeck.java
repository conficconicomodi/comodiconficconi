package it.polimi.ingsw.comodiconficconi.gamemodel.deckcard.test;

import static org.junit.Assert.*;
import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.CharacterCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.HatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.ItemCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.SectorCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.CharacterDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.Deck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.HatchDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.ItemDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.SectorDeck;

import org.junit.Test;

public class TestDeck {
	
	
	//ITEM DECK
	@Test
	public void testDrawingItemCorrectly() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertNotNull(c);
	}

	@Test
	public void testDrawingItem() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertTrue(c instanceof ItemCard);
	}

	@Test
	public void testDrawingItemFailure() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertFalse(c instanceof HatchCard);
	}

	@Test
	public void testDrawingFinishItem() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		for (int io = 0; io < ConstantCards.MAXITEMCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(d.isEmpty());
	}
	
	@Test
	public void testDrawingItemGraveyard() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		for (int io = 0; io < 12; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(!i.isEmpty());
	}

	@Test
	public void testDrawingItemGraveyardCardIn() {
		Deck d = new ItemDeck(false);
		Deck i = new ItemDeck(true);
		for (int io = 0; io < 12; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertNotNull(i.draw());
	}
	
	//HATCH DECK
	@Test
	public void testDrawingHatchCorrectly() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertNotNull(c);
	}

	@Test
	public void testDrawingHatch() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertTrue(c instanceof HatchCard);
	}

	@Test
	public void testDrawingHatchFailure() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertFalse(c instanceof ItemCard);
	}
	
	@Test
	public void testDrawingHatchFinish() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		for (int io = 0; io < ConstantCards.MAXHATCCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(d.isEmpty());
	}
	
	
	@Test
	public void testDrawingHatchGraveyard() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		for (int io = 0; io < ConstantCards.MAXHATCCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(!i.isEmpty());
	}

	
	@Test
	public void testDrawingHatchGraveyardCardIn() {
		Deck d = new HatchDeck(false);
		Deck i = new HatchDeck(true);
		for (int io = 0; io < ConstantCards.MAXHATCCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertNotNull(i.draw());
	}
	
	//SECTOR DECK

	@Test
	public void testDrawingSectorCorrectly() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertNotNull(c);
	}

	@Test
	public void testDrawingSector() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertTrue(c instanceof SectorCard);
	}

	@Test
	public void testDrawingSectorFailure() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		Card c = d.draw();
		i.addCard(c);
		assertFalse(c instanceof CharacterCard);
	}

	@Test
	public void testDrawingSectorFinish() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		for (int io = 0; io < ConstantCards.MAXSECTORCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(d.isEmpty());
	}
	
	@Test
	public void testDrawingSectorGraveyard() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		for (int io = 0; io < ConstantCards.MAXSECTORCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(!i.isEmpty());
	}
	@Test
	public void testDrawingSectorGraveyardCardIn() {
		Deck d = new SectorDeck(false);
		Deck i = new SectorDeck(true);
		for (int io = 0; io < ConstantCards.MAXSECTORCARD; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertNotNull(i.draw());
	}
	
	//CHARACTER DECK
	@Test
	public void testDrawingCharacterCorrectly() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		Card c = d.draw();
		i.addCard(c);
		assertNotNull(c);
	}

	@Test
	public void testDrawingCharacter() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		Card c = d.draw();
		i.addCard(c);
		assertTrue(c instanceof CharacterCard);
	}

	@Test
	public void testDrawingCharacterFailure() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		Card c = d.draw();
		i.addCard(c);
		assertFalse(c instanceof ItemCard);
	}
	
	@Test
	public void testDrawingCharacterFinish() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		for (int io = 0; io < 4; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(d.isEmpty());
	}
	
	@Test
	public void testDrawingCharacterGraveyard() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		for (int io = 0; io < 4; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertTrue(!i.isEmpty());
	}
	@Test
	public void testDrawingCharacterGraveyardCardIn() {
		Deck d = new CharacterDeck(4, false);
		Deck i = new CharacterDeck(4, true);
		for (int io = 0; io < 4; io++) {
			Card c = d.draw();
			i.addCard(c);
		}
		assertNotNull(i.draw());
	}
}
