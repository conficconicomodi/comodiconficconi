package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;
import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;

public class PlayerTest {

	private Player human;
	private Player alien;

	@Before
	public void setUp() {
		human = PlayerFactory.getPlayer(ConstantPlayer.HUMAN, 3);
		alien = PlayerFactory.getPlayer(ConstantPlayer.ALIEN, 4);
	}

	@Test
	public void failCreatingPlayer() {
		Player player = PlayerFactory.getPlayer("wrong player", 6);
		assertNull(player);
	}

	@Test
	public void correctPlayerId() {
		assertTrue(human.getPlayerId() == 3);
		assertTrue(alien.getPlayerId() == 4);
	}

	@Test
	public void addItems() {
		Card item = FactoryCard.getCard(ConstantCards.ADRENALINECODE);

		human.addItem(item);
		assertTrue(human.hasThisCard(item));
	}

	@Test
	public void noItems() {
		assertFalse(human.hasThisCard(FactoryCard
				.getCard(ConstantCards.SPOTLIGHTCODE)));
	}

	@Test
	public void removeItems() {
		Card item = FactoryCard.getCard(ConstantCards.ADRENALINECODE);

		alien.addItem(item);
		assertTrue(alien.hasThisCard(item));
		alien.removeItem(item);
		assertTrue(alien.getAllCards().isEmpty());
	}

	@Test
	public void fullItems() {
		while (!human.fullItems())
			human.addItem(FactoryCard.getCard(ConstantCards.ADRENALINECODE));

		assertTrue(human.getAllCards().size() == 3);
	}

	@Test
	public void defenseCard() {
		assertNull(human.getDefenseCard());
		
		human.addItem(FactoryCard.getCard(ConstantCards.DEFENSECODE));

		assertTrue(human.hasDefense());

		assertTrue(human
				.getDefenseCard()
				.toString()
				.equals(FactoryCard.getCard(ConstantCards.DEFENSECODE)
						.toString()));
	}

	@Test
	public void pastMoves() {
		human.addMove(new Coordinates(6, 6));

		assertTrue(!human.getPastMoves().isEmpty());
	}

	@Test
	public void resetPlayer() {
		Player testHumanPlayer = PlayerFactory.getPlayer(ConstantPlayer.HUMAN, 6);
		Player testAlienPlayer = PlayerFactory.getPlayer(ConstantPlayer.ALIEN, 8);
		human.setSteps(5);
		alien.setSteps(7);
		assertFalse(human.getSteps() == testHumanPlayer.getSteps());
		assertFalse(alien.getSteps() == testAlienPlayer.getSteps());

		human.resetPlayer();
		alien.resetPlayer();
		assertTrue(human.getSteps() == testHumanPlayer.getSteps());
		assertFalse(alien.getSteps() == testAlienPlayer.getSteps());
		
		alien.getFSM().get(ConstantFSM.END_TURN).put(ConstantFSM.ATTACK, ConstantFSM.END_TURN);
		assertFalse(alien.getFSM().equals(testAlienPlayer.getFSM()));
		alien.resetPlayer();
		assertTrue(alien.getFSM().equals(testAlienPlayer.getFSM()));
	}

	@Test
	public void playerFSM() {
		assertNotNull(human.getFSM());

		assertFalse(human.getFSM().equals(alien.getFSM()));
	}

	@Test
	public void playerState() {
		int previousState = alien.getState();
		assertTrue(previousState == ConstantFSM.BEFORE_MOVE);

		alien.setState(ConstantFSM.END_TURN);
		assertFalse(previousState == alien.getState());
	}

	@Test
	public void isHuman() {
		assertTrue(human.isHuman());
		assertFalse(alien.isHuman());
	}

	@Test
	public void killPlayer() {
		human.setDead();
		assertTrue(human.isDead());
	}
}
