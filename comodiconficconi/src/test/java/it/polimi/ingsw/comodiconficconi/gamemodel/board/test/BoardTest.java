package it.polimi.ingsw.comodiconficconi.gamemodel.board.test;

import static org.junit.Assert.*;

import java.util.Set;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Board;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.SectorFactory;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.PlayerFactory;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {
	
	private Board board;
	private Player human;
	private Player alien;
	
	@Before
	public void setUp(){
		board = new Board("galilei");	
		human = PlayerFactory.getPlayer("human", 2);
		alien = PlayerFactory.getPlayer("alien", 5);
	}
	
	@Test
	public void boardNotEmpty() {	
		assertTrue(!board.getBoard().isEmpty());
	}
	
	@Test
	public void getSector(){
		Sector sector = board.getSector(new Coordinates(5, 5));
		assertTrue(sector != null);
	}
	
	@Test
	public void canMoveHuman(){
		Coordinates coord, newCoord;
		
		human.setCurrentSector(board.getHumanSector());
		coord = human.getCurrentSector();
		newCoord = new Coordinates(coord.getX() + 1, coord.getY() + 1);

		assertTrue(board.canMove(human, newCoord));		
	}
	
	@Test
	public void cannotMoveHuman(){
		Coordinates coord, newCoord;
		
		human.setCurrentSector(board.getHumanSector());
		coord = human.getCurrentSector();
		newCoord = new Coordinates(coord.getX() + 2, coord.getY());
		
		assertFalse(board.canMove(human, newCoord));
	}
	
	@Test
	public void canMoveAlien(){
		Coordinates coord, newCoord;
		
		alien.setCurrentSector(board.getAlienSector());
		coord = alien.getCurrentSector();
		newCoord = new Coordinates(coord.getX() - 2, coord.getY());
		
		assertTrue(board.canMove(alien, newCoord));
	}
	
	@Test
	public void getNeighbors(){
		Set<Coordinates> neighbors;
		
		neighbors = board.getSectorNeighbors(new Coordinates(5, 6));
		
		assertNotNull(neighbors);
	}
	
	@Test
	public void sectorHasToDraw(){
		assertTrue(SectorFactory.getSector(SectorFactory.DANGEROUS).hasToDraw());
		assertTrue(SectorFactory.getSector(SectorFactory.HATCH).hasToDraw());
		assertFalse(SectorFactory.getSector(SectorFactory.ALIEN).hasToDraw());
		assertFalse(SectorFactory.getSector(SectorFactory.HUMAN).hasToDraw());
		assertFalse(SectorFactory.getSector(SectorFactory.SECURE).hasToDraw());
	}
}
