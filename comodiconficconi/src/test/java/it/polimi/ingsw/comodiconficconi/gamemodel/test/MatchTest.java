package it.polimi.ingsw.comodiconficconi.gamemodel.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;
import it.polimi.ingsw.comodiconficconi.gamemodel.Match;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.AlienSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.HumanSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.ItemCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.SectorCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alessandro
 * @author davide
 *
 */
public class MatchTest {

	private Match match;
	private Player player;

	@Before
	public void setUp() {
		match = new Match("galilei", 4);
		player = match.setPlayer(0);
		match.setPlayer(1);
		match.setPlayer(2);
		match.setPlayer(3);
		match.setInitialPlayer();
	}

	@Test
	public void createMatch() {
		assertNotNull(match);
	}

	// players
	@Test
	public void testLengthPlayers() {
		assertFalse(match.getPlayers().size() == 5);
	}

	@Test
	public void testPlayerInASector() {
		Coordinates coord = match.getCurrentPlayer().getCurrentSector();
		Sector sector = match.getBoard().getSector(coord);
		Set<Player> list = sector.getPlayersInside();
		assertFalse(list.isEmpty());
	}

	@Test
	public void testPlayerInASectorCurrentSector() {
		Coordinates coord = match.getCurrentPlayer().getCurrentSector();
		Sector sector = match.getBoard().getSector(coord);
		Set<Player> list = sector.getPlayersInside();
		assertTrue(list.contains(match.getCurrentPlayer()));
	}

	@Test
	public void testSetNextPlayer0() {
		assertTrue(match.getCurrentPlayer() == match.getPlayers().get(0));

	}

	@Test
	public void testSetNextPlayerer1() {
		match.setNextPlayer();
		assertTrue(match.getCurrentPlayer() == match.getPlayers().get(1));
	}

	@Test
	public void testSetNextPlayer2() {
		match.setNextPlayer();
		match.setNextPlayer();
		assertTrue(match.getCurrentPlayer() == match.getPlayers().get(2));
	}

	@Test
	public void testSetNextPslayer3() {
		match.setNextPlayer();
		match.setNextPlayer();
		match.setNextPlayer();
		assertTrue(match.getCurrentPlayer() == match.getPlayers().get(3));
	}

	@Test
	public void testSetNextPlayerTurnComplete() {
		match.setNextPlayer();
		match.setNextPlayer();
		match.setNextPlayer();
		match.setNextPlayer();
		assertTrue(match.getCurrentPlayer() == match.getPlayers().get(0));

	}

	@Test
	public void testGetPlayers() {
		assertNotNull(match.getPlayers());
	}

	@Test
	public void testSetPlayer() {
		assertTrue(player instanceof Player);
	}

	// deck and cards
	@Test
	public void testDrawSectorCard() {
		Card c;
		c = match.drawSectorCard();
		assertTrue(c instanceof SectorCard);
	}

	@Test
	public void testDrawItemCard() {
		Card c;
		c = match.drawItemCard();
		assertTrue(c instanceof ItemCard);
	}

	@Test
	public void testDrawItem() {
		assertNotNull(match.drawItemCard());
	}

	// move and initial position
	@Test
	public void testSetInitialPosition() {
		Coordinates c = player.getCurrentSector();
		Sector s = match.getBoard().getSector(c);
		assertTrue(s instanceof AlienSector || s instanceof HumanSector);
	}

	@Test
	public void testMovePlayer() {
		Coordinates c = player.getCurrentSector();
		Sector s = null;
		match.setInitialPlayer();
		if (match.getBoard().canMove(match.getCurrentPlayer(),
				new Coordinates(c.getX() - 1, c.getY() + 1)))
			s = match.movePlayer(new Coordinates(c.getX() - 1, c.getY() + 1));
		if (s != null)
			assertTrue(s.canMoveIn());
		else
			assertNull(s);
	}

	// end of available turns
	@Test
	public void testFinishTurnFailure() {
		for (int i = 0; i < 39; i++) {
			if (i != 0)
				match.setNextPlayer();
			match.setNextPlayer();
			match.setNextPlayer();
			assertFalse(match.finishTurn());
			assertTrue(match.checkWinners().isEmpty());
			match.setNextPlayer();
		}
	}

	@Test
	public void testfinishTurn() {
		for (int i = 0; i < 39; i++) {
			if (i != 0)
				match.setNextPlayer();
			match.setNextPlayer();
			match.setNextPlayer();
			match.setNextPlayer();
		}
		assertTrue(match.finishTurn());
		assertFalse(match.checkWinners().isEmpty());
	}

	@Test
	public void testCheckWinnersFailure() {
		match.setNextPlayer();
		match.setNextPlayer();
		match.killPlayer(match.getCurrentPlayer());
		assertTrue(match.checkWinners().isEmpty());
	}

	// finshHuman
	@Test
	public void testfinishHumanBefore() {
		assertFalse(match.finishHuman());
	}

	@Test
	public void testfinishHumanAfter() {
		List<Player> players = match.getPlayers();
		Player play = null;
		for (Player p : players)
			if (p.isHuman())
				play = p;
		match.killPlayer(play);
		for (Player p : players)
			if (p.isHuman())
				play = p;
		match.killPlayer(play);
		assertTrue(match.finishHuman());
	}

	@Test
	public void testFinishHumanCasual() {
		int i = 0;
		List<Player> players = match.getPlayers();
		Player play = null;
		while (i < 2) {
			for (Player p : players)
				if (p.isHuman())
					play = p;
			players.remove(play);
			i++;
		}
		assertTrue(match.finishHuman());
	}

	@Test
	public void testLastHumanEscapedNotEmpty() {
		int i = 0;
		List<Player> players = match.getPlayers();
		Player play = null;
		while (i < 2) {
			for (Player p : players)
				if (p.isHuman())
					play = p;
			players.remove(play);
			i++;
		}
		assertTrue(!match.lastHumanEscaped(play).isEmpty());
	}

	@Test
	public void testLastHumanEscapedAlienWinners() {
		int i = 0;
		List<Player> players = match.getPlayers();
		Player play = null;
		while (i < 2) {
			for (Player p : players)
				if (p.isHuman())
					play = p;
			players.remove(play);
			i++;
		}
		for (Player p : players) {
			assertTrue(!p.isHuman());
		}
	}

	@Test
	public void testLastHumanEscapedAlienLosers() {
		int i = 0;
		List<Player> players = match.getPlayers();
		Player play = null;
		while (i < 2) {
			for (Player p : players)
				if (p.isHuman())
					play = p;
			players.remove(play);
			i++;
		}
		for (Player p : players) {
			assertTrue(match.lastHumanEscaped(play).containsKey(p));
			assertFalse(match.lastHumanEscaped(play).get(p));
		}
	}

	@Test
	public void testLastHumanKilled() {
		List<Player> players = match.getPlayers();
		Player play = null;
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		assertTrue(!match.lastHumanDied().isEmpty());
	}

	@Test
	public void testOnlyAliensWinnersLastHumanDied() {
		List<Player> players = match.getPlayers();
		Player play = null;
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		for (Player p : players)
			assertTrue(!p.isHuman());
	}

	@Test
	public void testOnlyWinnersWhenLastHumanDied() {
		List<Player> players = match.getPlayers();
		Player play = null;
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		for (Player p : players)
			if (p.isHuman()) {
				play = p;
				break;
			}
		match.killPlayer(play);
		for (Player p : players)
			if (match.lastHumanDied().containsKey(p))
				assertTrue(match.lastHumanDied().get(p));
	}

	@Test
	public void testfinishHumansfailure() {
		List<Player> players = match.getPlayers();
		Player play = null;
		for (Player p : players)
			if (p.isHuman())
				play = p;
		match.killPlayer(play);
		assertFalse(match.finishHuman());

	}

	// finish Hatch
	@Test
	public void testFinishOpenHatchFailure() {

		List<Sector> hatch = match.getBoard().getHatchSector();
		for (Sector s : hatch) {
			assertFalse(match.finishOpenHatch());
			match.sectorEffect(s);
		}
	}

	@Test
	public void testNoMoreHatchesFailure() {
		List<Sector> hatch = match.getBoard().getHatchSector();
		for (Sector s : hatch) {
			assertTrue(match.noMoreHatches().isEmpty());
			match.sectorEffect(s);
		}
	}

	@Test
	public void testFinishOpenHatches() {

		List<Sector> hatch = match.getBoard().getHatchSector();
		for (Sector s : hatch) {
			match.sectorEffect(s);
		}
		assertTrue(match.finishOpenHatch());
	}

	@Test
	public void testNoMoreHatchesNotEmpty() {
		List<Sector> hatch = match.getBoard().getHatchSector();
		for (Sector s : hatch) {
			match.sectorEffect(s);
		}
		assertTrue(!match.noMoreHatches().isEmpty());
	}

	@Test
	public void testNoMoreHatchesWinners() {
		List<Sector> hatch = match.getBoard().getHatchSector();
		for (Sector s : hatch) {
			match.sectorEffect(s);
		}
		for (Player p : match.getPlayers())
			if (match.noMoreHatches().containsKey(p))
				if (!p.isHuman())
					assertTrue(match.noMoreHatches().get(p));
				else
					assertTrue(!match.noMoreHatches().get(p));
	}

	// Death and attack
	@Test
	public void testDeathOnPlayers() {
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		int size = match.getPlayers().size();
		Player play = null;
		for (Player p : match.getPlayers())
			if (!p.isHuman())
				play = p;
		while (!play.equals(match.getCurrentPlayer())) {
			match.setNextPlayer();
		}
		match.attack();
		assertFalse(size == match.getPlayers().size());
	}

	@Test
	public void testHungryAlienSteps() {
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		match.setNextPlayer();
		match.movePlayer(new Coordinates(8, 10));
		Player play = null;
		for (Player p : match.getPlayers())
			if (!p.isHuman())
				play = p;
		while (!play.equals(match.getCurrentPlayer())) {
			match.setNextPlayer();
		}
		match.attack();
		assertTrue(match.getCurrentPlayer().getSteps() == ConstantPlayer.HUNGRYALIENSTEPS);
	}
}