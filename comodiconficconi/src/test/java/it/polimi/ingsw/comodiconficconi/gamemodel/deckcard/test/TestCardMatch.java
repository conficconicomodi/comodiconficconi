package it.polimi.ingsw.comodiconficconi.gamemodel.deckcard.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.Match;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.VisitorConcrete;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseInSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Silence;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

import org.junit.Before;
import org.junit.Test;

public class TestCardMatch {

	private Match match;
	private Player player;
	private List<Player> players;
	private Visitor visitor;

	@Before
	public void setUp() {
		match = new Match("galilei", 8);
		match.setPlayer(0);
		match.setPlayer(1);
		match.setPlayer(2);
		match.setPlayer(3);
		match.setPlayer(4);
		match.setPlayer(5);
		match.setPlayer(6);
		match.setPlayer(7);
		match.setInitialPlayer();
		players = match.getPlayers();
		setHumanPlayer();
		visitor = new VisitorConcrete(match);
	}

	private void setHumanPlayer() {
		for (Player play : players) {
			if (play.isHuman()) {
				player = play;
				break;
			} else
				match.setNextPlayer();
		}
	}

	private void setAlienPlayer() {
		for (Player play : players) {
			if (!play.isHuman()) {
				player = play;
				break;
			} else
				match.setNextPlayer();
		}
	}

	@Test
	public void testAdrenaline() {
		Card adrenaline = FactoryCard.getCard(ConstantCards.ADRENALINECODE);
		player.addItem(adrenaline);
		match.useCard(adrenaline);
		assertTrue(player.getSteps() == 2);

	}

	@Test
	public void testAttack() {
		Card attack = FactoryCard.getCard(ConstantCards.ATTACKCODE);
		player.addItem(attack);
		match.useCard(attack);
		assertFalse(player.hasThisCard(attack));
		match.attack();
	}

	@Test
	public void testDefense() {
		Player human = player;
		Card defense = FactoryCard.getCard(ConstantCards.DEFENSECODE);
		player.addItem(defense);
		match.useCard(defense);
		match.movePlayer(new Coordinates(10, 10));
		setAlienPlayer();
		match.movePlayer(new Coordinates(10, 10));
		match.attack();
		assertFalse(human.isDead());
		assertFalse(player.hasDefense());
	}

	@Test
	public void testTeleport() {
		Card teleport = FactoryCard.getCard(ConstantCards.TELEPORTCODE);
		player.addItem(teleport);
		match.movePlayer(new Coordinates(10, 10));
		match.useCard(teleport);
		assertTrue(match.getCurrentPlayer().getCurrentSector()
				.equals(match.getBoard().getHumanSector()));
	}

	@Test
	public void testSedative() {
		Card sedative = FactoryCard.getCard(ConstantCards.SEDATIVECODE);
		player.addItem(sedative);
		match.useCard(sedative);
		assertFalse(player.hasThisCard(sedative));
	}

	@Test
	public void testSpotLight() {
		Card spotlight = FactoryCard.getCard(ConstantCards.SPOTLIGHTCODE);
		player.addItem(spotlight);
		match.useCard(spotlight);
		assertFalse(player.hasThisCard(spotlight));
	}

	@Test
	public void testNotfierVisitor() {
		assertFalse(visitor.notifierSet());
	}

	@Test
	public void testDrawSectorCard() {
		Coordinates coordinates = new Coordinates(10, 9);
		Sector sector = match.movePlayer(coordinates);
		match.sectorEffect(sector);
		assertTrue(sector.hasToDraw());
	}

	@Test
	public void testNoiseInsectorWithItem() {
		Card noiseSectorItem = FactoryCard
				.getCard(ConstantCards.NOISEINSECTORWITHITEMCODE);
		assertTrue(noiseSectorItem instanceof NoiseInSector);
		match.useCard(noiseSectorItem);
	}

	@Test
	public void testNoiseInsectorWithoutItem() {
		Card noiseSector = FactoryCard.getCard(ConstantCards.NOISEINSECTORCODE);
		assertTrue(noiseSector instanceof NoiseInSector);
		match.useCard(noiseSector);
	}

	@Test
	public void testNoiseAnywhereWithItem() {
		Card noiseAnywhereItem = FactoryCard
				.getCard(ConstantCards.NOISEANYWHEREWITHITEMCODE);
		assertTrue(noiseAnywhereItem instanceof NoiseAnywhere);
		match.useCard(noiseAnywhereItem);
	}

	@Test
	public void testNoiseAnywhereWithoutItem() {
		Card noiseAnywhere = FactoryCard
				.getCard(ConstantCards.NOISEANYWHERECODE);
		assertTrue(noiseAnywhere instanceof NoiseAnywhere);
		match.useCard(noiseAnywhere);
	}

	@Test
	public void testSilence() {
		Card silence = FactoryCard.getCard(ConstantCards.SILENCECODE);
		assertTrue(silence instanceof Silence);
		match.useCard(silence);
	}

	@Test
	public void testNoMoreHatch() {
		List<Sector> list = match.getBoard().getHatchSector();
		for (Sector sector : list) {
			match.sectorEffect(sector);
		}
		assertTrue(match.finishOpenHatch());
	}
	
	
	@Test
	public void testOpenHatch(){
		Card openHatch1 =  FactoryCard.getCard(ConstantCards.OPENHATCHCODE);
		Card openHatch2 =  FactoryCard.getCard(ConstantCards.OPENHATCHCODE);
		match.useCard(openHatch1);
		assertFalse(match.finishHuman());
		setHumanPlayer();
		List<Player> list = new ArrayList<Player>(players);
		for(Player player : list)
			if(!player.equals(match.getCurrentPlayer()))
				match.killPlayer(player);
		match.useCard(openHatch2);
		assertTrue(match.finishHuman());
	}
}
