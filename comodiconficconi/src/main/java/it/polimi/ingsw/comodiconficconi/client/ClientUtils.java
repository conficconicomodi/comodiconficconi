package it.polimi.ingsw.comodiconficconi.client;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * This class serves as a support for the client to translate from coordinates
 * objects to string coordinates.
 * 
 * @author alessandro
 *
 */
public class ClientUtils {

	/**
	 * This is a private constructor so that this class can never be
	 * instantiated
	 */
	private ClientUtils() {
	}
	
	/**
	 * This method translates the coordinates object to string coordinates
	 * 
	 * @param coord
	 * @return String coordinates
	 */
	public static String coordToString(Coordinates coord) {
		String msg;
		if (coord.getX() + 1 < 10)
			msg = (char) (coord.getY() + 65) + "0" + (coord.getX() + 1);
		else
			msg = (char) (coord.getY() + 65) + "" + (coord.getX() + 1);
		return msg;
	}

	/**
	 * This method translates the string coordinates to coordinates object
	 * 
	 * @param string
	 * @return
	 */
	public static Coordinates stringToCoord(String string) {
		int xCoord = 0;
		int yCoord = 0;

		yCoord = (int) string.charAt(0) - 65;
		if ((int) string.charAt(1) == 0)
			xCoord = ((int) string.charAt(2) - 48) - 1;
		else
			xCoord = (((int) string.charAt(1) - 48) * 10 + ((int) string
					.charAt(2) - 48)) - 1;
		return new Coordinates(xCoord, yCoord);

	}
}
