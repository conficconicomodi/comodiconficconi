package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * the command that notify that client's user want to use a card
 * 
 * @author davide
 *
 */
public class CommandCard extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5378550145019407753L;
	private Card card;

	/**
	 * 
	 * @param card
	 *            : the card want to be used
	 */
	public CommandCard(Card card) {
		this.card = card;
	}

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
		commandHandlerServer.applyCommand(this);
	}

	public Card getCard() {
		return card;
	}
}
