package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * * this class represent the card that let a player be considered as an alien
 * 
 * @author davide
 *
 */
public class AlienCard extends CharacterCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3406475970756069589L;

	@Override
	public String toString() {
		return ConstantPlayer.ALIEN;
	}

}
