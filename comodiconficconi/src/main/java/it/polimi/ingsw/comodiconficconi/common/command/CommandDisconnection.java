package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command to notify the disconnection
 * 
 * @author davide
 *
 */
public class CommandDisconnection extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3538763076443703693L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);

	}

}
