package it.polimi.ingsw.comodiconficconi.gamemodel.card;

/**
 * A class that by the factory pattern create the object card of the game
 * 
 * @author davide
 */

public class FactoryCard {
	private static final int ITEM = 1;
	private static final int SECTOR = 2;
	private static final int CHARACTER = 3;
	private static final int HATCH = 4;
	private static final int SCALEFACTOR = 10;

	private static int tmp;
	// code for character card
	private static final int ALIEN = 0;
	private static final int HUMAN = 1;
	// code for hatch card
	private static final int OPEN = 0;
	private static final int BLOCKED = 1;
	// code for sector card
	private static final int NOISEANYWHERE = 0;
	private static final int NOISEANYWHEREWITHOBJECT = 1;
	private static final int NOISEINSECTOR = 2;
	private static final int NOISEINSECTORWITHOBJECT = 3;
	private static final int SILENCE = 4;

	// code for item card
	private static final int ADRENALINE = 0;
	private static final int ATTACKCARD = 1;
	private static final int DEFENSE = 2;
	private static final int SEDATIVE = 3;
	private static final int SPOTLIGHT = 4;
	private static final int TELEPORT = 5;

	private FactoryCard() {

	}

	/**
	 * 
	 * @param id
	 *            this parameter is the value for create card; 10 to 15 stands
	 *            for an item card, 20 to 24 sector card, 30 to 31 character
	 *            card , 40 to 41 hatch card; the codification of card is in
	 *            constant class
	 * @return the new card created
	 */
	public static Card getCard(int id) throws CardCodeException{
		tmp = id / 10;
		if (tmp == SECTOR || tmp == HATCH) {
			return factorSecHatch(id);
		} else if (tmp == ITEM) {
			return factoryItemCard(id % SCALEFACTOR);
		} else if (tmp == CHARACTER) {
			return factoryCharacterCard(id % SCALEFACTOR);
		}

		throw new CardCodeException();

	}

	private static Card factorSecHatch(int id) throws CardCodeException{
		if (tmp == SECTOR)
			return factorySectorCard(id % SCALEFACTOR);
		else if (tmp == HATCH)
			return factoryHatchCard(id % SCALEFACTOR);
		throw new CardCodeException();
	}

	/**
	 * 
	 * @param id
	 *            the id number for human or alien card; 0 stands for alien
	 *            while 1 stands for human
	 * 
	 * @return the human or alien card created
	 * @throws CardCodeException 
	 */
	private static Card factoryCharacterCard(int id) throws CardCodeException {
		switch (id) {
		case ALIEN:
			return new AlienCard();
		case HUMAN:
			return new HumanCard();
		default:
			throw new CardCodeException();

		}
	}

	/**
	 * 
	 * @param id
	 *            the id number 0 for open or 1 for blocked card; 0 stands for
	 *            open hatch 1 to blocked hatch
	 * @return the blocked or open hatch card created
	 * @throws CardCodeException 
	 */
	private static Card factoryHatchCard(int id) throws CardCodeException {
		if (id == OPEN)
			return new OpenHatchCard();
		else if (id == BLOCKED)
			return new BlockedHatchCard();
		else
			throw new CardCodeException();

	}

	/**
	 * 
	 * @param id
	 *            the id number for noise anywhere noise in sector or silence
	 *            card;
	 * 
	 *            0 stands for noise anywhere 1 for noise anywhere with object 2
	 *            for noise in sector 3 for noise in sector and 4 silence
	 * 
	 * @return the sector card created
	 * @throws CardCodeException 
	 */
	private static Card factorySectorCard(int id) throws CardCodeException {
		if (id == NOISEANYWHERE || id == NOISEANYWHEREWITHOBJECT)
			return factorNoiseanywhere(id);

		else if (id == NOISEINSECTOR || id == NOISEINSECTORWITHOBJECT)
			return factorNoiseinsector(id);
		else if (id == SILENCE)
			return new Silence();
		else
			throw new CardCodeException();
	}

	// factor item for noise anywhere to decrement complexity
	private static Card factorNoiseanywhere(int id) throws CardCodeException {
		if (id == NOISEANYWHERE)
			return new NoiseAnywhere(false);
		else if (id == NOISEANYWHEREWITHOBJECT)
			return new NoiseAnywhere(true);
		throw new CardCodeException();
	}

	// factor item for adrenaline and noise anywhere to decrement complexity
	private static Card factorNoiseinsector(int id) throws CardCodeException {
		if (id == NOISEINSECTOR)
			return new NoiseInSector(false);
		else if (id == NOISEINSECTORWITHOBJECT)
			return new NoiseInSector(true);
		throw new CardCodeException();
	}

	/**
	 * 
	 * @param id
	 *            is a number between 0 and 5 that make create the item card; 0
	 *            for adrenaline, 1 for attack, 2 for sedative , 3 for
	 *            spotlight, 5 for for teleport
	 * @return return the item card that correspond to the id number
	 * @throws CardCodeException 
	 */
	private static Card factoryItemCard(int id) throws CardCodeException {
		if (id == ADRENALINE || id == SEDATIVE)
			return factorAdrSed(id);
		else if (id == ATTACKCARD || id == DEFENSE)
			return factorAttDef(id);
		else if (id == SPOTLIGHT || id == TELEPORT)
			return factorSpoTel(id);
		
		throw new CardCodeException();

	}

	// factor item for adrenaline and sedative to decrement complexity
	private static Card factorAdrSed(int id) throws CardCodeException {
		if (id == ADRENALINE)
			return new Adrenaline();
		else if (id == SEDATIVE)
			return new Sedative();
		
		throw new CardCodeException();
	}

	// factor item for attack and defense to decrement complexity
	private static Card factorAttDef(int id) throws CardCodeException {
		if (id == ATTACKCARD)
			return new AttackCard();
		else if (id == DEFENSE)
			return new Defense();
		throw new CardCodeException();
	}

	// factor item for spotlight and teleport to decrement complexity
	private static Card factorSpoTel(int id) throws CardCodeException {
		if (id == SPOTLIGHT)
			return new SpotLight();
		else if (id == TELEPORT)
			return new Teleport();
		throw new CardCodeException();
	}
}
