package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;

/**
 *
 * This class instantiate the different kind of maps
 *
 * @author alessandro
 *
 */
public class BoardFactory {

	private static final int ROWS = 14;
	private static final int COLUMNS = 23;

	private static final Logger LOGGER = Logger.getLogger(BoardFactory.class);
	private static Scanner inFile;

	private BoardFactory() {
	}

	/**
	 * This static method takes a file containing all the sectors as parameter
	 * and scans it number by number and it instantiates every sector and
	 * putting it into the board
	 * 
	 * @param map
	 * @param board
	 */
	public static void getBoard(File map, Map<Coordinates, Sector> board) {
		try {
			inFile = new Scanner(map);
		} catch (FileNotFoundException e) {
			LOGGER.debug("Couldn't find file " + e);
		}

		int sector;
		for (int i = 0; i < ROWS; i++)
			for (int j = 0; j < COLUMNS; j++) {
				if (inFile.hasNext()) {
					sector = Integer.parseInt(inFile.next());
					board.put(new Coordinates(i, j),
							SectorFactory.getSector(sector));
				}
			}

		inFile.close();
	}
}
