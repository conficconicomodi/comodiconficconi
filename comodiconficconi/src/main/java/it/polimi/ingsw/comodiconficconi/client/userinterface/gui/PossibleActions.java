package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.PossibleActionListener;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * This class contains all the possible actions that a user can make in its turn
 * 
 * @author alessandro
 *
 */
public class PossibleActions extends JPanel {

	private static final long serialVersionUID = 1L;

	private AttackLabel attack;
	private MoveLabel move;
	private PassTurnLabel passTurn;
	private DrawCardLabel drawCard;
	private DiscardItem discardItem;

	private transient List<PossibleActionListener> listeners = new ArrayList<PossibleActionListener>();

	private Dimension dimension;

	/**
	 * 
	 * @param dimension
	 */
	public PossibleActions(Dimension dimension) {
		setBackground(Color.WHITE);
		setLayout(new FlowLayout());
		setBorder(new EmptyBorder(5, 5, 5, 5));

		this.dimension = dimension;
		setSize(dimension);
		setPreferredSize(dimension);

		ImageIcon attackImg = getScaledImage(new ImageIcon(this.getClass()
				.getResource(ConstantClient.ATTACKACTION)));
		ImageIcon moveImg = getScaledImage(new ImageIcon(this.getClass()
				.getResource(ConstantClient.MOVEACTION)));
		ImageIcon passTurnImg = getScaledImage(new ImageIcon(this.getClass()
				.getResource(ConstantClient.PASSTURNACTION)));
		ImageIcon drawCardImg = getScaledImage(new ImageIcon(this.getClass()
				.getResource(ConstantClient.DRAWCARDACTION)));
		ImageIcon discardImg = getScaledImage(new ImageIcon(this.getClass()
				.getResource(ConstantClient.DISCARDITEMACTION)));

		attack = new AttackLabel(attackImg);
		move = new MoveLabel(moveImg);
		passTurn = new PassTurnLabel(passTurnImg);
		drawCard = new DrawCardLabel(drawCardImg);
		discardItem = new DiscardItem(discardImg);

	}

	public void showAttack() {
		add(attack);
	}

	public void showDrawCard() {
		add(drawCard);
	}

	public void showPassTurn() {
		add(passTurn);
	}

	public void showMove() {
		add(move);
	}

	public void showDiscarItem() {
		add(discardItem);
	}

	public void addListener(PossibleActionListener listener) {
		listeners.add(listener);
	}

	private void notifyAttack() {
		for (PossibleActionListener listener : listeners)
			listener.notifyAttack();
	}

	private void notifyMove() {
		for (PossibleActionListener listener : listeners)
			listener.notifyMove();
	}

	private void notifyPassTurn() {
		for (PossibleActionListener listener : listeners)
			listener.notifyPassTurn();
	}

	private void notifyDrawCard() {
		for (PossibleActionListener listener : listeners)
			listener.notifyDrawCard();
	}

	private void notifyDiscard() {
		for (PossibleActionListener listener : listeners)
			listener.notifyDiscardItem();
	}

	private ImageIcon getScaledImage(ImageIcon imageAction) {
		Image image = imageAction.getImage();
		Image newImg = image.getScaledInstance(dimension.width / 3 - 20,
				dimension.height - 10, Image.SCALE_SMOOTH);
		return new ImageIcon(newImg);
	}

	private class AttackLabel extends JLabel {

		private static final long serialVersionUID = 1L;

		public AttackLabel(ImageIcon attackImage) {
			super(attackImage);
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		class MyMouseListener extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent e) {
				notifyAttack();
			}
		}
	}

	private class PassTurnLabel extends JLabel {

		private static final long serialVersionUID = 1L;

		public PassTurnLabel(ImageIcon attackImage) {
			super(attackImage);
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		class MyMouseListener extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent e) {
				notifyPassTurn();
			}
		}
	}

	private class MoveLabel extends JLabel {

		private static final long serialVersionUID = 1L;

		public MoveLabel(ImageIcon attackImage) {
			super(attackImage);
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		class MyMouseListener extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent e) {
				notifyMove();
			}
		}
	}

	private class DrawCardLabel extends JLabel {

		private static final long serialVersionUID = 1L;

		public DrawCardLabel(ImageIcon attackImage) {
			super(attackImage);
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		class MyMouseListener extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent e) {
				notifyDrawCard();
			}
		}
	}

	private class DiscardItem extends JLabel {

		private static final long serialVersionUID = 1L;

		public DiscardItem(ImageIcon discardImage) {
			super(discardImage);
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		class MyMouseListener extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent e) {
				notifyDiscard();
			}
		}
	}
}
