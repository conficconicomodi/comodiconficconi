package it.polimi.ingsw.comodiconficconi.client.userinterface;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

import java.util.List;
import java.util.Map;

/**
 * This interface gives methods to be implemented both in GUI and CLI
 * 
 * @author alessandro
 *
 */
public interface UserInterface {

	/**
	 * This method initializes a match with a given board, player associated to
	 * the client and a string representing the map
	 * 
	 * @param board
	 * @param player
	 * @param mapName
	 */
	void initializeMatch(Map<Coordinates, Sector> board, Player player,
			String mapName);

	/**
	 * This method informs the player that his turn has started and he has to do
	 * send commands
	 */
	void startTurn();

	/**
	 * This method informs the player that he has to make a move
	 * 
	 * @param list
	 */
	void askMove(List<String> list);

	/**
	 * This method informs the player that he has to send to server coordinates
	 * for noise anywhere
	 */
	void askNoiseCoordinates();

	/**
	 * This method informs the player that he has to send to server coordinates
	 * for spotlight
	 */
	void askSpotlightCoordinates();

	/**
	 * This method notifies that coordinates are needed from the server
	 */
	void askMoveCoordinates();

	/**
	 * This method informs the user that an item card has been added to the
	 * player
	 * 
	 * @param card
	 */
	void addCard(Card card);

	/**
	 * This method informs the user that an item card has been used/removed from
	 * his deck
	 * 
	 * @param card
	 */
	void removeCard(Card card);

	/**
	 * This method changes the position of the player and updates the past moves
	 * he has done from the start of the match
	 * 
	 * @param coord
	 * @param pastMoves
	 */
	void setMovePlayer(Coordinates coord, List<Coordinates> pastMoves);

	/**
	 * This method changes the position of the player
	 * 
	 * @param newPosition
	 */
	void setMovePlayer(Coordinates newPosition);

	/**
	 * This method notify a general thing to the player
	 * 
	 * @param notification
	 */
	void notifyToAll(String notification);

	/**
	 * This method notifies that the turn has ended
	 */
	void turnEnded();

	/**
	 * This method notifies that the player has been killed
	 */
	void death();

	/**
	 * This method notifies that the player has won
	 */
	void win();

	/**
	 * This method notifies that the player has lost
	 */
	void lose();

	/**
	 * This method informs the player that the command he gave in input was not
	 * valid
	 */
	void commandNotValid();

	/**
	 * This method notifies a disconnection from server
	 */
	void disconnection();

	/**
	 * This method notifies that the server is out of order
	 */
	void serverOutOfOrder();
}
