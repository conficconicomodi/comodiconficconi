package it.polimi.ingsw.comodiconficconi.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface represent the remote object of the client that exposed method
 * to receive command and to close the connection.
 * 
 * @author davide
 *
 */
public interface RemoteClientInterface extends Remote {
	/**
	 * the handler of client use this to notify the client a new command sent to
	 * it
	 * 
	 * @param command
	 *            : the command sent to the client
	 * @throws RemoteException
	 */
	public void notifyNewCommand(CommandServerToClient command)
			throws RemoteException;

	/**
	 * method to close the communication channel with client
	 * 
	 * @throws RemoteException
	 */
	public void close() throws RemoteException;

}
