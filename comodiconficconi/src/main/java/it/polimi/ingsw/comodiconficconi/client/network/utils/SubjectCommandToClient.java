package it.polimi.ingsw.comodiconficconi.client.network.utils;

import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * Interface used to notify Observer of Command for Client, it's like the
 * Subject of Observer pattern, with the difference that notify a command
 * received
 * 
 * @author davide
 *
 */
public interface SubjectCommandToClient {
	/**
	 * Add an Observer
	 * 
	 * @param observer
	 *            : the observer that I want to add
	 */
	void addObserver(ObserverCommandToClient observer);

	/**
	 * Remove the Observer
	 * 
	 * @param observer
	 *            : observer want to be removed
	 */
	void removeObserver(ObserverCommandToClient observer);

	/**
	 * Notify the Observer of the command received
	 * 
	 * @param command
	 *            : the command received and need to be notified
	 */
	void notifyObserver(CommandServerToClient command);
}
