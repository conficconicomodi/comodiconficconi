package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.client.ClientUtils;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.GameBoardListener;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * This class contains all the main panels which are GameConsole, GameBoard and
 * PlayerMoves. It has functions that permit the changing and updating of the
 * panels.
 * 
 * @author alessandro
 *
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private transient Toolkit toolkit = Toolkit.getDefaultToolkit();
	private Dimension screenDimension = new Dimension();
	private int height;
	private int width;
	private int toolBarHeight;

	private Container content;
	private GameBoard board;
	private PlayerMoves moves;
	private GameConsole console;
	private GeneralPanel general;

	/**
	 * 
	 * This constructor gets the dimension of the screen on which the user is
	 * playing and depending on it the frame will be dimensioned so that every
	 * panel fits in it. At start a new general panel is created and it will
	 * contain a welcome image until the match starts
	 * 
	 */
	public MainFrame() {
		super("Escape from the Aliens in Outer Space");

		setLayout(new BorderLayout());
		content = this.getContentPane();

		screenDimension = toolkit.getScreenSize();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		toolBarHeight = Toolkit.getDefaultToolkit().getScreenInsets(
				getGraphicsConfiguration()).bottom;

		width = screenDimension.width;
		height = screenDimension.height - toolBarHeight;

		addGeneralPanel(ConstantClient.WELCOME);
		pack();
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
	}

	/**
	 * This method serves to remove the welcome image from the frame
	 */
	public void removeWelcome() {
		remove(general);
	}

	/**
	 * This method has as parameters a game board containing coordinates and
	 * their relative sectors and a listener that will be notified of every
	 * click on the board. given this parameters the method instantiates a new
	 * game board panel (WEST)
	 * 
	 * @param gameBoard
	 * @param listener
	 */
	public void setGameBoard(Map<Coordinates, Sector> gameBoard,
			GameBoardListener listener) {
		board = new GameBoard(gameBoard, new Dimension(width, height));
		board.addListeners(listener);
		content.add(board, BorderLayout.WEST);
	}

	/**
	 * This method sets a new player moves panel (NORTH)
	 */
	public void setMoves() {
		moves = new PlayerMoves(new Dimension(width, 80));
		content.add(moves, BorderLayout.NORTH);
	}

	/**
	 * This method sets a new game consol panel (EAST)
	 */
	public void setGameConsole() {
		console = new GameConsole(new Dimension(width * 1 / 3,
				board.getPreferredSize().height));
		content.add(console, BorderLayout.EAST);
	}

	/**
	 * This method adds a new message to the JTextArea in the game console
	 * 
	 * @param string
	 */
	public void addMsg(String string) {
		JTextArea msgConsole = console.getMsgConsole();
		msgConsole.append(string);
		msgConsole.setCaretPosition(msgConsole.getDocument().getLength());

	}

	public GameBoard getGameBoard() {
		return board;
	}

	public GameConsole getGameConsole() {
		return console;
	}

	public PlayerMoves getPlayerMoves() {
		return moves;
	}

	/**
	 * This method updates all the past moves of the player and displays them in
	 * the Player moves panel
	 * 
	 * @param pastMoves
	 */
	public void updatePastMoves(List<Coordinates> pastMoves) {
		List<JLabel> pastMovesInGUI = moves.getPastMoves();
		Coordinates coord;
		int index;
		for (index = 1; index <= pastMoves.size(); index++) {
			coord = pastMoves.get(index - 1);
			if (index < 10)
				pastMovesInGUI.get(index - 1).setText(
						"0" + index + "[" + ClientUtils.coordToString(coord)
								+ "].");
			else
				pastMovesInGUI.get(index - 1).setText(
						index + "[" + ClientUtils.coordToString(coord) + "].");
		}
		moves.repaint();
	}

	/**
	 * This method removes all the possible actions from the Action panel
	 */
	public void removeAllActions() {
		console.getPossibleActionsPanel().removeAll();
		console.repaint();
	}

	/**
	 * This method adds a general panel to the frame. It needs a string
	 * containing the path of the image
	 * 
	 * @param image
	 */
	public void addGeneralPanel(String image) {
		ImageIcon backgroundImg = new ImageIcon(this.getClass().getResource(
				image));
		int imageWidth = backgroundImg.getIconWidth();
		int imgHeight = backgroundImg.getIconHeight();
		
		general = new GeneralPanel(backgroundImg);
		general.setSize(new Dimension(imageWidth, imgHeight));
		general.setPreferredSize(new Dimension(imageWidth, imgHeight));
		content.add(general);
		pack();
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);	
		
		Runnable repaintFrame = new Runnable() {
			@Override
			public void run(){
				content.repaint();
			}
		};
		
		SwingUtilities.invokeLater(repaintFrame);
	}
}
