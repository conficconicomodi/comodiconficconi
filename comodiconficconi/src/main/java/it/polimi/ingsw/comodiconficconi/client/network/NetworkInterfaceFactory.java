package it.polimi.ingsw.comodiconficconi.client.network;

import it.polimi.ingsw.comodiconficconi.client.network.NetworkInterface;
import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;

/**
 * This class represent the Factory pattern and is used to create a Network
 * Interface
 * 
 * @author davide
 *
 */
public class NetworkInterfaceFactory {

	private NetworkInterfaceFactory() {

	}

	/**
	 * this method bring back a NetworkInterface
	 * 
	 * @param id
	 *            : the id number of network interface : 1 for Socket, 2 for
	 *            RMI, default value is socket interface
	 * @return the network interface
	 */
	public static NetworkInterface getNetworkInterface(int id) {

		switch (id) {

		case ConstantNetwork.SOCKETINTERFACE:
			return new SocketInterface();

		case ConstantNetwork.RMIINTERFACE:
			return new RMIInterface();

		default:
			return new SocketInterface();
		}
	}
}
