package it.polimi.ingsw.comodiconficconi.common.command;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * command used to notify a new position
 * 
 * @author davide
 *
 */
public class CommandNewPosition extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1950253554928150766L;
	private Coordinates newPosition;
	private List<Coordinates> pastMoves;

	/**
	 * 
	 * @param newPosition
	 *            : the coordinates of the new position
	 * @param pastMoves
	 *            : the list of the past moves
	 */
	public CommandNewPosition(Coordinates newPosition,
			List<Coordinates> pastMoves) {
		this.newPosition = newPosition;
		this.pastMoves = new ArrayList<Coordinates>(pastMoves);
	}

	/**
	 * 
	 * @param newPosition
	 *            : the new position
	 */
	public CommandNewPosition(Coordinates newPosition) {
		this.newPosition = newPosition;
	}

	public Coordinates getNewPosition() {
		return newPosition;
	}

	public List<Coordinates> getPastMoves() {
		return pastMoves;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}
}
