package it.polimi.ingsw.comodiconficconi.server;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.server.utils.ObserverMatchHandler;

/**
 * this interface define the behavior of client handler: this class has to
 * handle the communication with a single client, has notify adding of a client,
 * and has to receive and send command through the channel dedicated that the
 * user has chose on client
 * 
 * @author davide
 *
 */

public abstract class ClientHandler implements Runnable {

	protected Player player;
	protected int code;
	protected boolean closed;

	public int getCode() {
		return code;
	}

	/**
	 * this method bind a client handler, than a client, with a player
	 * 
	 * @param player
	 *            : the player of the game
	 */
	public void addPlayer(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	/**
	 * add the allower for command application
	 * 
	 * @param allower
	 *            : the allower for the command
	 */
	public abstract void addAllower(ObserverMatchHandler allower);

	/**
	 * add the parser, applier of command
	 * 
	 * @param commandHandler
	 *            : the command handler
	 */
	public abstract void addCommandHandler(CommandHandlerServer commandHandler);

	/**
	 * send the command to the client
	 * 
	 * @param command
	 *            : the command going to be sent
	 */
	public abstract void sendCommand(CommandServerToClient command)
			throws Exception;

	/**
	 * notify a new command received
	 * 
	 * @param command
	 *            : the command received that wille be notified
	 */
	public abstract void notifyNewCommand(CommandClientToServer command);

	/**
	 * match invoked this method to close and notify the close to client
	 */
	public abstract void closeByServer();

	/**
	 * this method invoked when is the that client close connection
	 */
	public abstract void closeByClient();

	public boolean isClosed() {
		return closed;
	}

}
