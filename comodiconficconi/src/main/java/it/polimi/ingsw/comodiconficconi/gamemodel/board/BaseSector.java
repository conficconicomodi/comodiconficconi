package it.polimi.ingsw.comodiconficconi.gamemodel.board;

/**
 * This class defines a generic base sector. It should have two or more
 * subclasses of the specific starting sector of every character
 * 
 * @author alessandro
 *
 */
public class BaseSector extends Sector {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7658903324032848988L;

	@Override
	public boolean canMoveIn() {
		return false;
	}

	@Override
	public boolean hasToDraw() {
		return false;
	}

}
