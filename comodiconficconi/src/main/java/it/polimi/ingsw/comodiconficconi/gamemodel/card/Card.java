package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import java.io.Serializable;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitable;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * This class represent the abstract object card that can be used by the support of the visitor
 * @author davide
 *
 */
public abstract class Card implements Visitable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5774283205460650456L;
	@Override
	public void applyEffect(Visitor visitor) {

	}
}
