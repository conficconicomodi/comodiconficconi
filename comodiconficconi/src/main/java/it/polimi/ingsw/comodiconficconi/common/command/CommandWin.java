package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command sent when the game is finished
 * 
 * @author davide
 *
 */
public class CommandWin extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3798731723532795875L;
	private boolean win;

	/**
	 * 
	 * @param win
	 *            : true if the player win false otherwise
	 */
	public CommandWin(boolean win) {
		this.win = win;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public boolean getWin() {
		return win;
	}
}
