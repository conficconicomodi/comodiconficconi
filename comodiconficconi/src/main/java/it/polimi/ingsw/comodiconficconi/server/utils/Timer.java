package it.polimi.ingsw.comodiconficconi.server.utils;

import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;
import it.polimi.ingsw.comodiconficconi.server.ServerStarter;

import org.apache.log4j.Logger;

/**
 * A timer that when expired make a match start if there are the minimum number
 * of players
 * 
 * @author davide
 *
 */
public class Timer implements Runnable {
	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.utils.Timer.class);

	private ServerStarter creator;

	public Timer(ServerStarter creator) {
		this.creator = creator;
	}

	/**
	 * the effective timer
	 */
	@Override
	public void run() {

		try {
			Thread.sleep(ConstantNetwork.TIMER);
		} catch (InterruptedException e) {
			LOGGER.debug("Timer interrupted: " + e);
			return;
		}
		creator.timerExpired();

	}

}
