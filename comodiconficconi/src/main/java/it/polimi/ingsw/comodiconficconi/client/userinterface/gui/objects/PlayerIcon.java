package it.polimi.ingsw.comodiconficconi.client.userinterface.gui.objects;

import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.Hexagon;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import org.apache.log4j.Logger;

/**
 * This class saves the image of the player icon (Human or Alien) and at the
 * start of the game it is set in one of the base sectors depending on the
 * identity of the player
 * 
 * @author alessandro
 *
 */
public class PlayerIcon extends JComponent {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(PlayerIcon.class);

	private Point location;
	private transient Image icon;
	private int top;
	private int side;

	/**
	 * The constructor saves the image and the dimension that the image will
	 * have depending on the side of the hexagon
	 * 
	 * @param hex
	 * @param top
	 * @param side
	 * @param iconPath
	 */
	public PlayerIcon(Hexagon hex, int top, int side, String iconPath) {
		location = hex.getCenter();
		this.top = top;
		this.side = side;

		try {
			icon = ImageIO.read(this.getClass().getResource(iconPath));
		} catch (IOException e) {
			LOGGER.error(e);
		}
		icon = icon.getScaledInstance(side * 2 - 1, top * 2 - 1,
				Image.SCALE_SMOOTH);
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(icon, location.x - side + 1, location.y - top + 1, null);
	}

	/**
	 * This method changes the location of the player icon
	 */
	public void changePosition(Point newLocation) {
		location = newLocation;
	}
}
