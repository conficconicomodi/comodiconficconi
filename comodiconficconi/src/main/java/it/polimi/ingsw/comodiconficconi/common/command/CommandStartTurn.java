package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command sent to notify the start of the his turn
 * 
 * @author davide
 *
 */
public class CommandStartTurn extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5447676804471250335L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
