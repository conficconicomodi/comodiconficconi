package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * command used to send coordinates to the server
 * 
 * @author davide
 *
 */
public class CommandSendCoordinates extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8459327948185423722L;
	private Coordinates coordinates;

	public CommandSendCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
		commandHandlerServer.applyCommand(this);

	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

}
