package it.polimi.ingsw.comodiconficconi.gamemodel;

import java.util.List;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.gamemodel.deck.Deck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.SectorDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * This class serves as a utility class to help the match class in applying the
 * operations.
 * 
 * @author alessandro
 *
 */
public class MatchUtils {

	private static final Logger LOGGER = Logger.getLogger(MatchUtils.class);

	private MatchUtils() {
	}

	/**
	 * This method returns a new sector deck
	 * 
	 * @return
	 */
	public static Deck getNewSectorDeck() {
		return new SectorDeck(false);
	}

	/**
	 * This method returns a new item deck retrieving all the items from the
	 * item graveyard deck
	 * 
	 * @param itemDeck
	 * @param itemGraveyard
	 */
	public static void shuffleItemDeck(Deck itemDeck, Deck itemGraveyard) {
		while (!itemGraveyard.isEmpty())
			itemDeck.addCard(itemGraveyard.draw());
		itemDeck.shuffle();
	}

	/**
	 * This method returns the position in the list players of a given player
	 * 
	 * @param players
	 * @param player
	 * @return index corresponding on the position of the player in the list
	 */
	public static int getPositionOfAPlayer(List<Player> players, Player player) {
		for (int i = 0; i < players.size(); i++)
			if (players.get(i).equals(player)) {
				LOGGER.debug(i);
				return i;
			}
		return 0;
	}
}
