package it.polimi.ingsw.comodiconficconi.gamemodel.card;

/**
 * This class represent the abstract object of the character card of the game
 * 
 * @author davide
 *
 */
public abstract class CharacterCard extends Card {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7963138875739475210L;

}
