package it.polimi.ingsw.comodiconficconi.client.userinterface.cli;

/**
 * This class was made only to call the system out only once
 * 
 * @author alessandro
 *
 */
public class Printer {

	/**
	 * This is the hidden constructor so that this class can never be
	 * instantiated
	 */
	private Printer() {

	}

	/**
	 * This method calls the system.out.print with a string in input
	 * 
	 * @param string
	 */
	public static void print(String string) {
		System.out.print(string);
	}

	/**
	 * This methods prints a string and goes to a new line
	 * 
	 * @param string
	 */
	public static void println(String string) {
		System.out.println(string);
	}

	/**
	 * This method prints a new line
	 */
	public static void println() {
		System.out.println();
	}
}
