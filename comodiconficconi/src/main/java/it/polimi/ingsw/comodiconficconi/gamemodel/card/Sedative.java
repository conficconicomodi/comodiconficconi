package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * This card allow the player to ignore the draw phase when he goes on a
 * dangerous sector
 * 
 * @author davide
 *
 */
public class Sedative extends ItemCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8151054479590482746L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.SEDATIVE;
	}
}
