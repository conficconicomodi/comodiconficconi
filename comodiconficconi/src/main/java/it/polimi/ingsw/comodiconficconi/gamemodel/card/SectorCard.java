package it.polimi.ingsw.comodiconficconi.gamemodel.card;

/**
 * the abstract object sector card that is drawn when a player goes on a
 * dangerou sector
 * 
 * @author davide
 *
 */
public abstract class SectorCard extends Card {

	/**
	 * 
	 */
	private static final long serialVersionUID = 679926913820835229L;

}
