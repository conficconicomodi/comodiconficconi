package it.polimi.ingsw.comodiconficconi.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;
import it.polimi.ingsw.comodiconficconi.server.rmi.ServerStarterRMI;
import it.polimi.ingsw.comodiconficconi.server.socket.ServerStarterSocket;
import it.polimi.ingsw.comodiconficconi.server.utils.ServerObserver;
import it.polimi.ingsw.comodiconficconi.server.utils.Timer;

/**
 * This class start the server create the listener on socket and rmi channel,
 * when the number of connection reach the max number of players or the timer
 * expired create the match and reset the list of awaiting clients
 * 
 * @author davide
 *
 */
public class ServerStarter implements Runnable, ServerObserver {

	/**
	 * synchronized queue for all client that are waiting for a match
	 */
	private Deque<ClientHandler> waitingClients;
	/**
	 * 
	 * synchronized queue for all match that are on
	 */
	private Deque<MatchHandler> createdMatch;
	/**
	 * listener of socket connection
	 */
	private ServerStarterSocket socketListener;
	/**
	 * listener of rmi connection
	 */
	private ServerStarterRMI rmiListener;
	/**
	 * service to control all runnable submitted
	 */
	private ExecutorService executor;
	/**
	 * boolean used to read if the server has to been shut down
	 */
	private boolean goOn;
	private BufferedReader inKeyboard;
	private int port;
	private static ServerStarter instance;
	private Thread timer;

	private static final Logger LOGGER = Logger.getLogger(ServerStarter.class);

	private ServerStarter(int port) {
		this.port = port;
		inKeyboard = new BufferedReader(new InputStreamReader(System.in));
	}

	/**
	 * 
	 * @return the only possible instance of starter server
	 */
	public static synchronized ServerStarter getInstance() {
		if (instance == null) {
			return new ServerStarter(ConstantNetwork.PORT);
		} else
			return instance;
	}

	/**
	 * start server and listener
	 */
	@Override
	public void run() {
		startServer();

	}

	/**
	 * create the socket listener and rmi listener and listen itself if the
	 * server need to be shut down
	 */

	private void startServer() {
		executor = Executors.newCachedThreadPool();
		waitingClients = new ConcurrentLinkedDeque<ClientHandler>();
		createdMatch = new ConcurrentLinkedDeque<MatchHandler>();
		socketListener = new ServerStarterSocket(this, port);
		executor.submit(socketListener);
		rmiListener = new ServerStarterRMI(this);
		executor.submit(rmiListener);
		// the right procedure to close all that is on when closing the server
		goOn = true;
		LOGGER.warn("Press Q to exit the server");
		while (goOn) {
			String scelta = null;
			try {
				scelta = inKeyboard.readLine();
			} catch (IOException e) {
				LOGGER.debug("can't read");
				LOGGER.fatal(e);
			}

			if ("Q".equals(scelta) || "q".equals(scelta)) {
				goOn = false;
				suppress();
			} else
				LOGGER.warn("command not recognized");

		}
	}

	/**
	 * add a new client handler to the list of waiting client an if the timer
	 * expired or the queue reach the max number of a player for a match, launch
	 * a new match
	 * 
	 * @param clientHandler
	 *            :the client handler that is going to be added
	 */

	public synchronized void addClient(ClientHandler clientHandler) {
		if (waitingClients.size() == ConstantNetwork.MINPLAYERS - 1)
			createTimer();
		LOGGER.trace("adding a client");
		waitingClients.add(clientHandler);
		executor.submit(clientHandler);
		if (waitingClients.size() == ConstantNetwork.MAXPLAYERS) {
			LOGGER.trace("create a match");
			timer.interrupt();
			createMatch();
		}
	}

	/**
	 * this method check if in the list there are the minimum number of players
	 * 
	 * @return true if there are almost two players
	 */
	public synchronized boolean checkWaitingList() {
		return waitingClients.size() >= ConstantNetwork.MINPLAYERS;

	}

	/**
	 * this method is invoked by the timer and handle when it expired
	 */
	public synchronized void timerExpired() {
		if (checkWaitingList())
			createMatch();
	}

	/**
	 * this method let to create a timer
	 */
	private void createTimer() {
		LOGGER.trace("create timer");
		timer = new Thread(new Timer(this));
		timer.start();
	}

	/**
	 * this method create the match with all clients in waiting
	 */
	private synchronized void createMatch() {
		List<ClientHandler> list = new ArrayList<ClientHandler>();
		for (ClientHandler c : waitingClients)
			list.add(c);
		MatchHandler mc = new MatchHandler(list, this);
		executor.submit(mc);
		createdMatch.add(mc);
		waitingClients = new ConcurrentLinkedDeque<ClientHandler>();

	}

	/**
	 * this method closed all
	 */
	private void suppress() {
		closeWaitingList();
		if (timer != null && timer.isAlive())
			timer.interrupt();
		try {

			socketListener.endListening();
			inKeyboard.close();
			socketListener.endListening();
			rmiListener.endListening();
			closeMatches();
			LOGGER.debug("close all now closing executor");
			executor.shutdown();

		} catch (IOException e) {
			LOGGER.debug("error on closing");
			LOGGER.fatal(e);
		}
		try {
			LOGGER.debug("thread going to sleep");
			Thread.sleep(ConstantNetwork.SLEEPBEFORECLOSEALL);
		} catch (InterruptedException e) {
			LOGGER.fatal("process on closing error: \n" + e);
		}
		System.exit(ConstantNetwork.EXITSTATUS);
	}

	/**
	 * method called when the server is shutting down itself and than notify the
	 * closing connection to all waiting clients
	 */
	private void closeWaitingList() {
		LOGGER.trace("closing client that are waiting");
		if (!waitingClients.isEmpty())
			for (ClientHandler c : waitingClients)
				c.closeByServer();
	}

	/**
	 * this method close the match on
	 */
	private void closeMatches() {
		LOGGER.trace("closing match that are on");
		List<MatchHandler> matchToClose = new ArrayList<MatchHandler>();
		for (MatchHandler m : createdMatch)
			matchToClose.add(m);
		for (MatchHandler m : matchToClose)
			m.closeMatch();

	}

	@Override
	public void notifyAdd(ClientHandler c) {
		addClient(c);

	}

	@Override
	public synchronized void notifyClose(MatchHandler mh) {
		createdMatch.remove(mh);

	}

	@Override
	public synchronized void notifyRemove(ClientHandler c) {
		LOGGER.debug("removing client " + c.getCode());
		waitingClients.remove(c);
		if (!checkWaitingList() && timer != null)
			timer.interrupt();
	}

}
