package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * This class extends Player and creates a new Alien player
 * 
 * @author alessandro
 *
 */
public class AlienPlayer extends Player {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6691101652111948775L;

	public AlienPlayer(int id) {
		super(id);
		steps = ConstantPlayer.ALIENSTEPS;
		FSM = BuilderFSM.getAlienFSM();
	}

	@Override
	public boolean isHuman() {
		return false;
	}

	@Override
	public String toString() {
		return ConstantPlayer.ALIEN;
	}

	@Override
	public void resetPlayer() {
		FSM = BuilderFSM.getAlienFSM();
	}
}