package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;

import java.util.HashMap;
import java.util.Map;

/**
 * This class builds the various FSM of each character
 * 
 * @author alex
 *
 */
public class BuilderFSM {
	private static Map<Object, Integer> beforeMove;
	private static Map<Object, Integer> waitMoveCoordinates;
	private static Map<Object, Integer> solvedSector;
	private static Map<Object, Integer> needToSolveSector;
	private static Map<Object, Integer> waitNoiseCoordinates;
	private static Map<Object, Integer> endTurn;
	private static Map<Object, Integer> waitSpotlightCoord;
	private static Map<Object, Integer> waitDiscardItemAfterNoise;
	private static Map<Object, Integer> waitDiscardItemBeforeEndTurn;
	private static Map<Object, Integer> fullItemsAfterNoiseAnywhere;
	private static Map<Object, Integer> fullItemsBeforeEndTurn;

	/** Human FSM */
	private static Map<Integer, Map<Object, Integer>> humanFSM;
	/** Alien FSM */
	private static Map<Integer, Map<Object, Integer>> alienFSM;

	private BuilderFSM() {
	}

	/**
	 * This method builds the human FSM
	 * 
	 * @return hash map containing the human fsm
	 */
	public static Map<Integer, Map<Object, Integer>> getHumanFSM() {

		humanFSM = new HashMap<Integer, Map<Object, Integer>>();

		beforeMove = new HashMap<Object, Integer>();
		waitMoveCoordinates = new HashMap<Object, Integer>();
		solvedSector = new HashMap<Object, Integer>();
		needToSolveSector = new HashMap<Object, Integer>();
		waitNoiseCoordinates = new HashMap<Object, Integer>();
		endTurn = new HashMap<Object, Integer>();
		waitSpotlightCoord = new HashMap<Object, Integer>();
		waitDiscardItemAfterNoise = new HashMap<Object, Integer>();
		waitDiscardItemBeforeEndTurn = new HashMap<Object, Integer>();
		fullItemsAfterNoiseAnywhere = new HashMap<Object, Integer>();
		fullItemsBeforeEndTurn = new HashMap<Object, Integer>();

		beforeMove.put(ConstantFSM.MOVE, ConstantFSM.WAIT_MOVE_COORD);
		beforeMove.put(ConstantFSM.CARD, ConstantFSM.BEFORE_MOVE);

		waitMoveCoordinates.put(ConstantFSM.SEND_COORDINATES,
				ConstantFSM.WAIT_MOVE_COORD);

		solvedSector.put(ConstantFSM.END_TURN_COMMAND, ConstantFSM.BEFORE_MOVE);
		solvedSector.put(ConstantFSM.CARD, ConstantFSM.SOLVED_SECTOR);

		needToSolveSector.put(ConstantFSM.SOLVE_SECTOR,
				ConstantFSM.NEED_TO_SOLVE_SECTOR);
		needToSolveSector.put(ConstantFSM.CARD,
				ConstantFSM.NEED_TO_SOLVE_SECTOR);

		waitNoiseCoordinates.put(ConstantFSM.SEND_COORDINATES,
				ConstantFSM.END_TURN);

		endTurn.put(ConstantFSM.END_TURN_COMMAND, ConstantFSM.BEFORE_MOVE);
		endTurn.put(ConstantFSM.CARD, ConstantFSM.END_TURN);

		waitDiscardItemAfterNoise.put(ConstantFSM.CARD,
				ConstantFSM.WAIT_NOISE_COORDINATES);
		waitDiscardItemBeforeEndTurn
				.put(ConstantFSM.CARD, ConstantFSM.END_TURN);

		fullItemsAfterNoiseAnywhere.put(ConstantFSM.CARD,
				ConstantFSM.WAIT_NOISE_COORDINATES);
		fullItemsAfterNoiseAnywhere.put(ConstantFSM.DISCARD_ITEM,
				ConstantFSM.DISCARD_AFTER_NOISE_ANYWHERE);

		fullItemsBeforeEndTurn.put(ConstantFSM.CARD, ConstantFSM.END_TURN);
		fullItemsBeforeEndTurn.put(ConstantFSM.DISCARD_ITEM,
				ConstantFSM.DISCARD_BEFORE_END_TURN);

		humanFSM.put(ConstantFSM.BEFORE_MOVE, beforeMove);
		humanFSM.put(ConstantFSM.WAIT_MOVE_COORD, waitMoveCoordinates);
		humanFSM.put(ConstantFSM.SOLVED_SECTOR, solvedSector);
		humanFSM.put(ConstantFSM.NEED_TO_SOLVE_SECTOR, needToSolveSector);
		humanFSM.put(ConstantFSM.WAIT_NOISE_COORDINATES, waitNoiseCoordinates);
		humanFSM.put(ConstantFSM.END_TURN, endTurn);
		humanFSM.put(ConstantFSM.WAIT_SPOTLIGHT_COORDINATES, waitSpotlightCoord);
		humanFSM.put(ConstantFSM.DISCARD_AFTER_NOISE_ANYWHERE,
				waitDiscardItemAfterNoise);
		humanFSM.put(ConstantFSM.DISCARD_BEFORE_END_TURN,
				waitDiscardItemBeforeEndTurn);
		humanFSM.put(ConstantFSM.FULL_ITEMS_AFTER_NOISE_ANYWHERE,
				fullItemsAfterNoiseAnywhere);
		humanFSM.put(ConstantFSM.FULL_ITEMS_BEFORE_END_TURN,
				fullItemsBeforeEndTurn);

		return humanFSM;
	}

	/**
	 * This class builds the alien fsm
	 * 
	 * @return hash map of the alien fsm
	 */
	public static Map<Integer, Map<Object, Integer>> getAlienFSM() {

		alienFSM = new HashMap<Integer, Map<Object, Integer>>();

		beforeMove = new HashMap<Object, Integer>();
		waitMoveCoordinates = new HashMap<Object, Integer>();
		solvedSector = new HashMap<Object, Integer>();
		needToSolveSector = new HashMap<Object, Integer>();
		waitNoiseCoordinates = new HashMap<Object, Integer>();
		endTurn = new HashMap<Object, Integer>();
		waitDiscardItemAfterNoise = new HashMap<Object, Integer>();
		waitDiscardItemBeforeEndTurn = new HashMap<Object, Integer>();
		fullItemsAfterNoiseAnywhere = new HashMap<Object, Integer>();
		fullItemsBeforeEndTurn = new HashMap<Object, Integer>();

		beforeMove.put(ConstantFSM.MOVE, ConstantFSM.WAIT_MOVE_COORD);
		waitMoveCoordinates.put(ConstantFSM.SEND_COORDINATES,
				ConstantFSM.WAIT_MOVE_COORD);

		solvedSector.put(ConstantFSM.ATTACK, ConstantFSM.END_TURN);
		solvedSector.put(ConstantFSM.END_TURN_COMMAND, ConstantFSM.BEFORE_MOVE);

		needToSolveSector.put(ConstantFSM.ATTACK, ConstantFSM.END_TURN);
		needToSolveSector.put(ConstantFSM.SOLVE_SECTOR,
				ConstantFSM.NEED_TO_SOLVE_SECTOR);
		waitNoiseCoordinates.put(ConstantFSM.SEND_COORDINATES,
				ConstantFSM.END_TURN);

		endTurn.put(ConstantFSM.END_TURN_COMMAND, ConstantFSM.BEFORE_MOVE);

		waitDiscardItemAfterNoise.put(ConstantFSM.CARD,
				ConstantFSM.WAIT_NOISE_COORDINATES);
		waitDiscardItemBeforeEndTurn
				.put(ConstantFSM.CARD, ConstantFSM.END_TURN);

		fullItemsAfterNoiseAnywhere.put(ConstantFSM.DISCARD_ITEM,
				ConstantFSM.DISCARD_AFTER_NOISE_ANYWHERE);

		fullItemsBeforeEndTurn.put(ConstantFSM.DISCARD_ITEM,
				ConstantFSM.DISCARD_BEFORE_END_TURN);

		alienFSM.put(ConstantFSM.BEFORE_MOVE, beforeMove);
		alienFSM.put(ConstantFSM.WAIT_MOVE_COORD, waitMoveCoordinates);
		alienFSM.put(ConstantFSM.SOLVED_SECTOR, solvedSector);
		alienFSM.put(ConstantFSM.NEED_TO_SOLVE_SECTOR, needToSolveSector);
		alienFSM.put(ConstantFSM.WAIT_NOISE_COORDINATES, waitNoiseCoordinates);
		alienFSM.put(ConstantFSM.END_TURN, endTurn);
		alienFSM.put(ConstantFSM.FULL_ITEMS_AFTER_NOISE_ANYWHERE,
				fullItemsAfterNoiseAnywhere);
		alienFSM.put(ConstantFSM.FULL_ITEMS_BEFORE_END_TURN,
				fullItemsBeforeEndTurn);
		alienFSM.put(ConstantFSM.DISCARD_AFTER_NOISE_ANYWHERE,
				waitDiscardItemAfterNoise);
		alienFSM.put(ConstantFSM.DISCARD_BEFORE_END_TURN,
				waitDiscardItemBeforeEndTurn);

		return alienFSM;
	}
}
