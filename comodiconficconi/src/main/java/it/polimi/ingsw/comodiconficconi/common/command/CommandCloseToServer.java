package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command to notify that client is going to be closed
 * 
 * @author davide
 *
 */
public class CommandCloseToServer extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8016136687130067896L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
	}

}
