package it.polimi.ingsw.comodiconficconi.server.utils;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.server.ClientHandler;

/**
 * 
 * An interface of MatchHandler for client handler to allow the command for that
 * client
 * 
 * @author davide
 *
 */
public interface ObserverMatchHandler {
	/**
	 * this method will check and allow the command for that player
	 * 
	 * @param command
	 *            : the command received
	 * @param player
	 *            : the player the will use the command
	 * @return true if it is allowed false otherwise
	 */
	boolean isAllowed(CommandClientToServer command, Player player);

	/**
	 * this method remove from the match the client handler "clientHandler" and
	 * than the player
	 * 
	 * @param clientHandler
	 *            : the client handler going to be removed
	 */
	void removeClient(ClientHandler clientHandler);
}
