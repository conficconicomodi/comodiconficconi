package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command that want to notify the will to make an attack
 * 
 * @author davide
 *
 */
public class CommandAttack extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8740988247399148611L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {

		commandHandlerServer.applyCommand(this);
	}

}
