package it.polimi.ingsw.comodiconficconi.common.command;

import java.util.List;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * a command that can be sent to ask to make a move
 * 
 * @author davide
 *
 */
public class CommandAskMove extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6627552647344274768L;
	private List<String> actions;

	/**
	 * 
	 * @param actions
	 *            : the list of all possible actions
	 */
	public CommandAskMove(List<String> actions) {
		this.actions = actions;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public List<String> getActions() {
		return actions;
	}
}
