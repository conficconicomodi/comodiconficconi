package it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers;

/**
 * This interface serves as a listener for the Possible action panel
 * 
 * @author alessandro
 *
 */
public interface PossibleActionListener {
	/** notifies a move action to server */
	void notifyMove();

	/** notifies an attack action to server */
	void notifyAttack();

	/** notifies a pass turn action to server */
	void notifyPassTurn();

	/** notifies a draw card action to server */
	void notifyDrawCard();

	/** notifies a discard action to server */
	void notifyDiscardItem();
}
