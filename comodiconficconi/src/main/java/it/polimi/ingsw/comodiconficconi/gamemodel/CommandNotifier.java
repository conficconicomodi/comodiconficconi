package it.polimi.ingsw.comodiconficconi.gamemodel;

import java.util.Map;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * Interface used from match to notify someone who listen what is happening
 * 
 * @author davide
 * @author alessandro
 *
 */
public interface CommandNotifier {

	/**
	 * this method notify the need to set the next player
	 */
	public void notifySetNext();

	/**
	 * notifying the method to ask the coordinate for spotlight
	 */
	public void spotlightNotify();

	/**
	 * notifying the method to ask the coordinate for noise anywhere
	 */
	public void noiseAnyWhereNotify();

	/**
	 * this method makes sure that the player goes into the end state
	 */
	public void endStateNotify();

	/**
	 * * a generic notify to all user that are playing
	 * 
	 * @param message
	 *            : the message to all
	 */
	public void genericNotifyToAll(String message);

	/**
	 * this method will notify to all a message that contains coordinates
	 * 
	 * @param message
	 *            : the generic message
	 * @param coord
	 *            : the coordinates where happened something
	 */
	public void genericNotifyWithCoordinates(String message, Coordinates coord);

	/**
	 * This method notifies that the card that the player wants to use is not
	 * available
	 */
	public void notifyCannotUseCard();

	/**
	 * this method notifies to the current player that he has full item
	 */
	public void notifyFullItems();

	/**
	 * this method notifies that the current player has drawn an item card
	 * 
	 * @param item
	 *            card to add to the current player
	 */
	public void notifyNewItem(Card item);

	/**
	 * This methods notifies that the current player has to remove an item card
	 * 
	 * @param item
	 *            card that cannot be drawn
	 */
	public void notifyDiscardItem(Card item);

	/**
	 * This method notifies that a generic player used an item and has to remove
	 * it
	 * 
	 * @param item
	 *            that has to be removed
	 * @param player
	 *            that has to remove the used or discarded card
	 */
	public void notifyDiscardItem(Card item, Player player);

	/**
	 * This method notifies a new position of the player
	 */
	public void notifyNewPosition();

	/**
	 * This method is used to update the FSM of the current player so that he
	 * can perform an attack after his movement
	 *
	 */
	public void updateAttackFSM();

	/**
	 * This method is used to update the FSM of the current player so that if he
	 * goes in a dangerous sector he doesn't have to draw a sector card
	 * 
	 */
	public void updateSedativeFSM();

	/**
	 * method to get the hash map of the winners
	 * 
	 * @param winners
	 */
	public void notifyWinners(Map<Player, Boolean> winners);

	/**
	 * this method will notify to the current player that he escaped and to the
	 * others that the current player has escaped
	 */
	public void notifyEscaped();

	/**
	 * notify that a player has been killed
	 * 
	 * @param player
	 *            : the player that will be notified of his death
	 */
	public void notifyDeath(Player player);

}
