package it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * This interface gives a method to notify that a sector of the board has been
 * clicked
 * 
 * @author alessandro
 *
 */
public interface GameBoardListener {

	/**
	 * This method notifies a new clicked sector to the game controller (and all
	 * the possible listeners)
	 * 
	 * @param coord
	 */
	void notify(Coordinates coord);
}
