package it.polimi.ingsw.comodiconficconi.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * this is the ClientHandler interface for RMI
 * 
 * @author davide
 *
 */
public interface RemoteClientHandler extends Remote {
	/**
	 * this method will notify the new command to the server
	 * 
	 * @param c
	 * @throws RemoteException
	 */
	public void notifyNewCommand(CommandClientToServer c)
			throws RemoteException;

	/**
	 * this method the client the list of waiting clients
	 * 
	 * @param port
	 * @throws RemoteException
	 */
	public void addClient(int port) throws RemoteException;

	/**
	 * this will close connection
	 * 
	 * @throws RemoteException
	 */
	public void closeByServer() throws RemoteException;

}
