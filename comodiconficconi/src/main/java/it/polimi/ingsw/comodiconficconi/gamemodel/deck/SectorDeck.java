package it.polimi.ingsw.comodiconficconi.gamemodel.deck;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;

/**
 * the class of the sector deck
 * 
 * @author davide
 *
 */
public class SectorDeck extends Deck {

	/**
	 * construct the deck empty or full as the default value of the game, the
	 * deck is already shuffled
	 * 
	 * @param empty
	 *            : true if you want to build an empty deck false if you want to
	 *            get a default built deck
	 */
	public SectorDeck(boolean empty) {
		super();
		if (!empty) {
			for (int i = 0; i < ConstantCards.MAXNOISEANYWHERECARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.NOISEANYWHERECODE));

			}

			for (int i = 0; i < ConstantCards.MAXNOISEANYWHEREWITHITEMCARD; i++) {
				cards.add(FactoryCard
						.getCard(ConstantCards.NOISEANYWHEREWITHITEMCODE));

			}

			for (int i = 0; i < ConstantCards.MAXNOISEINSECTORCARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.NOISEINSECTORCODE));

			}

			for (int i = 0; i < ConstantCards.MAXNOISEINECTORWITHITEMCARD; i++) {
				cards.add(FactoryCard
						.getCard(ConstantCards.NOISEINSECTORWITHITEMCODE));

			}

			for (int i = 0; i < ConstantCards.MAXSILENCECARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.SILENCECODE));

			}
			this.shuffle();
		}
	}

}