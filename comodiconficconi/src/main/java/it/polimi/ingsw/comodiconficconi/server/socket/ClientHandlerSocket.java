package it.polimi.ingsw.comodiconficconi.server.socket;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToServer;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotValid;
import it.polimi.ingsw.comodiconficconi.server.ClientHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ObserverMatchHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ServerObserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * the handler of a single client connected on socket; it received all command
 * from a client and notify to the superior that a new command need to be
 * handled
 * 
 * @author davide
 *
 */
public class ClientHandlerSocket extends ClientHandler {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.socket.ClientHandlerSocket.class);

	private Socket socket;
	private ObjectInputStream inSocket;
	private ObjectOutputStream outSocket;
	private int clientNumber;
	private CommandHandlerServer dispatcher;
	private ObserverMatchHandler allower;
	private ServerObserver creator;

	public ClientHandlerSocket(Socket socket, int number,
			ServerObserver serverStarter) {
		LOGGER.trace("client handler socket");
		allower = null;
		clientNumber = number;
		this.socket = socket;
		code = hashCode();
		creator = serverStarter;
		closed = false;
		try {
			inSocket = new ObjectInputStream(this.socket.getInputStream());
			outSocket = new ObjectOutputStream(this.socket.getOutputStream());
			outSocket.flush();
		} catch (IOException e) {
			LOGGER.debug("cannot open comunication channel");
			LOGGER.fatal(e);
			closeByServer();
		}

	}

	@Override
	public void run() {
		CommandClientToServer command;
		while (true) {
			command = null;
			try {
				command = (CommandClientToServer) inSocket.readObject();

			} catch (ClassNotFoundException | IOException e) {
				LOGGER.fatal(e);
				close();
				break;
			}
			if (command instanceof CommandCloseToServer)
				closeByClient();
			else if (allower != null && allower.isAllowed(command, player)) {
				LOGGER.debug("command allowed");
				notifyNewCommand(command);
			} else if (!allower.isAllowed(command, player) || allower == null) {
				try {
					LOGGER.debug("command not valid");
					sendCommand(new CommandNotValid());
				} catch (IOException e) {
					LOGGER.fatal(e);
					closeByClient();
				}
			}
		}
	}

	@Override
	public void sendCommand(CommandServerToClient command) throws IOException {
		LOGGER.debug("write command" + command);
		outSocket.writeObject(command);
		outSocket.flush();
	}

	@Override
	public void notifyNewCommand(CommandClientToServer command) {
		LOGGER.debug("command " + command + "going to be applied");
		command.useCommand(dispatcher);
	}

	@Override
	public void closeByServer() {
		LOGGER.trace("close by server");
		try {
			outSocket.writeObject(new CommandCloseToClient());
		} catch (IOException e) {
			LOGGER.debug(e);
		}
		close();
	}

	@Override
	public void closeByClient() {
		LOGGER.trace("close by client");
		if (allower != null)
			allower.removeClient(this);
		else
			creator.notifyRemove(this);
		close();
	}

	private void close() {
		if (!closed) {
			LOGGER.debug("going to close client handler" + clientNumber);
			try {
				closed = true;
				socket.close();
				inSocket.close();
				outSocket.close();
			} catch (Exception e) {
				LOGGER.debug("can not close ");
				LOGGER.fatal(e);
			}
		}
	}

	@Override
	public void addAllower(ObserverMatchHandler allower) {
		this.allower = allower;
	}

	@Override
	public void addCommandHandler(CommandHandlerServer commandHandler) {
		dispatcher = commandHandler;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + clientNumber;
		result = prime * result + ((socket == null) ? 0 : socket.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientHandlerSocket other = (ClientHandlerSocket) obj;
		if (clientNumber != other.clientNumber)
			return false;
		if (socket == null) {
			if (other.socket != null)
				return false;
		} else if (!socket.equals(other.socket))
			return false;
		return true;
	}

}
