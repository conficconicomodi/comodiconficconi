package it.polimi.ingsw.comodiconficconi.client.userinterface;

import it.polimi.ingsw.comodiconficconi.client.controller.ClientController;
import it.polimi.ingsw.comodiconficconi.client.userinterface.cli.CommandLineInterface;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.GraphicInterface;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;

/**
 * This factory returns two kinds of user interface depending on the type chosen
 * from the user
 * 
 * @author alessandro
 *
 */
public class UserInterfaceFactory {

	private UserInterfaceFactory() {
	}

	/**
	 * This method builds a new kind of user interface (CLI or GUI)
	 * 
	 * @param type
	 * @param gameController
	 * @return
	 */
	public static UserInterface getUserInterface(int type,
			ClientController gameController) {
		if (type == ConstantClient.CLI)
			return new CommandLineInterface(gameController);
		else if (type == ConstantClient.GUI)
			return new GraphicInterface(gameController);
		else
			return new CommandLineInterface(gameController);
	}
}
