package it.polimi.ingsw.comodiconficconi.constants;

/**
 * the constant needed for network
 * 
 * @author davide
 *
 */
public class ConstantNetwork {

	/**
	 * port of socket channel
	 */
	public final static int PORT = 8888;
	/**
	 * address of socket channel
	 */
	public final static String ADDRESS = "localhost";
	/**
	 * the initial port of client on rmi
	 */
	public static final int RMICLIENTPORT = 3030;
	/**
	 * the rmi client handler port
	 */
	public static final int RMICLIENTHANDLERPORT = 4040;
	/**
	 * the max numbers of players
	 */
	public static final int MAXPLAYERS = 8;
	/**
	 * the minum number of players
	 */
	public static final int MINPLAYERS = 2;
	/**
	 * the duration of the timer
	 */
	public static final long TIMER = 45000;
	/**
	 * the max duration of a turn for a player
	 */
	public static final long TURNTIMER = 90000;
	
	/**
	 * a constant like a timer
	 */
	public static final long SLEEPBEFORECLOSEALL = 2000;
	
	/**
	 * the ID for socket interface
	 */
	public static final int SOCKETINTERFACE = 1;
	/**
	 * the ID for RMI interface
	 */
	public static final int RMIINTERFACE = 2;
	/**
	 * the status on which invoke the system exit
	 */
	public static final int EXITSTATUS = 0;

	private ConstantNetwork() {
	}

}
