package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * the sector card that make the player declare : "Silence"
 * 
 * @author davide
 *
 */
public class Silence extends SectorCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3607728498001407760L;

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.SILENCE;
	}
}
