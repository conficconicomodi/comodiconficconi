package it.polimi.ingsw.comodiconficconi.constants;
/**
 * This class defines the constants for a match
 * @author alessandro
 *
 */
public class ConstantMatch {
	
	public static final int MAXTURNS = 39;
	
	private ConstantMatch(){
	}
}
