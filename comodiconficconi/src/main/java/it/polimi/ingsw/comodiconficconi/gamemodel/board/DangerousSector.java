package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * This is class is used to instantiate a dangerous sector
 * 
 * @author alessandro
 * 
 */
public class DangerousSector extends Sector {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5240600697393883250L;

	@Override
	public boolean canMoveIn() {
		return true;
	}

	@Override
	public String toString() {
		return ConstantBoard.DANGEROUSSECTOR;
	}

	@Override
	public boolean hasToDraw() {
		return true;
	}

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.drawSectorCard();
	}
}
