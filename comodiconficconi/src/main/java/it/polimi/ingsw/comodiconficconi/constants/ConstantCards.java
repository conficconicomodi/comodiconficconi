package it.polimi.ingsw.comodiconficconi.constants;

/**
 * this class contains the constants for cards and decks
 * 
 * @author davide
 *
 */
public class ConstantCards {

	public static final String DEFENSE = "defense";
	public static final String TELEPORT = "teleport";
	public static final String ATTACK = "attack";
	public static final String ADRENALINE = "adrenaline";
	public static final String SEDATIVE = "sedative";
	public static final String SPOTLIGHT = "spotlight";
	
	public static final String BLOCKEDHATCH = "blockedhatch";
	public static final String OPENHATCH = "openhatch";
	
	public static final String SILENCE = "silence";
	public static final String NOISEANYWHERE = "noiseanywhere";
	public static final String NOISEINSECTOR = "noiseinsector";

	
	
	// ITEM
	public static final int MAXITEMCARD = 12;

	// adrenaline
	public static final int ADRENALINECODE = 10;
	public static final int MAXADRENALINECARD = 2;

	// attack
	public static final int ATTACKCODE = 11;
	public static final int MAXATTACKCARD = 2;

	// defense
	public static final int DEFENSECODE = 12;
	public static final int MAXDEFENSECARD = 1;

	// sedative
	public static final int SEDATIVECODE = 13;
	public static final int MAXSEDATIVECARD = 3;

	// spotlight
	public static final int SPOTLIGHTCODE = 14;
	public static final int MAXSPOTLIGHTCARD = 2;

	// teleport
	public static final int TELEPORTCODE = 15;
	public static final int MAXTELEPORTCARD = 2;



	// SECTOR
	public static final int MAXSECTORCARD = 25;

	// noise any where
	public static final int NOISEANYWHERECODE = 20;
	public static final int MAXNOISEANYWHERECARD = 6;
	public static final int NOISEANYWHEREWITHITEMCODE = 21;
	public static final int MAXNOISEANYWHEREWITHITEMCARD = 4;

	// noise in sector
	public static final int NOISEINSECTORCODE = 22;
	public static final int MAXNOISEINSECTORCARD = 6;
	public static final int NOISEINSECTORWITHITEMCODE = 23;
	public static final int MAXNOISEINECTORWITHITEMCARD = 4;

	// silence
	public static final int SILENCECODE = 24;
	public static final int MAXSILENCECARD = 5;

	// CHARCARCTER
	public static final int MAXCHARACTERCARD = 8;

	// alien
	public static final int ALIENCODE = 30;
	public static final int MAXALIENCARD = 4;

	// human
	public static final int HUMANCODE = 31;
	public static final int MAXHUMANCARD = 4;
	
	// HATCH
	public static final int MAXHATCCARD = 6;
	// open
	public static final int OPENHATCHCODE = 40;
	public static final int MAXOPENHATCHCARD = 3;
	// closed
	public static final int BLOCKEDHATCHCODE = 41;
	public static final int MAXBLOCKEDHATCHCARD = 3;

	// DECK
	public static final int CHARACTERDECK = 0;
	public static final int EMPTYCHARACTERDECK = 1;
	public static final int ITEMDECK = 2;
	public static final int EMPTYITEMDECK = 3;
	public static final int SECTORDECK = 4;
	public static final int EMPTYSECTORDECK = 5;
	public static final int HATCHDECK = 6;
	public static final int EMPTYHATCHDECK = 7;
	//Character card
	public static final String[] ALIENNAMEVALUE = {
		"Primo Alieno : Piero Ceccarella",
		"Secondo Alieno : Vittorio Martana",
		"Terzo Alieno : Maria Galbani", "Quarto Alieno : Paolo London" };
	
	public static final String[] HUMANNAMEVALUE = {
		"Il Capitano : Ennio Maria Dominoni",
		"Il Pilota : Julia Niguloti a.k.a. \"Cabal\"",
		"Lo Psicologo : Silvano Porpora",
		"Il Soldato : Truccio Brendon a.k.a. \"Il Pirri\"" };





	private ConstantCards() {

	}
}
