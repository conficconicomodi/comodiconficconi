package it.polimi.ingsw.comodiconficconi.common;

import it.polimi.ingsw.comodiconficconi.common.command.CommandAttack;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDiscardItem;
import it.polimi.ingsw.comodiconficconi.common.command.CommandEndTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSendCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSolveSector;

/**
 * this class will be invoked by a command to be interpreted and handle command
 * 
 * @author davide
 *
 */
public interface CommandHandlerServer {
	/**
	 * this method will validate the move if it is valid
	 * 
	 * @param move
	 *            that the player would do
	 */
	void applyCommand(CommandMove move);

	/**
	 * this method will apply the attack
	 * 
	 * @param attack
	 */

	void applyCommand(CommandAttack attack);

	/**
	 * this method will pass the turn of current player
	 * 
	 * @param endTurn
	 */
	void applyCommand(CommandEndTurn endTurn);

	/**
	 * this method will ask to the server to use a card
	 * 
	 * @param card
	 */
	void applyCommand(CommandCard card);

	/**
	 * notifying coordinates to server
	 * 
	 * @param sendCoordinates
	 */
	void applyCommand(CommandSendCoordinates sendCoordinates);

	/**
	 * notifying solving sector to server
	 * 
	 * @param solveSector
	 */
	void applyCommand(CommandSolveSector solveSector);

	/**
	 * notifying that the client wants to discard an item
	 * 
	 * @param discardItem
	 */
	void applyCommand(CommandDiscardItem discardItem);
}
