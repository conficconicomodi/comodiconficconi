package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * 
 * This class is used to instantiate escape hatch sectors.
 * 
 * @author alessandro
 * 
 */
public class EscapeHatchSector extends Sector {

	/**
	 * 
	 */
	private static final long serialVersionUID = -534605041961300480L;
	private boolean isOpen = true;

	@Override
	public String toString() {
		return ConstantBoard.ESCAPEHATCHSECTOR;
	}

	/**
	 * once this method is called the hatch will always be closed.
	 */
	private void setClosed() {
		isOpen = false;
	}

	@Override
	public boolean canMoveIn() {
		return isOpen;
	}

	@Override
	public boolean hasToDraw() {
		return true;
	}

	@Override
	public void applyEffect(Visitor visitor) {
		setClosed();
		visitor.visitDrawHatchCard();
	}
}