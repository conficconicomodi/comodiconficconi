package it.polimi.ingsw.comodiconficconi.common;

import java.io.Serializable;

/**
 * This class represent generic command that will be sent from server to client,
 * it contains the information that client need to interact with the user
 * 
 * @author davide
 *
 */
public abstract class CommandServerToClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 519141823354315585L;

	/**
	 * Method to process the information from the command
	 * 
	 * @param commandHandlerClient
	 *            : the client handler of the command who parse and apply the
	 *            command
	 */
	public abstract void useCommand(CommandHandlerClient commandHandlerClient);
}
