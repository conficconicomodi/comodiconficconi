package it.polimi.ingsw.comodiconficconi.server.utils;

import it.polimi.ingsw.comodiconficconi.server.ClientHandler;
import it.polimi.ingsw.comodiconficconi.server.MatchHandler;

/**
 * this class used by accepter of connection to notify a new client it's
 * connected or to remove, or to notify the closure of a match
 * 
 * @author davide
 *
 */
public interface ServerObserver {
	/**
	 * this let the accepter to add ClientHandler "clientHandler" to the waiting
	 * list for a match
	 * 
	 * @param clientHandler
	 *            : the clientHandler that is connected
	 */
	public void notifyAdd(ClientHandler clientHandler);

	/**
	 * this method remove from the waiting queue for a match the client that
	 * close connection
	 * 
	 * @param clientHandler
	 *            : the clientHandler that is going to be removed
	 */
	public void notifyRemove(ClientHandler clientHandler);

	/**
	 * this will notify the closure of a match
	 * 
	 * @param matchHandler
	 *            : the matchHandler closed
	 */
	public void notifyClose(MatchHandler matchHandler);

}
