package it.polimi.ingsw.comodiconficconi.client.network.utils;

import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * Like the observer pattern interface but used to be notified for a new command
 * 
 * @author davide
 *
 */
public interface ObserverCommandToClient {
	/**
	 * method invoked by the subject to notify the observer that a new command
	 * for client is received
	 * 
	 * @param command
	 */
	void update(CommandServerToClient command);
}
