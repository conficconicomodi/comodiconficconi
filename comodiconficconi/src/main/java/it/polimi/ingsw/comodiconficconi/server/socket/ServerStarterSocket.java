package it.polimi.ingsw.comodiconficconi.server.socket;

import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;
import it.polimi.ingsw.comodiconficconi.server.ClientHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ServerObserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * this class can accept all clients that require to connect on socket channel
 * 
 * @author davide
 *
 */
public class ServerStarterSocket implements Runnable {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.socket.ServerStarterSocket.class);

	private int port;
	private ServerSocket server;
	private boolean listening;
	private ServerObserver creator;

	public ServerStarterSocket(ServerObserver serverStarter) {
		LOGGER.trace("Creating ServerSocket listener");
		port = ConstantNetwork.PORT;
		listening = false;
		creator = serverStarter;
	}

	public ServerStarterSocket(ServerObserver serverStarter, int port) {
		LOGGER.trace("Creating ServerSocket listener with setted port and address");
		this.port = port;
		listening = false;
		creator = serverStarter;
	}

	@Override
	public void run() {
		try {
			startListening();
		} catch (IOException e) {

			LOGGER.debug(e + "\n Listening failure");
			try {
				endListening();
			} catch (IOException e2) {
				LOGGER.debug(e2);
			}
		}
	}

	/**
	 * this method create a server socket and accept all connection
	 * 
	 * @throws IOException
	 */
	private void startListening() throws IOException {

		LOGGER.trace("creating the listener");
		server = new ServerSocket(port);
		listening = true;
		int i = 0;// number of clients accepted
		while (listening) {
			Socket socket = server.accept();
			ClientHandler c = new ClientHandlerSocket(socket, i, creator);
			creator.notifyAdd(c);
			LOGGER.trace("new client accepted and added to waiting list now going to start it");
			i++;//
		}

	}

	/**
	 * this method closed all waiting ClientHandlerSocket and the server socket
	 * 
	 * @throws IOException
	 */
	public void endListening() throws IOException {
		if (listening) {
			LOGGER.trace("start closing waiting socket connection");
			listening = false;
			LOGGER.trace("try to close serverSocket");
			server.close();
		}
	}

}
