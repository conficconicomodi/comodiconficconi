package it.polimi.ingsw.comodiconficconi.server.rmi;

import it.polimi.ingsw.comodiconficconi.common.RemoteClientHandler;
import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;
import it.polimi.ingsw.comodiconficconi.server.ClientHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ServerObserver;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.Logger;

/**
 * this class will put on remote the client handler interface and add the new
 * client handler created when someone connect
 * 
 * @author davide
 *
 */
public class ServerStarterRMI implements Runnable {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.rmi.ServerStarterRMI.class);

	private ServerObserver creator;
	private Registry registry = null;
	private String name = "ClientHandler";
	private static int number = 0;
	private ClientHandlerRMI handlerRMI;
	private RemoteClientHandler clientHandler;

	public ServerStarterRMI(ServerObserver so) {
		LOGGER.trace("listener for rmi");
		creator = so;
	}

	@Override
	public void run() {
		createClientHandler();
	}

	/**
	 * the first creation of a client handler
	 */
	private void createClientHandler() {
		handlerRMI = new ClientHandlerRMI(this, number);
		try {
			LOGGER.trace("exporting client handler rmi");
			clientHandler = handlerRMI;
			RemoteClientHandler stub = (RemoteClientHandler) UnicastRemoteObject
					.exportObject(clientHandler, 0);
			registry = LocateRegistry
					.createRegistry(ConstantNetwork.RMICLIENTHANDLERPORT);
			registry.bind(name, stub);
			number++;
		} catch (Exception e) {
			LOGGER.fatal("error on exporting" + e);
			endListening();
		}
	}

	/**
	 * creation of a client when the first one was already created
	 */
	private void newClient() {
		try {
			handlerRMI = new ClientHandlerRMI(this, number);
			LOGGER.trace("exporting another client handler rmi");
			clientHandler = handlerRMI;
			RemoteClientHandler stub = (RemoteClientHandler) UnicastRemoteObject
					.exportObject(clientHandler, 0);
			registry.rebind(name, stub);
			number++;
		} catch (RemoteException e) {
			LOGGER.fatal(e);
		}
	}

	/**
	 * method that add a client handler rmi to the waiting list for match and
	 * create a new client handler rmi
	 * 
	 * @param clientHandlerRMI
	 */
	public void addClient(ClientHandlerRMI clientHandlerRMI) {
		creator.notifyAdd(clientHandlerRMI);
		newClient();
	}

	/**
	 * close the listener of RMI connection
	 */
	public synchronized void endListening() {
		try {
			LOGGER.trace("close rmi listener");
			registry.unbind(name);
		} catch (RemoteException | NotBoundException e) {
			LOGGER.fatal(e);
		}
		registry = null;
	}

	/**
	 * method invoked when the client handler ask to remove it from the waiting
	 * list
	 * 
	 * @param clientHandler
	 *            : the client handler that ask for remove
	 */
	public void removeWaitingClient(ClientHandler clientHandler) {
		creator.notifyRemove(clientHandler);
	}
}
