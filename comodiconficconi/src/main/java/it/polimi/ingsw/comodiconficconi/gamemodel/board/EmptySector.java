package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;

/**
 * This class instantiates a new empty sector
 * 
 * @author alex
 *
 */
public class EmptySector extends Sector {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4196374134450727802L;

	@Override
	public boolean canMoveIn() {
		return false;
	}

	@Override
	public String toString() {
		return ConstantBoard.EMPTYSECTOR;
	}

	@Override
	public boolean hasToDraw() {
		return false;
	}
}
