package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * This is the spotlight item that reveal if there are players in a sector and
 * his neighbors
 * 
 * @author davide
 *
 */
public class SpotLight extends ItemCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5988684613426620458L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.SPOTLIGHT;
	}
}
