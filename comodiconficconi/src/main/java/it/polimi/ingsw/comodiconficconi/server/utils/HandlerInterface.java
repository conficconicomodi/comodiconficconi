package it.polimi.ingsw.comodiconficconi.server.utils;

/**
 * this is an interface that handle the expiration of the turn timer
 * 
 * @author davide
 *
 */
public interface HandlerInterface {

	/**
	 * method that that notify the timer for current turn is expired
	 */
	public void turnTimerExpired();
}
