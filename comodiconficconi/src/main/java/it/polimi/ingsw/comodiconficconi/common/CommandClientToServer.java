package it.polimi.ingsw.comodiconficconi.common;

import java.io.Serializable;

/**
 * This class represent generic command that will be sent from client to
 * server, it contains the information that server need to give the service to the client
 * 
 * @author davide
 *
 */
public abstract class CommandClientToServer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2161077951650417101L;

	/**
	 * Method to process the information from the command
	 * 
	 * @param commandHandlerServer
	 *            : the server handler of the command who parse and apply the
	 *            command
	 */
	public abstract void useCommand(CommandHandlerServer commandHandlerServer);

}
