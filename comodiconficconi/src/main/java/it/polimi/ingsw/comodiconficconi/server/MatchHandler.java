package it.polimi.ingsw.comodiconficconi.server;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAddCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordNoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordSpotLight;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDeath;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDisconnection;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNewPosition;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotification;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotificationWithCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandRemoveCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartMatch;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandWin;
import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;
import it.polimi.ingsw.comodiconficconi.gamemodel.CommandNotifier;
import it.polimi.ingsw.comodiconficconi.gamemodel.Match;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.server.utils.HandlerInterface;
import it.polimi.ingsw.comodiconficconi.server.utils.ObserverMatchHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ServerObserver;
import it.polimi.ingsw.comodiconficconi.server.utils.TurnTimer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 * this class builds the match and handle single connections with clients and
 * the match
 * 
 * @author davide
 * @author alessandro
 *
 */
public class MatchHandler implements Runnable, ObserverMatchHandler,
		CommandNotifier, HandlerInterface {

	private List<ClientHandler> clients;
	private List<ClientHandler> closedClients;
	private CommandHandlerServer commandHandler;
	private ServerObserver serverObserver;
	private Match match;
	private TurnTimer timer;
	private Thread timerThread;

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.MatchHandler.class);

	/**
	 * This constructor creates the clients and sets the observer to notify the
	 * server
	 * 
	 * @param clients
	 *            connected to the game
	 * @param serverObserver
	 *            main server to which will be notified the end of the game
	 */
	public MatchHandler(List<ClientHandler> clients,
			ServerObserver serverObserver) {
		LOGGER.trace("creating MatchHandler ");
		this.clients = clients;
		this.serverObserver = serverObserver;
		closedClients = new ArrayList<ClientHandler>();
	}

	@Override
	public void run() {
		initiazializeMatch();
	}

	private void initiazializeMatch() {
		String map = createIdMap();
		LOGGER.debug("Initializing match");
		match = new Match(map, clients.size());
		match.setNotifier(this);
		commandHandler = new CommandHandlerServerImpl(this, match);
		setPlayers();
		notifyAllStartMatch();
		match.setInitialPlayer();
		startTurn();
	}

	private void sendCommand(ClientHandler client, CommandServerToClient command) {
		try {
			client.sendCommand(command);
		} catch (Exception e) {
			LOGGER.fatal(e);
			closedClients.add(client);
		}
		checkDisconnection();
	}

	/**
	 * method to get a casual map
	 * 
	 * @return the string that represent the correct map
	 */
	private String createIdMap() {
		List<String> maps = new ArrayList<String>();
		maps.add(ConstantBoard.GALILEI);
		maps.add(ConstantBoard.GALVANI);
		maps.add(ConstantBoard.FERMI);
		if (clients.size() > 6)
			maps.remove("fermi");
		Collections.shuffle(maps);
		return maps.get(0);
	}

	/**
	 * 
	 * @return the current id player
	 */
	public int getCurrentIdPlayer() {
		return match.getCurrentPlayer().getPlayerId();
	}

	/**
	 * 
	 * @return the current player of the match
	 */
	public Player getCurrentPlayer() {
		return match.getCurrentPlayer();
	}

	/**
	 * set players and their client handler for start the match;
	 */
	private void setPlayers() {
		Collections.shuffle(clients);
		int i = 1;
		for (ClientHandler c : clients) {
			c.addPlayer(match.setPlayer(i));
			c.addAllower(this);
			c.addCommandHandler(commandHandler);
			i++;
		}
	}

	/**
	 * method invoked at the end of a turn that checks if the game is end or set
	 * the next player
	 */
	public void setNext() {
		LOGGER.trace("set next");
		Map<Player, Boolean> winners = match.checkWinners();
		if (winners.isEmpty() && !clients.isEmpty()) {
			LOGGER.trace("no winners e ci sono client connessi");
			match.setNextPlayer();
			startTurn();
		} else if (!winners.isEmpty() && !clients.isEmpty())
			notifyEndOfGame(winners);
	}

	private void startTurn() {
		notifyCurrentPlayer(new CommandStartTurn());
		notifyCurrentPlayer(new CommandAskMove(getPossibleActions()));
		createTurnTimer();
	}

	@Override
	public boolean isAllowed(CommandClientToServer command, Player player) {
		Player currentPlayer = getCurrentPlayer();
		int playerState = currentPlayer.getState();
		if (currentPlayer.equals(player)
				&& player.getFSM().get(playerState)
						.containsKey(command.getClass()))
			return true;

		return false;
	}

	// Method of notification
	/**
	 * method invoked that notify the start of the match
	 */
	private void notifyAllStartMatch() {
		for (ClientHandler client : clients)
			try {
				client.sendCommand(new CommandStartMatch(match.getBoard()
						.getBoard(), client.getPlayer(), match.getBoard()
						.getNameMap()));
			} catch (Exception e) {
				LOGGER.fatal(e);
				closedClients.add(client);
			}
		checkDisconnection();
	}

	/**
	 * notifying command in broadcast
	 * 
	 * @param command
	 *            to be notified
	 */
	public void notifyAllClients(CommandServerToClient command) {
		LOGGER.trace("notify all clients");
		for (ClientHandler client : clients) {
			try {
				client.sendCommand(command);
			} catch (Exception e) {
				LOGGER.fatal(e);
				closedClients.add(client);
			}
		}
		checkDisconnection();
	}

	@Override
	public void spotlightNotify() {
		notifyCurrentPlayer(new CommandAskCoordSpotLight());
		int playerState = getCurrentState();
		getCurrentPlayer().getFSM().get(ConstantFSM.WAIT_SPOTLIGHT_COORDINATES)
				.put(ConstantFSM.SEND_COORDINATES, playerState);
		setCurrentState(ConstantFSM.WAIT_SPOTLIGHT_COORDINATES);
	}

	@Override
	public void noiseAnyWhereNotify() {
		setCurrentState(ConstantFSM.WAIT_NOISE_COORDINATES);
		notifyCurrentPlayer(new CommandAskCoordNoiseAnywhere());

	}

	@Override
	public void genericNotifyToAll(String message) {
		LOGGER.trace("generic notify to all");
		this.notifyAllClients(new CommandNotification(message));

	}

	@Override
	public void notifyDeath(Player player) {
		ClientHandler clientHandler = null;
		for (ClientHandler client : clients)
			if (client.getPlayer().equals(player)) {
				clientHandler = client;
				sendCommand(client, new CommandDeath());
				client.closeByServer();
			}
		clients.remove(clientHandler);
		checkDisconnection();
		LOGGER.trace("notify to all the death");
		notifyAllClients(new CommandNotification(player.toString()
				.toUpperCase() + " player " + player.getPlayerId() + " died"));
	}

	@Override
	public void genericNotifyWithCoordinates(String message, Coordinates coord) {
		notifyAllClients(new CommandNotificationWithCoordinates(message, coord));
	}

	@Override
	public void notifyEscaped() {
		LOGGER.trace("notify escaped");
		ClientHandler clientHandler = null;
		Player player = getCurrentPlayer();
		for (ClientHandler client : clients)
			if (client.getPlayer().equals(player)) {
				sendCommand(client, new CommandWin(true));
				client.closeByServer();
				clientHandler = client;
			}
		clients.remove(clientHandler);
		notifyAllClients(new CommandNotificationWithCoordinates("Player "
				+ getCurrentIdPlayer() + " escaped in: ",
				player.getCurrentSector()));

		timerNotNeed();
	}

	@Override
	public void notifyFullItems() {
		notifyCurrentPlayer(new CommandNotification(
				"Your item deck is full, you need to discard or use an item card"));
		if (getCurrentState() == ConstantFSM.WAIT_NOISE_COORDINATES)
			setCurrentState(ConstantFSM.FULL_ITEMS_AFTER_NOISE_ANYWHERE);
		else if (getCurrentState() == ConstantFSM.END_TURN)
			setCurrentState(ConstantFSM.FULL_ITEMS_BEFORE_END_TURN);
		notifyCurrentPlayer(new CommandAskMove(getPossibleActions()));
	}

	/**
	 * This method sends a new command to the current player
	 * 
	 * @param command
	 */
	public void notifyCurrentPlayer(CommandServerToClient command) {
		LOGGER.trace("notify current player");
		for (ClientHandler client : clients)
			if (client.getPlayer().equals(match.getCurrentPlayer())) {
				sendCommand(client, command);
				return;
			}
	}

	/**
	 * This method notifyies a generic player
	 * 
	 * @param command
	 * @param player
	 */
	public void notifyGenericPlayer(CommandServerToClient command, Player player) {
		LOGGER.trace("notify generic player");
		for (ClientHandler client : clients)
			if (client.getPlayer().equals(player))
				sendCommand(client, command);
		return;
	}

	@Override
	public void notifyNewItem(Card item) {
		notifyAllClients(new CommandNotification("Player "
				+ getCurrentIdPlayer() + " picks an item"));
		notifyCurrentPlayer(new CommandAddCard(item));
	}

	@Override
	public void notifyDiscardItem(Card item) {
		notifyAllClients(new CommandNotification("Player "
				+ getCurrentIdPlayer() + " used "
				+ item.toString().toUpperCase()));
		notifyCurrentPlayer(new CommandRemoveCard(item));
	}

	@Override
	public void notifyDiscardItem(Card item, Player player) {
		notifyAllClients(new CommandNotification("Player "
				+ player.getPlayerId() + " used "
				+ item.toString().toUpperCase()));
		notifyGenericPlayer(new CommandRemoveCard(item), player);
	}

	@Override
	public void notifyNewPosition() {
		LOGGER.trace("notify new position");
		notifyCurrentPlayer(new CommandNewPosition(getCurrentPlayer()
				.getCurrentSector()));
	}

	@Override
	public void notifyWinners(Map<Player, Boolean> winners) {
		notifyEndOfGame(winners);
	}

	@Override
	public void notifySetNext() {
		setNext();
	}

	@Override
	public void notifyCannotUseCard() {
		notifyCurrentPlayer(new CommandNotification("You cannot use this card"));
	}

	private void notifyEndOfGame(Map<Player, Boolean> winners) {
		LOGGER.trace("notifyEndOFGAME");
		for (ClientHandler client : clients)
			if (winners.containsKey(client.getPlayer()))
				try {
					LOGGER.trace(winners.get(client.getPlayer()));
					client.sendCommand(new CommandWin(winners.get(client
							.getPlayer())));
				} catch (Exception e) {
					LOGGER.fatal(e);
				}
		closeMatch();
	}

	// FSM

	private int getCurrentState() {
		return getCurrentPlayer().getState();
	}

	private void setCurrentState(int state) {
		getCurrentPlayer().setState(state);
	}

	@Override
	public void updateAttackFSM() {
		Map<Integer, Map<Object, Integer>> FSM = getCurrentPlayer().getFSM();

		FSM.get(ConstantFSM.NEED_TO_SOLVE_SECTOR).put(ConstantFSM.ATTACK,
				ConstantFSM.END_TURN);
		FSM.get(ConstantFSM.SOLVED_SECTOR).put(ConstantFSM.ATTACK,
				ConstantFSM.END_TURN);
		notifyCurrentPlayer(new CommandAskMove(getPossibleActions()));
	}

	@Override
	public void updateSedativeFSM() {
		Map<Integer, Map<Object, Integer>> FSM = getCurrentPlayer().getFSM();

		FSM.remove(ConstantFSM.NEED_TO_SOLVE_SECTOR);
		FSM.put(ConstantFSM.NEED_TO_SOLVE_SECTOR,
				FSM.get(ConstantFSM.SOLVED_SECTOR));
		notifyCurrentPlayer(new CommandAskMove(getPossibleActions()));
	}

	/**
	 * This method is used to create a list of all possible action that a player
	 * can do in its turn
	 * 
	 * @return list of strings containing all the possible actions
	 */
	public List<String> getPossibleActions() {
		LOGGER.trace("get possible actions");
		int playerState = getCurrentState();
		Set<Object> actions = getCurrentPlayer().getFSM().get(playerState)
				.keySet();
		List<String> actionsString = new ArrayList<String>();
		if (actions.contains(ConstantFSM.MOVE))
			actionsString.add(ConstantClient.MOVE);
		if (actions.contains(ConstantFSM.ATTACK))
			actionsString.add(ConstantClient.ATTACK);
		if (actions.contains(ConstantFSM.END_TURN_COMMAND))
			actionsString.add(ConstantClient.PASS);
		if (actions.contains(ConstantFSM.SOLVE_SECTOR))
			actionsString.add(ConstantClient.DRAW);
		if (actions.contains(ConstantFSM.CARD))
			actionsString.add(ConstantClient.CARD);
		if (actions.contains(ConstantFSM.DISCARD_ITEM))
			actionsString.add(ConstantClient.DISCARD);
		return actionsString;
	}

	/**
	 * This method sets a the new state of the current player
	 * 
	 * @param command
	 *            that is needed to change the state
	 */
	public void setNewState(CommandClientToServer command) {
		int playerState = getCurrentState();
		setCurrentState(getCurrentPlayer().getFSM().get(playerState)
				.get(command.getClass()));
		if (getCurrentState() == ConstantFSM.WAIT_NOISE_COORDINATES)
			notifyCurrentPlayer(new CommandAskCoordNoiseAnywhere());
	}

	@Override
	public void endStateNotify() {
		LOGGER.trace("end state");
		setCurrentState(ConstantFSM.END_TURN);
		notifyCurrentPlayer(new CommandAskMove(getPossibleActions()));
	}

	// Closure Method
	/**
	 * method that close all closed clients and close the match if there aren't
	 * any more players
	 */
	public void removeClosedClients() {
		LOGGER.debug("clients closed find!");
		List<ClientHandler> clientList = new ArrayList<ClientHandler>(
				closedClients);
		for (ClientHandler client : clientList) {
			if (clients.contains(client))
				client.closeByClient();
			else
				LOGGER.trace("client already closing");
		}
		closedClients = new ArrayList<ClientHandler>();
		if (clients.isEmpty())
			closeMatch();
	}

	@Override
	public void removeClient(ClientHandler clientHandler) {
		LOGGER.trace("remove client");
		clients.remove(clientHandler);
		Player player = getCurrentPlayer();
		if (clientHandler.getPlayer().equals(player) && !clients.isEmpty()) {
			LOGGER.trace("remove current client player");
			timerNotNeed();
			setNext();
		}
		if (!clientHandler.getPlayer().isDead())
			match.killPlayer(clientHandler.getPlayer());
		LOGGER.trace("disconnect");
		notifyExclusiveToAll(new CommandNotification(clientHandler.getPlayer()
				.toString().toUpperCase()
				+ " player "
				+ clientHandler.getPlayer().getPlayerId()
				+ " has disconnected from the game"), clientHandler);
		if (match.finishHuman() && !clients.isEmpty()) {
			notifyEndOfGame(match.lastHumanDied());
		}
	}

	/**
	 * method invoked by the ping timer to check if the current player is always
	 * on
	 */
	@Override
	public void turnTimerExpired() {
		LOGGER.debug("timer for turn expired");
		List<ClientHandler> list = new ArrayList<ClientHandler>(clients);
		for (ClientHandler clientHandler : list)
			if (clientHandler.getPlayer().equals(getCurrentPlayer())) {
				closedClients.add(clientHandler);
				try {
					clientHandler.sendCommand(new CommandDisconnection());
				} catch (Exception e) {
					LOGGER.debug(e);
				}
			}
		removeClosedClients();
	}

	/**
	 * method to make the ping timer start
	 */
	public void createTurnTimer() {
		LOGGER.debug("create turn timer");
		timer = new TurnTimer(this);
		timerThread = new Thread(timer);
		timerThread.start();
	}

	/**
	 * method to interrupt the turn timer if it is alive
	 */
	public void timerNotNeed() {
		LOGGER.trace("timer interrupted");
		if (timerThread.isAlive())
			timerThread.interrupt();
	}

	private void checkDisconnection() {
		if (!closedClients.isEmpty())
			removeClosedClients();
	}

	/**
	 * close the match and all its connection with client
	 * 
	 */
	public synchronized void closeMatch() {
		LOGGER.trace("closing match");
		timerNotNeed();
		if (!clients.isEmpty()) {
			List<ClientHandler> list = new ArrayList<ClientHandler>();
			for (ClientHandler clientHandler : clients)
				list.add(clientHandler);
			for (ClientHandler clientHandler : list) {
				LOGGER.trace("closing");
				clients.remove(clientHandler);
				if (!clientHandler.getPlayer().isDead())
					match.killPlayer(clientHandler.getPlayer());
				clientHandler.closeByServer();
			}
		}
		serverObserver.notifyClose(this);
	}

	private void notifyExclusiveToAll(CommandServerToClient command,
			ClientHandler clientHandler) {
		for (ClientHandler client : clients)
			if (!client.equals(clientHandler)) {
				try {
					client.sendCommand(command);
				} catch (Exception e) {
					LOGGER.fatal(e);
				}
			}
	}
}