package it.polimi.ingsw.comodiconficconi.common.command;

import java.util.Map;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * a command that notify the start of the match
 * 
 * @author davide
 *
 */
public class CommandStartMatch extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4287783139182527017L;
	private Map<Coordinates, Sector> board;
	private Player player;
	private String nameMap;

	/**
	 * 
	 * @param board
	 *            : the configuration of the map
	 * @param player
	 *            : the player associated to that client
	 * @param nameMap
	 *            : the name of the map
	 */
	public CommandStartMatch(Map<Coordinates, Sector> board, Player player,
			String nameMap) {
		this.board = board;
		this.player = player;
		this.nameMap = nameMap;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public Player getPlayer() {
		return player;
	}

	public Map<Coordinates, Sector> getBoard() {
		return board;
	}

	public String getNameMap() {
		return nameMap;
	}
}
