package it.polimi.ingsw.comodiconficconi.client.network;

import java.io.IOException;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.client.network.utils.ObserverCommandToClient;
import it.polimi.ingsw.comodiconficconi.client.network.utils.SubjectCommandToClientAbstract;
import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;

/**
 * * this class connect the client to the server through Socket technology
 * 
 * @author davide
 *
 */
public class SocketInterface extends SubjectCommandToClientAbstract implements NetworkInterface,
		ObserverCommandToClient {

	private ObjectInputStream inSocket;
	private ObjectOutputStream outSocket;
	private Socket socket;
	private SocketListener listener;
	private Thread hear;

	private static final Logger LOGGER = Logger
			.getLogger(SocketInterface.class);

	protected SocketInterface() {
		LOGGER.trace("socket interface");
	}

	@Override
	public void connect() throws IOException {
		socket = new Socket(ConstantNetwork.ADDRESS, ConstantNetwork.PORT);
		outSocket = new ObjectOutputStream(socket.getOutputStream());
		outSocket.flush();
		inSocket = new ObjectInputStream(socket.getInputStream());
		// create listener
		listener = new SocketListener(inSocket);
		listener.addObserver(this);
		hear = new Thread(listener);
		hear.start();
		LOGGER.trace("connected");
	}

	@Override
	public void sendCommand(CommandClientToServer command) throws IOException {
		LOGGER.trace("sending command" + command);
		outSocket.writeObject(command);
		outSocket.flush();

	}

	@Override
	public void notifyNewCommand(CommandServerToClient command) {
		LOGGER.trace("new command recieved " + command);
		notifyObserver(command);

	}

	@Override
	public void close() {
		try {
			LOGGER.trace("closing");
			socket.close();
			inSocket.close();
			outSocket.close();
		} catch (Exception e) {
			LOGGER.debug("Exception: on closing socket");
			LOGGER.fatal(e);
		}
		closeListener();
	}

	@Override
	public void update(CommandServerToClient command) {
		notifyNewCommand(command);

	}

	private void closeListener() {
		if (hear.isAlive())
			hear.interrupt();
	}

}
