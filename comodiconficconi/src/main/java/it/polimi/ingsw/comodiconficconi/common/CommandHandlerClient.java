package it.polimi.ingsw.comodiconficconi.common;

import it.polimi.ingsw.comodiconficconi.common.command.CommandAddCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordNoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordSpotLight;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDeath;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDisconnection;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNewPosition;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotValid;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotification;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotificationWithCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandRemoveCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartMatch;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandTurnEnded;
import it.polimi.ingsw.comodiconficconi.common.command.CommandWin;

public interface CommandHandlerClient {

	/**
	 * this method will notify all clients that the match starts
	 * 
	 * @param startMatch
	 */
	void applyCommand(CommandStartMatch startMatch);

	/**
	 * this method notify to client the start of the turn;
	 * 
	 * @param startTurn
	 */
	void applyCommand(CommandStartTurn startTurn);

	/**
	 * this method will ask to player the next move
	 * 
	 * @param askMove
	 */
	void applyCommand(CommandAskMove askMove);

	/**
	 * this method can notify what other player do if they attack or use a
	 * object or do a noise
	 * 
	 * @param notification
	 */
	void applyCommand(CommandNotification notification);

	/**
	 * This method notifies something to the player
	 * 
	 * @param notificationWithCoord
	 */
	void applyCommand(CommandNotificationWithCoordinates notificationWithCoord);

	/**
	 * this method will notify that command isn't valid
	 * 
	 * @param notValid
	 */
	void applyCommand(CommandNotValid notValid);

	/**
	 * This method updates the position of the player
	 * 
	 * @param newPosition
	 */
	void applyCommand(CommandNewPosition newPosition);

	/**
	 * This method asks for a coordinate used for noise sector
	 * 
	 * @param askNoiseAnywhere
	 */
	void applyCommand(CommandAskCoordNoiseAnywhere askNoiseAnywhere);

	/**
	 * This method asks for a coordinate for the spotlight card
	 * 
	 * @param askCoordSpotlight
	 */
	void applyCommand(CommandAskCoordSpotLight askCoordSpotlight);

	/**
	 * 
	 * @param askCoordMove
	 */
	void applyCommand(CommandAskCoordMove askCoordMove);

	/**
	 * This method notifies a new card
	 * 
	 * @param addCard
	 */
	void applyCommand(CommandAddCard addCard);

	/**
	 * This method notifies that the player used a card and has to remove it
	 * 
	 * @param removeCard
	 */
	void applyCommand(CommandRemoveCard removeCard);

	/**
	 * This method notifies that the player's turn has ended
	 * 
	 * @param turnEnded
	 */
	void applyCommand(CommandTurnEnded turnEnded);

	/**
	 * This method will notify the death of the player
	 * 
	 * @param death
	 */
	void applyCommand(CommandDeath death);

	/**
	 * This method will notify the win of the player
	 * 
	 * @param win
	 */
	void applyCommand(CommandWin win);

	/**
	 * server use this to notify to close the channel of client
	 * 
	 * @param closeToClient
	 */
	void applyCommand(CommandCloseToClient closeToClient);

	/**
	 * this method handle the disconnection from the server
	 * 
	 * @param disconnection
	 */
	void applyCommand(CommandDisconnection disconnection);

}
