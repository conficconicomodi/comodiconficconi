package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import java.awt.Image;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.client.controller.ClientController;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterface;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * This class handles all the interactions with the user regarding GUI. It
 * implements an interface called "UserInterface" which gives general methods
 * valid both for CLI and GUI It communicates with the controller through an
 * observer pattern.
 * 
 * @author alessandro
 *
 */
public class GraphicInterface implements UserInterface {

	private static final Logger LOGGER = Logger
			.getLogger(GraphicInterface.class);
	private MainFrame frame;
	private String player;
	private ClientController gameController;
	private Image icon;

	/**
	 * This constructor sets the game controller and builds the main frame of
	 * the game, with its own icon
	 * 
	 * @param gameController
	 */
	public GraphicInterface(ClientController gameController) {
		this.gameController = gameController;
		try {
			icon = ImageIO.read(this.getClass().getResource(
					ConstantClient.GAMEICON));
		} catch (IOException e) {
			LOGGER.error(e);
		}
		frame = new MainFrame();
		frame.setIconImage(icon);
		frame.validate();
	}

	/**
	 * This method initializes a match, with a new board, a new player and it
	 * sets all the main panels which are: GameBoard panel, GameConsole panel
	 * and PlayerMoves panel (which includes the Game logo)
	 */
	@Override
	public void initializeMatch(Map<Coordinates, Sector> board, Player player,
			String mapName) {
		frame.removeWelcome();
		frame.setGameBoard(board, gameController);
		frame.getGameBoard().addPlayerIcon(player);
		this.player = player.toString();
		frame.setGameConsole();
		frame.getGameConsole().getCardPanel().addListener(gameController);
		frame.getGameConsole().getPossibleActionsPanel()
				.addListener(gameController);
		frame.setMoves();
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.repaint();
		frame.validate();
		frame.addMsg("You are playing on " + mapName.toUpperCase() + " map\n");
	}

	@Override
	public void setMovePlayer(Coordinates coord, List<Coordinates> pastMoves) {
		frame.getGameBoard().changePosition(coord);
		frame.updatePastMoves(pastMoves);
	}

	@Override
	public void addCard(Card card) {
		frame.getGameConsole().addCard(card);
	}

	@Override
	public void removeCard(Card card) {
		frame.getGameConsole().removeCard(card);
		frame.addMsg(card.toString().toUpperCase() + " item removed\n");
	}

	@Override
	public void commandNotValid() {
		frame.addMsg("Command not valid\n");
	}

	@Override
	public void startTurn() {
		frame.addMsg("It is your turn\n");

	}

	@Override
	public void askNoiseCoordinates() {
		frame.addMsg("Send fake coordinates for noise\n");

	}

	@Override
	public void askSpotlightCoordinates() {
		frame.addMsg("Send coordinates for spotlight\n");
	}

	@Override
	public void notifyToAll(String notification) {
		frame.addMsg("" + notification + "\n");
	}

	@Override
	public void askMove(List<String> actions) {
		frame.removeAllActions();
		if (actions.contains(ConstantClient.ATTACK))
			frame.getGameConsole().showAttack();
		if (actions.contains(ConstantClient.MOVE))
			frame.getGameConsole().showMove();
		if (actions.contains(ConstantClient.PASS))
			frame.getGameConsole().showPassTurn();
		if (actions.contains(ConstantClient.DRAW))
			frame.getGameConsole().showDrawCard();
		if (actions.contains(ConstantClient.DISCARD))
			frame.getGameConsole().showDiscardItem();
	}

	@Override
	public void turnEnded() {
		frame.removeAllActions();
		frame.addMsg("Your turn has ended\n");
	}

	@Override
	public void setMovePlayer(Coordinates newPosition) {
		frame.getGameBoard().changePosition(newPosition);
	}

	@Override
	public void death() {
		frame.getContentPane().removeAll();
		frame.addGeneralPanel(ConstantClient.DEATH);
	}

	@Override
	public void win() {
		frame.getContentPane().removeAll();
		if (player.equals(ConstantClient.HUMAN))
			frame.addGeneralPanel(ConstantClient.WINHUMAN);
		else if (player.equals(ConstantClient.ALIEN))
			frame.addGeneralPanel(ConstantClient.WINALIEN);
	}

	@Override
	public void lose() {
		if (player.equals(ConstantClient.ALIEN)) {
			frame.getContentPane().removeAll();
			frame.addGeneralPanel(ConstantClient.LOSEALIEN);
		} else {
			frame.getContentPane().removeAll();
			frame.addGeneralPanel(ConstantClient.LOSEHUMAN);
		}
			
	}

	@Override
	public void askMoveCoordinates() {
		frame.addMsg("Click on the sector you want to move into\n");
	}

	@Override
	public void disconnection() {
		frame.getContentPane().removeAll();
		frame.addGeneralPanel(ConstantClient.DISCONNECTION);
	}

	@Override
	public void serverOutOfOrder() {
		frame.getContentPane().removeAll();
		frame.addGeneralPanel(ConstantClient.OUTOFORDER);
	}
}
