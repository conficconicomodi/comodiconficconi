package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command used to notify the need to solve a sector
 * 
 * @author davide
 *
 */
public class CommandSolveSector extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2132993410667023564L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
		commandHandlerServer.applyCommand(this);

	}

}
