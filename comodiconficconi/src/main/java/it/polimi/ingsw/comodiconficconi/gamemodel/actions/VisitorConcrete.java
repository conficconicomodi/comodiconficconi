package it.polimi.ingsw.comodiconficconi.gamemodel.actions;

import java.util.Map;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;
import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;
import it.polimi.ingsw.comodiconficconi.gamemodel.CommandNotifier;
import it.polimi.ingsw.comodiconficconi.gamemodel.Match;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Adrenaline;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.AttackCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.BlockedHatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Defense;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseInSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.OpenHatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Sedative;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Silence;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.SpotLight;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Teleport;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * This class implements the methods of Visitor
 * 
 * @author alessandro
 * @author davide
 *
 */
public class VisitorConcrete implements Visitor {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.gamemodel.actions.VisitorConcrete.class);

	private Match match;
	private CommandNotifier notifier = null;
	private static final String PLAYER = "Player ";
	private static final String MAKESNOISE = " makes noise in: ";

	public VisitorConcrete(Match match) {
		this.match = match;
	}

	@Override
	public void visitDrawHatchCard() {
		match.drawEscapeHatchCard().applyEffect(this);
	}

	@Override
	public void visit(Teleport card) {
		Player currentPlayer = match.getCurrentPlayer();
		if (currentPlayer.getState() == ConstantFSM.NEED_TO_SOLVE_SECTOR) {
			if (notifierSet())
				notifier.notifyCannotUseCard();
		} else {
			match.getBoard().getSector(currentPlayer.getCurrentSector())
					.removePlayer(currentPlayer);
			match.getBoard().setInitialPosition(currentPlayer);
			match.removeCard((Card) card);
			if (notifierSet()) {
				notifier.notifyDiscardItem(card);
				notifier.notifyNewPosition();
			}
		}
	}

	@Override
	public void visit(AttackCard attackCard) {
		match.removeCard((Card) attackCard);
		if (notifierSet()) {
			notifier.notifyDiscardItem(attackCard);
			notifier.updateAttackFSM();
		}
	}

	@Override
	public void visit(Defense defenseCard) {
		if (notifierSet())
			notifier.notifyCannotUseCard();
	}

	@Override
	public void visit(Adrenaline adrenalineCard) {
		match.getCurrentPlayer().setSteps(ConstantPlayer.ADRENALINESTEPS);
		match.removeCard((Card) adrenalineCard);
		if (notifierSet())
			notifier.notifyDiscardItem(adrenalineCard);
	}

	@Override
	public void visit(Sedative sedativeCard) {
		match.removeCard((Card) sedativeCard);
		if (notifierSet()) {
			notifier.notifyDiscardItem(sedativeCard);
			notifier.updateSedativeFSM();
		}
	}

	@Override
	public void visit(SpotLight spotlightCard) {
		if (notifierSet()) {
			notifier.notifyDiscardItem(spotlightCard);
			notifier.spotlightNotify();
		}
		match.removeCard((Card) spotlightCard);
	}

	@Override
	public void visit(NoiseInSector noiseInSector, boolean item) {
		if (notifierSet()) {
			notifier.genericNotifyWithCoordinates(PLAYER
					+ match.getCurrentPlayer().getPlayerId() + MAKESNOISE,
					match.getCurrentPlayer().getCurrentSector());
			notifier.endStateNotify();
		}
		if (item)
			match.checkDrawItem();
	}

	@Override
	public void visit(NoiseAnywhere noiseAnywhere, boolean item) {
		LOGGER.debug("Noise anywhere");
		if (notifierSet())
			notifier.noiseAnyWhereNotify();
		if (item)
			match.checkDrawItem();

	}

	@Override
	public void visit(Silence silenceCard) {
		LOGGER.debug("Silence");
		if (notifierSet()) {
			notifier.genericNotifyToAll(PLAYER
					+ match.getCurrentPlayer().getPlayerId() + ": SILENCE");
			notifier.endStateNotify();
		}
	}

	@Override
	public void visit(OpenHatchCard openHatchCard) {
		LOGGER.debug("Open Hatch");
		if (notifierSet())
			notifier.notifyEscaped();
		Player player = match.getCurrentPlayer();
		match.discardCards(player);
		if (match.isLastHuman(player)) {
			LOGGER.trace("finish human");
			Map<Player, Boolean> winners = match.lastHumanEscaped(player);
			if (notifierSet())
				notifier.notifyWinners(winners);
		} else if (match.finishOpenHatch()) {
			LOGGER.trace("finish open hatch");
			Map<Player, Boolean> winners = match.noMoreHatches();
			if (notifierSet())
				notifier.notifyWinners(winners);
		} else if (notifierSet())
			notifier.notifySetNext();
		else
			match.setNextPlayer();
		match.getPlayers().remove(player);
	}

	@Override
	public void visit(BlockedHatchCard blockedHatchCard) {
		LOGGER.debug("Blocked Hatch");
		if (match.finishOpenHatch()) {
			LOGGER.trace("finish open hatch");
			Map<Player, Boolean> winners = match.noMoreHatches();
			if (notifierSet())
				notifier.notifyWinners(winners);
		}
		if (notifierSet())
			notifier.genericNotifyWithCoordinates(PLAYER
					+ match.getCurrentPlayer().getPlayerId()
					+ " went in a blocked hatch in: ", match.getCurrentPlayer()
					.getCurrentSector());

	}

	@Override
	public void drawSectorCard() { 
		match.drawSectorCard().applyEffect(this);

	}

	@Override
	public void setNotifier(CommandNotifier commandNotifier) {
		notifier = commandNotifier;

	}

	@Override
	public boolean notifierSet() {
		return notifier != null;
	}
}