package it.polimi.ingsw.comodiconficconi.client.network;

import java.rmi.NotBoundException;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.client.network.utils.SubjectCommandToClientAbstract;
import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.common.RemoteClientHandler;
import it.polimi.ingsw.comodiconficconi.common.RemoteClientInterface;
import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;

/**
 * this class connect the client to the server through RMI technology
 * 
 * @author davide
 *
 */
public class RMIInterface extends SubjectCommandToClientAbstract implements
		NetworkInterface, RemoteClientInterface {

	private static final Logger LOGGER = Logger.getLogger(RMIInterface.class);

	private RemoteClientHandler clientHandler;
	private Registry registry;
	private String name = "ClientHandler";
	private Registry registry2;
	private String name2 = "Client";
	private int clientPort;
	private RemoteClientInterface client = this;

	protected RMIInterface() {
		LOGGER.trace("rmi interface");

	}

	@Override
	public void connect() throws Exception {
		LOGGER.trace("connecting");
		// import object
		registry = LocateRegistry
				.getRegistry(ConstantNetwork.RMICLIENTHANDLERPORT);
		clientHandler = (RemoteClientHandler) registry.lookup(name);
		RemoteClientInterface stub = (RemoteClientInterface) UnicastRemoteObject
				.exportObject(client, 0);
		clientPort = ConstantNetwork.RMICLIENTPORT;
		LOGGER.trace(clientPort);
		// export object
		try {
			registry2 = LocateRegistry
					.getRegistry(ConstantNetwork.RMICLIENTPORT);
			registry2.rebind(name2, stub);
			LOGGER.trace("already exists");
		} catch (Exception e) {
			LOGGER.debug(e);
			LOGGER.trace("doesn't exist");
			registry2 = LocateRegistry.createRegistry(clientPort);
			registry2.bind(name2, stub);
		}
		// register the client to the server
		clientHandler.addClient(clientPort);

	}

	@Override
	public void sendCommand(CommandClientToServer command) throws Exception {
		LOGGER.trace("sending command" + command);
		clientHandler.notifyNewCommand(command);

	}

	@Override
	public void notifyNewCommand(CommandServerToClient command) {
		LOGGER.trace("new command recieved " + command);
		notifyObserver(command);
	}

	@Override
	public void close() {
		try {
			LOGGER.debug("closing client rmi" + this);
			registry2.unbind(name2);
			registry2 = null;
		} catch (RemoteException | NotBoundException | NullPointerException e) {
			LOGGER.info(e);
		}
	}
}
