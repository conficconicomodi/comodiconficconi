package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this class make the player declare "Noise in sector" and his real position
 * 
 * @author davide
 *
 */
public class NoiseInSector extends SectorCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3111902272267172049L;
	private boolean object;

	protected NoiseInSector(boolean obj) {
		object = obj;
	}

	protected boolean isWithObject() {
		return object;
	}

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.visit(this, object);

	}

	@Override
	public String toString() {
		return ConstantCards.NOISEINSECTOR;
	}
}
