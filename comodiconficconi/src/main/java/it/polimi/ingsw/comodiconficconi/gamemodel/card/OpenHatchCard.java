package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this card permit the player to escape from the aliens
 * 
 * @author davide
 *
 */
public class OpenHatchCard extends HatchCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2507876086580992756L;

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.OPENHATCH;
	}
}
