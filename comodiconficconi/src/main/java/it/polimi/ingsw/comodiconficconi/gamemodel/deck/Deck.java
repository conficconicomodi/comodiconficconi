package it.polimi.ingsw.comodiconficconi.gamemodel.deck;

import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * this class represent the abstract deck of the game that can be specialized in
 * different types of deck
 * 
 * @author davide
 *
 */
public abstract class Deck {
	/**
	 * these are the cards that compose the deck
	 */
	protected List<Card> cards;

	public Deck() {
		cards = new ArrayList<Card>();
	}

	/**
	 * method that let to add a card to the top of the deck
	 * 
	 * @param card
	 *            : the card that will be added
	 * @return the deck itself
	 */
	public Deck addCard(Card card) {
		cards.add(card);
		return this;
	}

	/**
	 * method that shuffle the deck
	 * 
	 * @return the deck shuffled
	 */

	public Deck shuffle() {
		Collections.shuffle(cards);
		return this;
	}

	/**
	 * method to draw a card
	 * 
	 * @return the card on the top of the deck
	 */
	public Card draw() {
		Card card = null;
		card = cards.get(cards.size() - 1);
		cards.remove(cards.size() - 1);
		return card;
	}

	/**
	 * method to check if the deck is empty
	 * 
	 * @return true if it is empty false otherwise
	 */
	public boolean isEmpty() {
		return cards.isEmpty();
	}

}