package it.polimi.ingsw.comodiconficconi.server.utils;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.constants.ConstantNetwork;

/**
 * This is the Timer of the turn and when it expired it disconnect the current
 * player if it hasn't finish his turn yet
 * 
 * @author davide
 *
 */
public class TurnTimer implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(TurnTimer.class);
	private HandlerInterface handler;

	public TurnTimer(HandlerInterface handler) {
		this.handler = handler;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(ConstantNetwork.TURNTIMER);
		} catch (InterruptedException e) {
			LOGGER.debug("Timer interrupted: " + e);
			return;
		}
		handler.turnTimerExpired();

	}
}
