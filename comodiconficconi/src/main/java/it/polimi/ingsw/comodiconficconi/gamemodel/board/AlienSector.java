package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;

/**
 * This class defines the starting sector for the aliens
 * 
 * @author alessandro
 *
 */
public class AlienSector extends BaseSector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5617532447627475557L;

	@Override
	public String toString() {
		return ConstantBoard.ALIENSECTOR;
	}
}
