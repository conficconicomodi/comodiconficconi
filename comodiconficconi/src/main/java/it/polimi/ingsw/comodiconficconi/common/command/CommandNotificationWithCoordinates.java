package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * command sent to notify that something is happened in some coordinates
 * 
 * @author davide
 *
 */
public class CommandNotificationWithCoordinates extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6857600107685829927L;
	private String msg;
	private Coordinates coordinates;

	/**
	 * 
	 * @param message
	 *            : the message of notification
	 * @param coordinates
	 *            : the coordinates of the event
	 */
	public CommandNotificationWithCoordinates(String message,
			Coordinates coordinates) {
		this.msg = message;
		this.coordinates = coordinates;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public String getMsg() {
		return msg;
	}
}
