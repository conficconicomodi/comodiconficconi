package it.polimi.ingsw.comodiconficconi.client.controller;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.client.network.NetworkInterface;
import it.polimi.ingsw.comodiconficconi.client.network.utils.ObserverCommandToClient;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterface;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.GameBoardListener;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.CardPanelListener;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.PossibleActionListener;
import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAttack;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDiscardItem;
import it.polimi.ingsw.comodiconficconi.common.command.CommandEndTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSendCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSolveSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * This class is the Controller MVC of the program. It connects the view with
 * the model that is on the server through the chosen network interface (RMI or
 * Socket)
 * 
 * @author alessandro
 *
 */
public class ClientController implements GameBoardListener, CardPanelListener,
		PossibleActionListener, ObserverCommandToClient {

	private static final Logger LOGGER = Logger
			.getLogger(ClientController.class);

	private UserInterface userInterface;
	private NetworkInterface networkInterface;
	private CommandHandlerClient commandHandler;

	public ClientController(NetworkInterface networkInterface) {
		this.networkInterface = networkInterface;
		this.networkInterface.addObserver(this);
	}

	public void setCommandHandler(CommandHandlerClient handler) {
		this.commandHandler = handler;
	}

	public void setUserInterface(UserInterface userInterface) {
		this.userInterface = userInterface;
	}

	@Override
	public void notify(Coordinates coord) {
		sendCommand(new CommandSendCoordinates(coord));
	}

	@Override
	public void notify(Card card) {
		sendCommand(new CommandCard(card));
	}

	@Override
	public void notifyMove() {
		sendCommand(new CommandMove());
	}

	@Override
	public void notifyAttack() {
		sendCommand(new CommandAttack());
	}

	@Override
	public void notifyPassTurn() {
		sendCommand(new CommandEndTurn());
	}

	@Override
	public void notifyDrawCard() {
		sendCommand(new CommandSolveSector());
	}

	@Override
	public void notifyDiscardItem() {
		sendCommand(new CommandDiscardItem());
	}

	private void sendCommand(CommandClientToServer command) {
		try {
			networkInterface.sendCommand(command);
		} catch (Exception e) {
			LOGGER.debug(e);
			userInterface.serverOutOfOrder();
			networkInterface.close();
		}
	}

	@Override
	public void update(CommandServerToClient command) {
		command.useCommand(commandHandler);
	}

	/**
	 * method to close connection correctly by client
	 */
	public void closeConnection() {
		networkInterface.close();
	}
}
