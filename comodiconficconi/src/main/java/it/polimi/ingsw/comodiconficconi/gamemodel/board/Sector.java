package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitable;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * This class defines the abstract sector displayed in the map.
 * 
 * @author alessandro
 *
 */

public abstract class Sector implements Visitable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1676959040299921213L;
	protected Set<Player> playerInSector = new HashSet<Player>();

	/**
	 * This methods returns a set of players contained in the sector
	 * 
	 * @return A set of characters who are inside the sector.
	 */
	public Set<Player> getPlayersInside() {
		return playerInSector;
	}

	/**
	 * This method adds a player to the sector
	 * 
	 * @param player
	 */
	public void addPlayer(Player player) {
		playerInSector.add(player);
	}

	/**
	 * This method removes a player from the sector
	 * 
	 * @param player
	 */
	public void removePlayer(Player player) {
		playerInSector.remove(player);
	}

	/**
	 * This method returns a boolean depending on the possibility to move in the
	 * sector
	 * 
	 * @return
	 */
	public abstract boolean canMoveIn();

	/**
	 * This method returns a boolean depending on the fact that a player who
	 * steps into the sector has to draw a card or not
	 * 
	 * @return
	 */
	public abstract boolean hasToDraw();

	@Override
	public void applyEffect(Visitor visitor) {
		// This method is empty. Not all the subclasses will implement it
	}
}
