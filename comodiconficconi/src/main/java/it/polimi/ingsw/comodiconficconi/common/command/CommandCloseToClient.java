package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command used to notify that server is going to be closed
 * 
 * @author davide
 *
 */
public class CommandCloseToClient extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2300929340953110744L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
