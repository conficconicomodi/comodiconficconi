package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this card close forver an hatch and doesn't allow the human player that draw
 * this card to escape
 * 
 * @author davide
 */
public class BlockedHatchCard extends HatchCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5896762302246846494L;

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.BLOCKEDHATCH;
	}

}
