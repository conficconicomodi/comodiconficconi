package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;

/**
 * This class is necessary to return the right path of the image in the
 * resources folder so that every different item card has its own image
 * 
 * @author alessandro
 *
 */
public class CardFactory {

	private CardFactory() {
	}

	/**
	 * This static method switches the input string and finds what is the right
	 * image path to assign to the card
	 * 
	 * @param type
	 * @return String containing the path of the image
	 */
	public static String getCardImage(String type) {
		switch (type) {
		case ConstantCards.DEFENSE:
			return ConstantClient.DEFENSECARD;
		case ConstantCards.TELEPORT:
			return ConstantClient.TELEPORTCARD;
		case ConstantCards.ATTACK:
			return ConstantClient.ATTACKCARD;
		case ConstantCards.ADRENALINE:
			return ConstantClient.ADRENALINECARD;
		case ConstantCards.SEDATIVE:
			return ConstantClient.SEDATIVECARD;
		case ConstantCards.SPOTLIGHT:
			return ConstantClient.SPOTLIGHTCARD;
		default:
			return null;
		}
	}
}