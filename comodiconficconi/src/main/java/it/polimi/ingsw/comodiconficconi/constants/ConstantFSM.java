package it.polimi.ingsw.comodiconficconi.constants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import it.polimi.ingsw.comodiconficconi.common.command.CommandAttack;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDiscardItem;
import it.polimi.ingsw.comodiconficconi.common.command.CommandEndTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSendCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSolveSector;

/**
 * This class contains all the constants for the Finite State Machine. Here are
 * stored all the possible state that the FSM can have and the commands that
 * each state can apply is saved in an hash map with the class of the command
 * itself and the next state
 * 
 * The static methods contained in this class build the FSM relative to the
 * alien or human and if later someone adds another kind of character the
 * process to build his own FSM is the same, so that this feature is extendable
 * for future upgrades
 * 
 * @author alessandro
 *
 */
public class ConstantFSM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6496576367276225321L;

	public static final int BEFORE_MOVE = 0;
	public static final int WAIT_MOVE_COORD = 1;
	public static final int SOLVED_SECTOR = 2;
	public static final int NEED_TO_SOLVE_SECTOR = 3;
	public static final int WAIT_NOISE_COORDINATES = 4;
	public static final int END_TURN = 5;
	public static final int WAIT_SPOTLIGHT_COORDINATES = 6;
	public static final int DISCARD_AFTER_NOISE_ANYWHERE = 7;
	public static final int DISCARD_BEFORE_END_TURN = 8;
	public static final int FULL_ITEMS_AFTER_NOISE_ANYWHERE = 9;
	public static final int FULL_ITEMS_BEFORE_END_TURN = 10;

	public static final Object SEND_COORDINATES = CommandSendCoordinates.class;
	public static final Object MOVE = CommandMove.class;
	public static final Object CARD = CommandCard.class;
	public static final Object END_TURN_COMMAND = CommandEndTurn.class;
	public static final Object SOLVE_SECTOR = CommandSolveSector.class;
	public static final Object ATTACK = CommandAttack.class;
	public static final Object DISCARD_ITEM = CommandDiscardItem.class;
}
