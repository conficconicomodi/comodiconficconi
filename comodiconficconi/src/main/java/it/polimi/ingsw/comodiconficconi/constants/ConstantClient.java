package it.polimi.ingsw.comodiconficconi.constants;

/**
 * Constants for client
 * 
 * @author alessandro
 *
 */
public class ConstantClient {

	/** Constants for creating user interfaces */
	public static final int GUI = 2;
	public static final int CLI = 1;

	/**
	 * Players
	 */
	public static final String HUMAN = "human";
	public static final String ALIEN = "alien";

	/**
	 * GUI images references
	 */
	public static final String WELCOME = "/welcome.jpg";
	public static final String BACKGROUNDIMAGE = "/gameboardbackground.png";
	public static final String LOGO = "/logo.png";
	public static final String DEATH = "/death.png";
	public static final String WINHUMAN = "/winhuman.jpg";
	public static final String WINALIEN = "/winalien.jpg";
	public static final String LOSEALIEN = "/losealien.jpg";
	public static final String LOSEHUMAN = "/losehuman.jpg";
	public static final String DISCONNECTION = "/disconnected.jpg";
	public static final String OUTOFORDER = "/serveroutoforder.jpg";
	public static final String ALIENICON = "/alienicon.png";
	public static final String HUMANICON = "/humanicon.png";
	public static final String ATTACKACTION = "/attackAction.png";
	public static final String MOVEACTION = "/moveAction.png";
	public static final String PASSTURNACTION = "/passTurnAction.png";
	public static final String DRAWCARDACTION = "/drawCardAction.png";
	public static final String DISCARDITEMACTION = "/discardItemAction.png";
	public static final String GAMEICON = "/icon.png";

	/**
	 * Card images
	 */
	public static final String DEFENSECARD = "/defense.png";
	public static final String TELEPORTCARD = "/teleport.png";
	public static final String ATTACKCARD = "/attack.png";
	public static final String ADRENALINECARD = "/adrenaline.png";
	public static final String SEDATIVECARD = "/sedative.png";
	public static final String SPOTLIGHTCARD = "/spotlight.png";

	/**
	 * Sector images
	 */
	public static final String ALIENSECTOR = "/aliensector.png";
	public static final String HUMANSECTOR = "/humansector.png";
	public static final String SECURESECTOR = "/securesector.png";
	public static final String DANGEROUSSECTOR = "/dangeroussector.png";
	public static final String HATCHSECTOR = "/hatchsector.png";

	/**
	 * GUI Border for Hexagons
	 */
	public static final int BORDER = 40;

	/**
	 * Constants for CLI
	 */
	public static final String MOVE = "move";
	public static final String ATTACK = "attack";
	public static final String PASS = "pass";
	public static final String DRAW = "draw";
	public static final String CARD = "card";
	public static final String DISCARD = "discard";

	public static final int SEND_COMMAND_STATE = 0;
	public static final int SEND_COORDINATES_STATE = 1;
	public static final int SEND_CARD_STATE = 2;
	public static final int NULL = -1;
	public static final int SEND_COORDINATES_STATE_SPOTLIGHT = 3;
	public static final int END_GAME = 4;

	private ConstantClient() {
	}
}
