package it.polimi.ingsw.comodiconficconi.gamemodel.card;

public class CardCodeException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6819179876462860724L;
	private final String error = "Card code not valid";
	
	public String getError() {
		return error;
	}
}
