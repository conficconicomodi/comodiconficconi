package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import java.io.Serializable;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this card ,when used, give the possibility to human player to has a step of
 * two sector
 * 
 * @author davide
 *
 */
public class Adrenaline extends ItemCard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2003402759561475392L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.ADRENALINE;
	}
}
