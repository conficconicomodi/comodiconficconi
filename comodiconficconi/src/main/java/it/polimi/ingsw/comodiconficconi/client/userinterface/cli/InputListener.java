package it.polimi.ingsw.comodiconficconi.client.userinterface.cli;

/**
 * This interface will be implemented by all the classes that have to listen to
 * every input from keyboard
 * 
 * @author alessandro
 *
 */
public interface InputListener {
	/**
	 * This method notifyies that there is a new input from the keyboard
	 * 
	 * @param input
	 */
	void notify(String input);
}
