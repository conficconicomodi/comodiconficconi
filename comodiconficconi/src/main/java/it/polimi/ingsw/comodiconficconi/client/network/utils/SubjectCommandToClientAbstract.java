package it.polimi.ingsw.comodiconficconi.client.network.utils;

import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * the abstract representation of the subject command to client interface
 * 
 * @author davide
 *
 */
public abstract class SubjectCommandToClientAbstract implements
		SubjectCommandToClient {

	protected ObserverCommandToClient observer;

	public SubjectCommandToClientAbstract() {
	}

	@Override
	public void addObserver(ObserverCommandToClient observer) {
		this.observer = observer;

	}

	@Override
	public void removeObserver(ObserverCommandToClient observer) {
		if (this.observer.equals(observer))
			this.observer = null;
	}

	@Override
	public void notifyObserver(CommandServerToClient command) {
		observer.update(command);

	}

}
