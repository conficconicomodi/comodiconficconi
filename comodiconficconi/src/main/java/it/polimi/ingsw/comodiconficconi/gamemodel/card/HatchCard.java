package it.polimi.ingsw.comodiconficconi.gamemodel.card;

/**
 * This class represent the abstract object Hatch card of the game
 * 
 * @author davide
 *
 */
public abstract class HatchCard extends Card {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5145622914838363162L;

}
