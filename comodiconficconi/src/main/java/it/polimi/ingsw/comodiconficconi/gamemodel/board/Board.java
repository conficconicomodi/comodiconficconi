package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * This class defines the Map, saving the sectors in an HashMap. It has methods
 * to make all the calculation for the nearby sectors of a given sector and it
 * has the method to actually move a player on the board
 * 
 * @author alessandro
 *
 */
public class Board {

	private static final int MAXNEIGHBORS = 6;
	/** Array of positions necessary to find the neighbors of a given sector */
	private static final int[][] ROW = { { -1, -1, -1, 0, 1, 0 },
			{ 0, -1, 0, 1, 1, 1 } };

	/** Array of positions necessary to find the neighbors of a given sector */
	private static final int[][] COLUMN = { { -1, 0, 1, 1, 0, -1 },
			{ -1, 0, 1, 1, 0, -1 } };

	private Map<Coordinates, Sector> map;

	private String nameMap;

	/**
	 * This method takes in input the type of map that has to be played and
	 * instantiate a new HashMap containing the coordinates and the relative
	 * sectors
	 *
	 * @param type
	 */
	public Board(String type) {
		map = new HashMap<Coordinates, Sector>();
		File mapFile = new File(this.getClass()
				.getResource("/" + type + ".txt").getFile());
		BoardFactory.getBoard(mapFile, map);
		nameMap = type;
	}

	public String getNameMap() {
		return nameMap;
	}

	/**
	 * This method retrieves a sector from the board map with a coordinates as
	 * parameters
	 * 
	 * @param coord
	 * @return
	 */
	public Sector getSector(Coordinates coord) {
		return map.get(coord);
	}

	/**
	 * This method returns the map containing sectors
	 *
	 * @return
	 */
	public Map<Coordinates, Sector> getBoard() {
		return map;
	}

	/**
	 * This method moves a player from one sector to another
	 *
	 * @param player
	 * @param finalPosition
	 * @return Sector: the final sector of the player
	 */
	public Sector movePlayer(Player player, Coordinates finalPosition) {
		map.get(finalPosition).addPlayer(player);
		map.get(player.getCurrentSector()).removePlayer(player);

		player.setCurrentSector(finalPosition);
		return map.get(finalPosition);
	}

	/**
	 * This method returns true or false depending on the possibility to move in
	 * a certain sector from a starting sector.
	 *
	 * @param player
	 * @param finalPosition
	 * @param distance
	 * @return boolean
	 */
	public boolean canMove(Player player, Coordinates finalPosition) {
		Set<Coordinates> positions = new HashSet<Coordinates>();
		possiblePostitions(player, positions);
		if (positions.contains(finalPosition))
			return true;
		return false;
	}

	/**
	 * This method is used to return a set of neighbors of a given sector
	 *
	 * @param coordinate
	 * @return
	 */
	public Set<Coordinates> getSectorNeighbors(Coordinates coordinates) {
		Set<Coordinates> neighbors = new HashSet<Coordinates>();
		int offset = coordinates.getY() % 2;
		Sector sector;
		Coordinates coord;
		for (int i = 0; i < MAXNEIGHBORS; i++) {
			coord = new Coordinates(coordinates.getX() + ROW[offset][i],
					coordinates.getY() + COLUMN[offset][i]);
			sector = getSector(coord);
			if (sector != null && sector.canMoveIn())
				neighbors.add(coord);
		}
		return neighbors;
	}

	/**
	 * This method is an algorithm used to calculate all the possible movements
	 * that a player can do, storing them in a Set
	 *
	 * @param positions
	 *            : set of possible positions
	 * @param currentPosition
	 *            : starting position
	 * @param distance
	 *            : number of steps the player can make
	 */
	private void reachablePositions(Player player, Set<Coordinates> positions,
			Coordinates currentPosition, int distance) {
		int offset = currentPosition.getY() % 2;
		Sector sector;
		Coordinates coord;
		if (distance == 0)
			positions.add(currentPosition);
		else
			for (int i = 0; i < MAXNEIGHBORS; i++) {
				coord = new Coordinates(
						currentPosition.getX() + ROW[offset][i],
						currentPosition.getY() + COLUMN[offset][i]);
				sector = getSector(coord);
				if (sector != null
						&& sector.canMoveIn()
						&& (player.isHuman() || !ConstantBoard.ESCAPEHATCHSECTOR
								.equals(sector.toString()))) {
					reachablePositions(player, positions, coord, distance - 1);
					positions.add(coord);
				}
			}
	}

	/**
	 * This method calculates all the possible position in which a player can
	 * move into thanks to the method "reachablePositions" and if the player is
	 * alien the hatch sectors are removed from the set
	 * 
	 * @param player
	 * @param positions
	 */
	public void possiblePostitions(Player player, Set<Coordinates> positions) {
		reachablePositions(player, positions, player.getCurrentSector(),
				player.getSteps());
		positions.remove(player.getCurrentSector());
		if (!player.isHuman()) {
			Iterator<Coordinates> iterator = positions.iterator();
			while (iterator.hasNext()) {
				Sector hatch = map.get(iterator.next());
				if (ConstantBoard.ESCAPEHATCHSECTOR.equals(hatch.toString()))
					iterator.remove();
			}
		}
	}

	/**
	 * This method bring back a list of all hatch sector in the map
	 * 
	 * @return the list of all hatch sector
	 */
	public List<Sector> getHatchSector() {
		List<Sector> hatches = new ArrayList<Sector>();
		for (Coordinates c : map.keySet()) {
			Sector s = getSector(c);
			if (ConstantBoard.ESCAPEHATCHSECTOR.equals(s.toString()))
				hatches.add(s);
		}
		return hatches;
	}

	/**
	 * This method is used to get the Human starting sector
	 *
	 * @return Coordinates corresponding to the human starting sector
	 */
	public Coordinates getHumanSector() {
		for (Coordinates coord : map.keySet()) {
			Sector sector = getSector(coord);
			if (ConstantBoard.HUMANSECTOR.equals(sector.toString()))
				return coord;
		}
		return null;
	}

	/**
	 * This method is used to get the coordinates of the alien sector
	 *
	 * @return Coordinates corresponding to the alien starting sector
	 */
	public Coordinates getAlienSector() {
		for (Coordinates coord : map.keySet())
			if (ConstantBoard.ALIENSECTOR.equals(getSector(coord).toString()))
				return coord;
		return null;
	}

	/**
	 * This methods is used to set the initial position of a given player
	 *
	 * @param player
	 */
	public void setInitialPosition(Player player) {
		if (!player.isHuman()) {
			getSector(getAlienSector()).addPlayer(player);
			player.setCurrentSector(getAlienSector());
		} else if (player.isHuman()) {
			getSector(getHumanSector()).addPlayer(player);
			player.setCurrentSector(getHumanSector());
		}
	}

}
