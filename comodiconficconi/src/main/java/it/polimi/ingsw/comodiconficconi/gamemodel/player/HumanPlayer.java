package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * This class instantiates a new Human player
 * 
 * @author alessandro
 *
 */
public class HumanPlayer extends Player {

	/**
	 * 
	 */
	private static final long serialVersionUID = -120472398960353021L;

	public HumanPlayer(int id) {
		super(id);
		steps = ConstantPlayer.HUMANSTEPS;
		
		FSM = BuilderFSM.getHumanFSM();
	}

	@Override
	public boolean isHuman() {
		return true;
	}

	@Override
	public String toString() {
		return ConstantPlayer.HUMAN;
	}

	@Override
	public void resetPlayer() {
		setSteps(ConstantPlayer.HUMANSTEPS);
		FSM = BuilderFSM.getHumanFSM();
	}
}
