package it.polimi.ingsw.comodiconficconi.gamemodel.actions;

/**
 * This interface gives a method to be implemented so that the visitor pattern
 * is applied
 * 
 * @author alessandro
 *
 */
public interface Visitable {
	/**
	 * This method has a Visitor interface as parameter on which will be called
	 * the notify method. Each class that implements this interface will have a
	 * method in the visitor interface
	 * 
	 * @param visitor
	 */
	void applyEffect(Visitor visitor);
}
