package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command that notify the need of coordinates for spotlight
 * 
 * @author davide
 *
 */
public class CommandAskCoordSpotLight extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4934782408323647472L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
