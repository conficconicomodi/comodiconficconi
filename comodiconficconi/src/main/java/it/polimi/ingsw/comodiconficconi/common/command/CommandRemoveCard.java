package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * command sent to notify the need to remove a card
 * 
 * @author davide
 *
 */
public class CommandRemoveCard extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7480280528786856537L;
	private Card card;

	public CommandRemoveCard(Card card) {
		this.card = card;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public Card getCard() {
		return card;
	}

}
