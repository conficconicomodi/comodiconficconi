package it.polimi.ingsw.comodiconficconi.client.userinterface.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * This class is a runnable and serves as a listener from the keyboard. It is
 * necessary in order to prevent server from blocking every time it asks
 * something to client
 * 
 * @author alessandro
 *
 */
public class Reader implements Runnable {

	private BufferedReader inKeyboard;
	private static final Logger LOGGER = Logger.getLogger(Reader.class);
	private List<InputListener> listeners;

	/**
	 * This constructor takes as a parameter a buffered reader and assigns it to
	 * the private attribute inKeyboard
	 * 
	 * @param inKeyboard
	 */
	public Reader(BufferedReader inKeyboard) {
		this.inKeyboard = inKeyboard;
		listeners = new ArrayList<InputListener>();

	}

	@Override
	public void run() {
		while (true) {
			try {
				String input = inKeyboard.readLine();
				notifyListeners(input);
			} catch (IOException e) {
				LOGGER.debug(e);
			}
		}

	}

	public void setListener(InputListener listener) {
		listeners.add(listener);
	}

	/**
	 * This method has to notify a new input from keyboard to all the listeners
	 * 
	 * @param input
	 */
	private void notifyListeners(String input) {
		for (InputListener listener : listeners)
			listener.notify(input);
	}
}
