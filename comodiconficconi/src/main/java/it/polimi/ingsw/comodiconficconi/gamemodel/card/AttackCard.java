package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * the card that give to the human player who use this card the skill to attack
 * 
 * @author davide
 *
 */
public class AttackCard extends ItemCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3150344329178738134L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);

	}

	@Override
	public String toString() {
		return ConstantCards.ATTACK;
	}
}
