package it.polimi.ingsw.comodiconficconi.client.controller;

import it.polimi.ingsw.comodiconficconi.client.ClientUtils;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterface;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAddCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordNoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordSpotLight;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDeath;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDisconnection;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNewPosition;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotValid;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotification;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotificationWithCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandRemoveCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartMatch;
import it.polimi.ingsw.comodiconficconi.common.command.CommandStartTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandTurnEnded;
import it.polimi.ingsw.comodiconficconi.common.command.CommandWin;

/**
 * this class represent the implementation of the command handler of the client
 * 
 * @author davide
 * @author alessandro
 *
 */
public class CommandHandlerClientImpl implements CommandHandlerClient {

	/** The game controller MVC */
	private ClientController gameController;
	private UserInterface userInterface;

	public CommandHandlerClientImpl(ClientController gameController,
			UserInterface userInterface) {
		this.gameController = gameController;
		this.userInterface = userInterface;
	}

	@Override
	public void applyCommand(CommandStartMatch startMatch) {
		userInterface.initializeMatch(startMatch.getBoard(),
				startMatch.getPlayer(), startMatch.getNameMap());
	}

	@Override
	public void applyCommand(CommandStartTurn startTurn) {
		userInterface.startTurn();
	}

	@Override
	public void applyCommand(CommandAskMove askMove) {
		userInterface.askMove(askMove.getActions());
	}

	@Override
	public void applyCommand(CommandNotification notification) {
		userInterface.notifyToAll(notification.getNotification());
	}

	@Override
	public void applyCommand(
			CommandNotificationWithCoordinates notificationWithCoord) {
		String coordinates = ClientUtils.coordToString(notificationWithCoord
				.getCoordinates());
		userInterface.notifyToAll(notificationWithCoord.getMsg() + "["
				+ coordinates + "]");
	}

	@Override
	public void applyCommand(CommandNewPosition newPosition) {
		if (newPosition.getPastMoves() != null)
			userInterface.setMovePlayer(newPosition.getNewPosition(),
					newPosition.getPastMoves());
		else
			userInterface.setMovePlayer(newPosition.getNewPosition());
	}

	@Override
	public void applyCommand(CommandAskCoordNoiseAnywhere askCoordNoiseAnywhere) {
		userInterface.askNoiseCoordinates();
	}

	@Override
	public void applyCommand(CommandAskCoordSpotLight askCoordSpotlight) {
		userInterface.askSpotlightCoordinates();
	}

	@Override
	public void applyCommand(CommandAskCoordMove askCoordMove) {
		userInterface.askMoveCoordinates();
	}

	@Override
	public void applyCommand(CommandAddCard addCard) {
		userInterface.addCard(addCard.getItem());
	}

	@Override
	public void applyCommand(CommandRemoveCard removeCard) {
		userInterface.removeCard(removeCard.getCard());
	}

	@Override
	public void applyCommand(CommandTurnEnded turnEnded) {
		userInterface.turnEnded();
	}

	@Override
	public void applyCommand(CommandDeath death) {
		userInterface.death();
	}

	@Override
	public void applyCommand(CommandWin win) {
		if (win.getWin())
			userInterface.win();
		else {
			userInterface.lose();
		}
	}

	@Override
	public void applyCommand(CommandNotValid notValid) {
		userInterface.commandNotValid();
	}

	@Override
	public void applyCommand(CommandCloseToClient closeToClient) {
		gameController.closeConnection();
	}

	@Override
	public void applyCommand(CommandDisconnection disconnection) {
		userInterface.disconnection();
	}
}
