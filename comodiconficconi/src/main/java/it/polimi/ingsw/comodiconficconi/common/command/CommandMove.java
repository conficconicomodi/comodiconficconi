package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command to make a move
 * 
 * @author davide
 *
 */
public class CommandMove extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2034555704818683637L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
		commandHandlerServer.applyCommand(this);
	}
}
