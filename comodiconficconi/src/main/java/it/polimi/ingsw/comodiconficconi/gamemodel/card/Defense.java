package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this card let a human player to survive an alien attack
 * 
 * @author davide
 *
 */
public class Defense extends ItemCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2952074061541619398L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.DEFENSE;
	}
}
