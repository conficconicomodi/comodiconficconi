package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * a command that report the death of the client's player in the game
 * 
 * @author davide
 *
 */
public class CommandDeath extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3039450958150837564L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {

		commandHandler.applyCommand(this);

	}

}
