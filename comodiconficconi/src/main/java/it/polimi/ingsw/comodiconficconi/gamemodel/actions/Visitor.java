package it.polimi.ingsw.comodiconficconi.gamemodel.actions;

import it.polimi.ingsw.comodiconficconi.gamemodel.CommandNotifier;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Adrenaline;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.AttackCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.BlockedHatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Defense;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseAnywhere;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.NoiseInSector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.OpenHatchCard;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Sedative;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Silence;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.SpotLight;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Teleport;

/**
 * This interface gives all the operations that can be performed
 * 
 * @author alessandro
 *
 */
public interface Visitor {

	/**
	 * method to set the command notifier of the visitor
	 * 
	 * @param commandNotifier
	 *            : the command notifier that will notify to player what is
	 *            happened
	 */
	void setNotifier(CommandNotifier commandNotifier);

	/** Method invoked by Attack item card */
	void visit(AttackCard attackCard);

	/** Method invoked by Defense item card */
	void visit(Defense defenseCard);

	/** Method invoked by Teleport item card */
	void visit(Teleport teleportCard);

	/** Method invoked by Adrenaline item card */
	void visit(Adrenaline adrenalineCard);

	/** Method invoked by Sedative item card */
	void visit(Sedative sedativeCard);

	/** Method invoked by Spotlight item card */
	void visit(SpotLight spotlightCard);

	/** Method invoked by Noise in Sector card */
	void visit(NoiseInSector noiseInSector, boolean item);

	/** Method invoked by Noise anywhere card */
	void visit(NoiseAnywhere noiseAnywhere, boolean item);

	/** method invoked by Silence card */
	void visit(Silence silenceCard);

	/** method invoked by Escape Hatch card */
	void visit(OpenHatchCard openHatchCard);

	/** method invoked by Blocked Hatch card */
	void visit(BlockedHatchCard blockedHatchCard);

	/** Method invoked by Hatch sector */
	void visitDrawHatchCard();
	/** method invoked by Dangerous sector */
	void drawSectorCard();
	/**
	 * 
	 * @return true if the notifier is set false otherwise
	 */
	boolean notifierSet();
}