package it.polimi.ingsw.comodiconficconi.client.network;

import it.polimi.ingsw.comodiconficconi.client.network.utils.SubjectCommandToClientAbstract;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.log4j.Logger;

/**
 * this runnable listen on socket channel and notify to SocketInterface if there
 * are command for client
 * 
 * @author davide
 *
 */
public class SocketListener extends SubjectCommandToClientAbstract implements
		Runnable {
	private ObjectInputStream inSocket;
	private static final Logger LOGGER = Logger.getLogger(SocketListener.class);

	/**
	 * 
	 * @param inSocket
	 *            : the channel that this runnable will listen for object that
	 *            are command from Server to Client
	 */
	public SocketListener(ObjectInputStream inSocket) {
		this.inSocket = inSocket;
	}

	@Override
	public void run() {
		LOGGER.trace("start reading");
		while (true) {
			CommandServerToClient command;
			try {
				command = (CommandServerToClient) inSocket.readObject();
				notifyObserver(command);
			} catch (ClassNotFoundException | IOException e) {
				LOGGER.fatal(e);
				break;
			}
		}
	}

}