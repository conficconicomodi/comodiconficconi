package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * This class instantiates a new Player in the match
 * 
 * @author alessandro
 *
 */
public abstract class Player implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6145217889766171158L;

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.gamemodel.player.Player.class);

	protected int playerId;
	/** The sector in which is the player */
	protected Coordinates currentSector;
	/** If the player has been killed it becomes true */
	protected boolean dead;
	/** Item cards possessed by the player */
	protected List<Card> items;
	/** Number of steps that a player can make */
	protected int steps;
	/** List of past moves of the player */
	protected List<Coordinates> pastMoves;

	protected Map<Integer, Map<Object, Integer>> FSM;
	protected int playerState;

	public Player(int id) {
		LOGGER.debug("setting player");
		playerId = id;
		playerState = ConstantFSM.BEFORE_MOVE;
		items = new ArrayList<Card>();
		pastMoves = new ArrayList<Coordinates>();
	}

	/**
	 * This method sets the new position after a movement
	 * 
	 * @param newCoordinates
	 */
	public void setCurrentSector(Coordinates newCoordinates) {
		currentSector = newCoordinates;
	}

	/**
	 * This method returns the current position of the player
	 * 
	 * @return
	 */
	public Coordinates getCurrentSector() {
		return currentSector;
	}

	/**
	 * This method returns the player's id number
	 * 
	 * @return
	 */
	public int getPlayerId() {
		return playerId;
	}

	/**
	 * This method returns the state of the player. If he is or is not dead
	 * 
	 * @return
	 */
	public boolean isDead() {
		return dead;
	}

	/**
	 * This method sets to true the attribute dead.
	 */
	public void setDead() {
		dead = true;
	}

	/**
	 * This method returns the number of steps that a player can do to move in
	 * the board.
	 * 
	 * @return
	 */
	public int getSteps() {
		return steps;
	}

	/**
	 * This method sets a new value of steps.
	 * 
	 * @param newSteps
	 */
	public void setSteps(int newSteps) {
		steps = newSteps;
	}

	/**
	 * This method adds an item card to the array of cards
	 * 
	 * @param itemCard
	 */
	public void addItem(Card item) {
		items.add(item);
	}

	/**
	 * This method removes a card from the array of itemcards
	 * 
	 * @param itemCard
	 */
	public void removeItem(Card item) {
		Card itemToRemove = null;
		for (Card card : items)
			if (card.toString().equals(item.toString()))
				itemToRemove = card;
		items.remove(itemToRemove);
	}

	/**
	 * This method returns true if the items array is full and one item card has
	 * to be discarded
	 * 
	 * @return
	 */
	public boolean fullItems() {
		return items.size() > 2;
	}

	/**
	 * This method adds a new coordinates to the past moves list of the player
	 * 
	 * @param move
	 */
	public void addMove(Coordinates move) {
		pastMoves.add(move);
	}

	/**
	 * This method returns the past moves list of the player
	 * 
	 * @return
	 */
	public List<Coordinates> getPastMoves() {
		return pastMoves;
	}

	/**
	 * This method takes as parameter a card and checks if the player owns it or
	 * not
	 * 
	 * @param card
	 * @return
	 */
	public boolean hasThisCard(Card card) {
		for (Card ownedCard : items)
			if (ownedCard != null
					&& ownedCard.toString().equals(card.toString()))
				return true;
		return false;
	}

	/**
	 * This method returns true if the player has a defense card
	 * 
	 * @return
	 */
	public boolean hasDefense() {
		for (Card card : items)
			if (ConstantCards.DEFENSE.equals(card.toString()))
				return true;
		return false;
	}

	/**
	 * This method returns the defense card in case of attack
	 * 
	 * @return
	 */
	public Card getDefenseCard() {
		for (Card card : items)
			if (ConstantCards.DEFENSE.equals(card.toString()))
				return card;
		return null;
	}

	/**
	 * This method returns all the cards owned by the player
	 * 
	 * @return
	 */
	public List<Card> getAllCards() {
		List<Card> itemsToReturn = new ArrayList<Card>(this.items);
		return itemsToReturn;
	}

	public Map<Integer, Map<Object, Integer>> getFSM() {
		return FSM;
	}

	public int getState() {
		return playerState;
	}

	public void setState(int newState) {
		playerState = newState;
	}

	/**
	 * This method resets the state of the player for the next turn
	 */
	public abstract void resetPlayer();

	/**
	 * This method returns true if the player is human, false otherwise
	 * 
	 * @return boolean
	 */
	public abstract boolean isHuman();
}