package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 * This class draws the game console panel which will be devided in three parts:
 * 1) JTextArea that will visualize all the logs sent by the server 2) Card
 * panel which will visualize the 3 cards that a player owns 3) Possible actions
 * that a player can do in its turn
 * 
 * @author alessandro
 *
 */
public class GameConsole extends JPanel {

	private static final long serialVersionUID = 1L;

	private JTextArea msg;
	private CardPanel cards;
	private PossibleActions actions;

	/**
	 * In this constructor is set the size and preferred size of the game board
	 * panel, the border layout, the empty border and the background color
	 * (white). Then there is the text area where will be displayed all the
	 * messages coming from server and it is set into a JScollPane which will
	 * allow to scroll al the messages, then the card panel is instantiated and
	 * finally the action panel, which will display all the possible actions of
	 * the user in a given moment. The various actions can be added or removed
	 * every time with the relative public methods of this class
	 * 
	 * 
	 * @param dimension
	 */
	public GameConsole(Dimension dimension) {

		setSize(dimension);
		setPreferredSize(dimension);
		setLayout(new BorderLayout());
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setBackground(Color.WHITE);

		msg = new JTextArea();
		msg.setLineWrap(true);
		msg.setEditable(false);
		msg.setBorder(new EmptyBorder(5, 5, 5, 5));
		msg.setBackground(new Color(240, 240, 240));
		msg.setFont(new Font("Consolas", 0, 12));

		JScrollPane scrollPane = new JScrollPane(msg);
		Border border = BorderFactory.createLoweredBevelBorder();
		scrollPane.setBorder(border);
		scrollPane.setSize(new Dimension(getWidth(), getHeight() * 1 / 3 - 10));
		scrollPane.setPreferredSize(new Dimension(getWidth(),
				getHeight() * 1 / 3 - 10));
		JScrollBar vertical = scrollPane.getVerticalScrollBar();
		vertical.setPreferredSize(new Dimension(0, 0));

		cards = new CardPanel();
		cards.setSize(new Dimension(getWidth(), getHeight() * 1 / 3 - 10));
		cards.setPreferredSize(new Dimension(getWidth(),
				getHeight() * 1 / 3 - 10));
		cards.setBorder(new EmptyBorder(5, 5, 5, 5));

		actions = new PossibleActions(new Dimension(getWidth(),
				getHeight() * 1 / 3 - 10));
		actions.setBorder(new EmptyBorder(5, 5, 5, 5));

		add(scrollPane, BorderLayout.NORTH);
		add(cards, BorderLayout.CENTER);
		add(actions, BorderLayout.SOUTH);
	}

	public JTextArea getMsgConsole() {
		return msg;
	}

	public CardPanel getCardPanel() {
		return cards;
	}

	public PossibleActions getPossibleActionsPanel() {
		return actions;
	}

	public void showAttack() {
		actions.showAttack();
		actions.repaint();
		actions.validate();
	}

	public void showPassTurn() {
		actions.showPassTurn();
		actions.repaint();
		actions.validate();
	}

	public void showMove() {
		actions.showMove();
		actions.repaint();
		actions.validate();
	}

	public void showDrawCard() {
		actions.showDrawCard();
		actions.repaint();
		actions.validate();
	}

	public void showDiscardItem() {
		actions.showDiscarItem();
		actions.repaint();
		actions.validate();
	}

	public void addCard(Card card) {
		cards.addCard(card);
		cards.repaint();
		cards.validate();
	}

	public void removeCard(Card card) {
		cards.removeCard(card);
		cards.repaint();
		cards.validate();

	}
}