package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command sent by the server to notify the client that the turn is ended
 * 
 * @author davide
 *
 */
public class CommandTurnEnded extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5688069581674526525L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
