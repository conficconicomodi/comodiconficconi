package it.polimi.ingsw.comodiconficconi.client;

import it.polimi.ingsw.comodiconficconi.client.controller.ClientController;
import it.polimi.ingsw.comodiconficconi.client.controller.CommandHandlerClientImpl;
import it.polimi.ingsw.comodiconficconi.client.network.NetworkInterface;
import it.polimi.ingsw.comodiconficconi.client.network.NetworkInterfaceFactory;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterface;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterfaceFactory;
import it.polimi.ingsw.comodiconficconi.client.userinterface.cli.Printer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

/**
 * This is the main class of the client. The user who launches this main can
 * choose between two kind of connections with the server (Socket or RMI) and
 * two kind of user interface (CLI or GUI) After choosing the connection and the
 * user interface this class instantiates the client controller, the network
 * interface and the user interface.
 * 
 * @author alessandro
 *
 */
public class ClientMainClass {

	private static final Logger LOGGER = Logger
			.getLogger(ClientMainClass.class);
	private static BufferedReader inKeyboard;

	/**
	 * This is a hidden constructor so that this class can never be instantiated
	 */
	private ClientMainClass() {
	}

	public static void main(String[] args) {
		NetworkInterface networkInterface;
		UserInterface userInterface;
		CommandHandlerClient handler;

		inKeyboard = new BufferedReader(new InputStreamReader(System.in));
		String read = "";
		while (!"1".equals(read) && !"2".equals(read)) {
			Printer.println("Choose network interface to use:");
			Printer.println("1 - Socket");
			Printer.println("2 - RMI");
			try {
				read = inKeyboard.readLine();
			} catch (IOException e) {
				LOGGER.warn(e);
			}
			if (!"1".equals(read) && !"2".equals(read))
				Printer.println("Command not recognized!");
		}
		int choice = Integer.parseInt(read);
		networkInterface = NetworkInterfaceFactory.getNetworkInterface(choice);

		read = "";
		while (!"1".equals(read) && !"2".equals(read)) {
			Printer.println("Choose user interface:");
			Printer.println("1 - Command Line Interface");
			Printer.println("2 - Graphic User Interface");
			try {
				read = inKeyboard.readLine();
			} catch (IOException e) {
				LOGGER.warn(e);
			}
			if (!"1".equals(read) && !"2".equals(read))
				Printer.println("Command not recognized!");
		}
		choice = Integer.parseInt(read);

		ClientController gameController = new ClientController(networkInterface);

		userInterface = UserInterfaceFactory.getUserInterface(choice,
				gameController);
		gameController.setUserInterface(userInterface);
		handler = new CommandHandlerClientImpl(gameController, userInterface);
		gameController.setCommandHandler(handler);
		try {
			networkInterface.connect();
		} catch (Exception e) {
			LOGGER.fatal(e);
			userInterface.serverOutOfOrder();
		}
	}
}
