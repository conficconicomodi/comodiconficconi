package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * a command used to notify a message
 * 
 * @author davide
 *
 */
public class CommandNotification extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -686915693462627077L;
	private String notification;

	/**
	 *
	 * @param notification
	 *            : the message will be notified
	 */
	public CommandNotification(String notification) {
		this.notification = notification;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);

	}

	public String getNotification() {
		return notification;
	}

}
