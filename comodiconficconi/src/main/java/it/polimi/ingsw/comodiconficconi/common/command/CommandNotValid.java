package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command sent that states last command received was not valid
 * 
 * @author davide
 *
 */
public class CommandNotValid extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1008471639412991819L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);

	}
}
