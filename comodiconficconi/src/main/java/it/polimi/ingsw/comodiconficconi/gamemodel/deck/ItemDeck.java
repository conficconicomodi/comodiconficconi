package it.polimi.ingsw.comodiconficconi.gamemodel.deck;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;

/**
 * the class of the item deck
 * 
 * @author davide
 *
 */

public class ItemDeck extends Deck {

	/**
	 * construct the deck empty or full as the default value of the game, the
	 * deck is already shuffled
	 * 
	 * @param empty
	 *            : true if you want to build an empty deck false if you want to
	 *            get a default built deck
	 */
	public ItemDeck(boolean empty) {
		super();
		if (!empty) {

			for (int i = 0; i < ConstantCards.MAXADRENALINECARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.ADRENALINECODE));

			}

			for (int i = 0; i < ConstantCards.MAXATTACKCARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.ATTACKCODE));

			}

			for (int i = 0; i < ConstantCards.MAXDEFENSECARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.DEFENSECODE));

			}

			for (int i = 0; i < ConstantCards.MAXSEDATIVECARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.SEDATIVECODE));

			}

			for (int i = 0; i < ConstantCards.MAXSPOTLIGHTCARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.SPOTLIGHTCODE));

			}
			for (int i = 0; i < ConstantCards.MAXTELEPORTCARD; i++) {
				cards.add(FactoryCard.getCard(ConstantCards.TELEPORTCODE));

			}
			this.shuffle();
		}
	}

}
