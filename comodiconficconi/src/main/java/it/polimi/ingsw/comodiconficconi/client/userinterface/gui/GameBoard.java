package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.Hexagon;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.objects.PlayerIcon;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.objects.PlayerIconFactory;
import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.GameBoardListener;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

/**
 * This class creates and visualizes a new map, which is given from the server.
 * It has methods that consent change the position of the player icon and the
 * processor of the mouse event, which is the click.
 * 
 * @author alex
 *
 */
public class GameBoard extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(GameBoard.class);

	private int screenWidth;
	/** List of hexagons */
	private transient List<Hexagon> board;

	private PlayerIcon icon;
	private transient Image background;

	/** Side of each hexagon */
	private int side;
	/** distance between the center and each side of the hexagon */
	private int top;

	/**
	 * List of listeners who are observing the click of the various sectors in
	 * the map
	 */
	private transient List<GameBoardListener> listeners = new ArrayList<GameBoardListener>();

	/**
	 * This constructor needs a dimension of the portion of screen that can
	 * occupy and it will set his own height and width depending on the size of
	 * each hexagon. It also needs an hashmap of the board which is sent from
	 * the server and also the player connected to the client so that it knows
	 * where to put the player icon and more important what kind of icon to use.
	 * It also sets the background image and the mouse listener for the click on
	 * the sectors
	 * 
	 * @param board
	 * @param dimension
	 */
	public GameBoard(Map<Coordinates, Sector> board, Dimension dimension) {
		side = (dimension.width * 2 / 3) / 37;
		top = (int) (side * 0.8660254037844);
		screenWidth = dimension.width;
		setSize(getPreferredSize());
		setPreferredSize(getPreferredSize());

		this.board = new ArrayList<Hexagon>();

		try {
			background = ImageIO.read(this.getClass().getResource(
					ConstantClient.BACKGROUNDIMAGE));
		} catch (IOException e) {
			LOGGER.error(e);
		}
		background = background.getScaledInstance(getWidth(), getHeight(),
				Image.SCALE_SMOOTH);

		for (Coordinates coord : board.keySet()) {
			this.board.add(HexagonFactory.getHexagon(board.get(coord)
					.toString(), coord, side));
		}

		MyMouseListener mouseListener = new MyMouseListener();
		addMouseListener(mouseListener);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.drawImage(background, 0, 0, null);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		for (Hexagon hex : board)
			if (hex != null)
				hex.drawHexagon(g2);

		icon.paintComponent(g);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(screenWidth * 2 / 3, 28 * top
				+ ConstantClient.BORDER * 2 - top);
	}

	/**
	 * This method adds a listener that will invoke a method whenever an hexagon
	 * is clicked
	 * 
	 * @param listener
	 */
	public void addListeners(GameBoardListener listener) {
		listeners.add(listener);
	}

	/**
	 * This method informs all the listeners that a specified hexagon has been
	 * clicked
	 * 
	 * @param hex
	 */
	private void notifyCoordinates(Hexagon hex) {
		for (GameBoardListener listener : listeners)
			listener.notify(hex.getCoordinates());
	}

	/**
	 * This method retrieves the hexagon that corresponds to the given
	 * coordinates
	 * 
	 * @param coord
	 * @return
	 */
	public Hexagon getHexagon(Coordinates coord) {
		for (Hexagon hex : board)
			if (hex != null && hex.getCoordinates().equals(coord))
				return hex;
		return null;
	}

	/**
	 * This method changes the position of the player icon
	 * 
	 * @param coord
	 */
	public void changePosition(Coordinates coord) {
		icon.changePosition(getHexagon(coord).getCenter());
		repaint();
	}

	/**
	 * This method adds the player icon to the game board
	 * 
	 * @param player
	 */
	public void addPlayerIcon(Player player) {
		icon = PlayerIconFactory.getIcon(getHexagon(player.getCurrentSector()),
				side, top, player.toString());
	}

	/**
	 * This class serves as a listener for the click of the mouse
	 * 
	 * @author alessandro
	 *
	 */
	class MyMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			if (e.getID() == MouseEvent.MOUSE_CLICKED)
				for (Hexagon hex : board)
					if (hex != null && hex.hasThisCoords(x, y))
						notifyCoordinates(hex);
		}
	}
}
