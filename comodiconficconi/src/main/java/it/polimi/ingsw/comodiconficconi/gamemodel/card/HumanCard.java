package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * this class represent the card that let a player be considered as a human
 * 
 * @author davide
 *
 */
public class HumanCard extends CharacterCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3569627921153176511L;

	@Override
	public String toString() {
		return ConstantPlayer.HUMAN;
	}
}