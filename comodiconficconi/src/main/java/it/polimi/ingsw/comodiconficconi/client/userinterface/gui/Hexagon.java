package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.client.ClientUtils;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

/**
 * This class creates a new hexagon with its own position and his background
 * image and text.
 * 
 * @author alessandro
 *
 */
public class Hexagon {

	private static final Logger LOGGER = Logger.getLogger(Hexagon.class);

	private Polygon hex;
	private Point centerPoint;
	private int[] xPoints;
	private int[] yPoints;
	private Coordinates coord;
	private String text;
	private Image image;
	/** side of the hexagon */
	private int side;
	/** distance between the center and each side */
	private int top;

	/**
	 * 
	 * This constructor takes the side of the hexagon, the coordinates in the
	 * board and the image to be displayed. Thanks to the coordinates and the
	 * side it can calculate through mathematical algorithms the vertices of
	 * each hexagon starting from the center point and then gives an image to
	 * the created hexagon
	 * 
	 * @param side
	 * @param coord
	 * @param sectorImage
	 */
	public Hexagon(int side, Coordinates coord, String sectorImage) {

		top = (int) (side * 0.8660254037844);

		this.side = side;
		this.coord = coord;

		try {
			image = ImageIO.read(this.getClass().getResource(sectorImage));
		} catch (IOException e) {
			LOGGER.error(e);
		}
		image = image.getScaledInstance(2 * side - 1, 2 * top - 1,
				Image.SCALE_SMOOTH);

		centerPoint = new Point(coord.getY() * ((side * 3) / 2)
				+ ConstantClient.BORDER, (coord.getY() % 2) * top + 2
				* coord.getX() * top + ConstantClient.BORDER);

		if (ConstantClient.ALIENSECTOR.equals(sectorImage)
				|| ConstantClient.HUMANSECTOR.equals(sectorImage)
				|| ConstantClient.HATCHSECTOR.equals(sectorImage))
			text = "";
		else
			text = ClientUtils.coordToString(coord);

		setVertices();
		hex = new Polygon(xPoints, yPoints, 6);
	}

	private void setVertices() {
		int x = (int) centerPoint.getX();
		int y = (int) centerPoint.getY();

		xPoints = new int[] { x - side / 2, x + side / 2, x + side,
				x + side / 2, x - side / 2, x - side };
		yPoints = new int[] { y - top, y - top, y, y + top, y + top, y };
	}

	/**
	 * with this method the created hexagon will be painted. First the hexagon
	 * will be drawn and then the image and then the text is put in the center
	 * 
	 * @param g
	 */
	public void drawHexagon(Graphics2D g) {
		g.setStroke(new BasicStroke(2f));
		g.setColor(new Color(130, 130, 130));
		g.drawPolygon(hex);

		g.drawImage(image, centerPoint.x - side + 1, centerPoint.y - top + 1,
				null);

		g.setColor(new Color(100, 100, 100));
		g.setFont(new Font("Consolas", Font.PLAIN, 12));
		g.drawString(text, centerPoint.x - 10, centerPoint.y + 4);
	}

	/**
	 * This method returns true if an hexagon is situated in the clicked
	 * coordinates
	 * 
	 * @param mouseX
	 * @param mouseY
	 * @return boolean
	 */
	public boolean hasThisCoords(int mouseX, int mouseY) {
		return hex.contains(mouseX, mouseY);
	}

	// Getters

	public Coordinates getCoordinates() {
		return coord;
	}

	public Polygon getHexagon() {
		return hex;
	}

	public String getText() {
		return text;
	}

	public Point getCenter() {
		return centerPoint;
	}
}
