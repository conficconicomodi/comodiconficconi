package it.polimi.ingsw.comodiconficconi.gamemodel;

import it.polimi.ingsw.comodiconficconi.constants.ConstantMatch;
import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.VisitorConcrete;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Board;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.CharacterDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.Deck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.HatchDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.ItemDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.deck.SectorDeck;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.PlayerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * This class creates and initiates the match. It has all the functions that
 * change the state of the game
 * 
 * @author alessandro
 * @author davide
 *
 */
public class Match {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.gamemodel.Match.class);

	private Visitor visitor;
	private CommandNotifier notifier = null;

	/** New board with all the necessary sectors */
	private Board board;
	/** List of players still connected to the game */
	private List<Player> players;
	/** Player who has to do the moves */
	private Player currentPlayer;
	/** Current turn */
	private int currentTurn;

	/**
	 * All kinds of deck that has to be created Item deck have the graveyard
	 * where to put all the used items
	 */
	private Deck itemDeck;
	private Deck itemDeckGraveyard;
	private Deck sectorDeck;
	private Deck characterDeck;
	private Deck escapeHatchDeck;

	/**
	 * This constructor initializes all the decks, the right board and the list
	 * of players which has to be filled later on
	 * 
	 * @param map
	 *            containing the string of the right map to load
	 * @param numberOfPlayers
	 *            currently playing the game
	 */
	public Match(String map, int numberOfPlayers) {

		board = new Board(map);
		currentTurn = 0;

		visitor = new VisitorConcrete(this);
		// Creating decks
		itemDeck = new ItemDeck(false);
		itemDeckGraveyard = new ItemDeck(true);
		sectorDeck = new SectorDeck(false);
		characterDeck = new CharacterDeck(numberOfPlayers, false);
		escapeHatchDeck = new HatchDeck(false);

		// players
		players = new ArrayList<Player>();
	}

	/**
	 * set the command notifier for users interaction
	 * 
	 * @param commandNotifier
	 *            : the command notifier used by the match
	 */
	public void setNotifier(CommandNotifier commandNotifier) {
		notifier = commandNotifier;
		visitor.setNotifier(notifier);
	}

	//
	// The following methods are setters and getters
	//

	/**
	 * This method adds a new player to the list and returns the player that has
	 * been created.
	 * 
	 * @param id
	 *            of the player
	 * @return the created player
	 */
	public Player setPlayer(int id) {
		Player player = PlayerFactory.getPlayer(drawCharacterCard().toString(),
				id);
		players.add(player);
		addPlayerToBoard(player);
		return player;
	}

	/**
	 * This method adds the player to the board
	 * 
	 * @param player
	 *            that has to be added
	 */
	private void addPlayerToBoard(Player player) {
		board.setInitialPosition(player);
	}

	/**
	 * This method sets the player who starts
	 */
	public void setInitialPlayer() {
		currentPlayer = players.get(0);
	}

	/**
	 * This method sets the next player
	 */
	public void setNextPlayer() {
		int index = MatchUtils.getPositionOfAPlayer(players, currentPlayer);
		if (index + 1 != players.size())
			currentPlayer = players.get(index + 1);
		else {
			currentPlayer = players.get(0);
			currentTurn++;
		}
	}

	// Getters

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public int getCurrentTurn() {
		return currentTurn;
	}

	public Board getBoard() {
		return board;
	}

	//
	// The following methods are the deck handlers
	//

	/**
	 * This method returns a Character card so that a Player can be instantiated
	 * 
	 * @return card that has been drawn
	 */
	private Card drawCharacterCard() {
		return characterDeck.draw();
	}

	/**
	 * This method is used to draw a sector card when someone goes into a
	 * dangerous sector
	 * 
	 * @return sector card that is drawn
	 */
	public Card drawSectorCard() {
		if (!sectorDeck.isEmpty())
			return sectorDeck.draw();
		sectorDeck = MatchUtils.getNewSectorDeck();
		return sectorDeck.draw();
	}

	/**
	 * This method returns an escape hatch card when a human goes into it
	 * 
	 * @return escape hatch card that has been drawn
	 */
	public Card drawEscapeHatchCard() {
		return escapeHatchDeck.draw();
	}

	/**
	 * This method retrieves a card from the item deck
	 * 
	 * @return item card that has been drawn, null if item decks are empty
	 */
	public Card drawItemCard() {
		LOGGER.debug("going to draw item");
		if (!itemDeck.isEmpty()) {
			LOGGER.debug("drawing");
			return itemDeck.draw();
		} else if (!itemDeckGraveyard.isEmpty()) {
			MatchUtils.shuffleItemDeck(itemDeck, itemDeckGraveyard);
			LOGGER.debug(itemDeck.isEmpty());
			return itemDeck.draw();
		}
		return null;
	}

	//
	// The following methods are used to apply given commands
	//

	/**
	 * moves the currentPlayer to the specified position
	 * 
	 * @param finalPosition
	 * @return the final sector in which the player has moved
	 */
	public Sector movePlayer(Coordinates finalPosition) {
		currentPlayer.addMove(finalPosition);
		return board.movePlayer(currentPlayer, finalPosition);
	}

	/**
	 * This method uses an item card
	 * 
	 * @param card
	 *            that has to be used
	 */
	public void useCard(Card card) {
		card.applyEffect(visitor);

	}

	/**
	 * This method removes the Card from Item available
	 * 
	 * @param card
	 *            removed from the current player's items
	 */
	public void removeCard(Card card) {
		currentPlayer.removeItem(card);
		itemDeckGraveyard.addCard(card);
	}

	/**
	 * This method checks the current player can draw the item card. If the item
	 * decks are empty no card will be drawn and if the current player has
	 * already full items a notify will be sent to him and he will discard or
	 * use an item in order to draw the item
	 */
	public void checkDrawItem() {
		Card item;
		if (!itemDeck.isEmpty() || !itemDeckGraveyard.isEmpty()) {
			if (!currentPlayer.fullItems()) {
				item = drawItemCard();
				currentPlayer.addItem(item);
				if (visitor.notifierSet())
					notifier.notifyNewItem(item);
			} else if (visitor.notifierSet())
				notifier.notifyFullItems();
		} else if (visitor.notifierSet())
			notifier.genericNotifyToAll("The item deck is empty");
	}

	/**
	 * This method attacks in the current sector and notifyies everyone of the
	 * attack and if someone has been killed and removes from the list of
	 * players all the killed players. If there aren't anymore humans the game
	 * ends and all the aliens win the match
	 */
	public void attack() {
		if (visitor.notifierSet())
			notifier.genericNotifyWithCoordinates(
					"Player " + currentPlayer.getPlayerId() + " attacks in ",
					currentPlayer.getCurrentSector());
		Set<Player> playersToKill = new HashSet<Player>(board.getSector(
				currentPlayer.getCurrentSector()).getPlayersInside());
		playersToKill.remove(currentPlayer);
		for (Player player : playersToKill) {
			if (player.isHuman() && player.hasDefense())
				humanWithDefense(player);
			else {
				killPlayer(player);
				if (visitor.notifierSet())
					notifier.notifyDeath(player);
				if (!currentPlayer.isHuman() && player.isHuman())
					currentPlayer.setSteps(ConstantPlayer.HUNGRYALIENSTEPS);
			}
		}
		if (finishHuman()) {
			Map<Player, Boolean> winners = lastHumanDied();
			if (visitor.notifierSet())
				notifier.notifyWinners(winners);
		}
	}
	
	private void humanWithDefense(Player player){
		Card defense = player.getDefenseCard();
		if (visitor.notifierSet())
			notifier.notifyDiscardItem(defense, player);
		player.removeItem(defense);
		itemDeckGraveyard.addCard(defense);
	}
	
	/**
	 * Apply the effects of a sector
	 * 
	 * @param sector
	 *            that has to apply the effect
	 */
	public void sectorEffect(Sector sector) {
		if (sector.hasToDraw())
			sector.applyEffect(visitor);
	}

	/**
	 * This method kills a player after an attack in the sector where the player
	 * was
	 * 
	 * @param player
	 *            that has to be killed
	 */
	public void killPlayer(Player player) {
		LOGGER.debug("Killing the player: " + player);
		discardCards(player);
		player.setDead();
		board.getSector(player.getCurrentSector()).removePlayer(player);
		players.remove(player);
	}

	/**
	 * this method discards all item cards of a given player
	 * 
	 * @param player
	 *            that discard all cards
	 */
	public void discardCards(Player player) {
		List<Card> cards = player.getAllCards();
		for (Card card : cards) {
			LOGGER.trace(card);
			itemDeckGraveyard.addCard(card);
			player.removeItem(card);
		}
	}

	/**
	 * this method checks if there aren't any more turns to play
	 * 
	 * @return true if turns are finished, false otherwise
	 */
	public boolean finishTurn() {
		if (players.get(players.size() - 1).equals(getCurrentPlayer())
				&& currentTurn + 1 == ConstantMatch.MAXTURNS)
			return true;
		return false;
	}

	/**
	 * This methods checks if there are winners and who they are
	 * 
	 * @return a map that contain player and if he win
	 */
	public Map<Player, Boolean> checkWinners() {
		Map<Player, Boolean> winners = new HashMap<Player, Boolean>();
		if (finishTurn())
			for (Player player : players) {
				LOGGER.trace("turn finish");
				if (player.isHuman())
					winners.put(player, false);
				else
					winners.put(player, true);
			}
		return winners;
	}

	/**
	 * This method checks if there aren't anymore humans in the game
	 * 
	 * @return true if there aren't human alive any more
	 */
	public boolean finishHuman() {
		for (Player player : players)
			if (player.isHuman())
				return false;

		return true;
	}

	/**
	 * This method checks if there aren't anymore escape hatches
	 * 
	 * @return true if there aren't open hatch sector
	 */
	public boolean finishOpenHatch() {
		List<Sector> hatches = getBoard().getHatchSector();
		for (Sector sector : hatches)
			if (sector.canMoveIn())
				return false;
		return true;
	}

	/**
	 * method to check if player is the last human
	 * 
	 * @param player
	 *            to be checked
	 * @return true if the player is the last human, false otherwise
	 */
	public boolean isLastHuman(Player player) {
		List<Player> list = new ArrayList<Player>();
		for (Player playerToCheck : players)
			if (!playerToCheck.equals(player))
				list.add(playerToCheck);
		for (Player playerToCheck : list) {
			if (playerToCheck.isHuman())
				return false;
		}
		return true;
	}

	/**
	 * method invoked when last human in game died to get the map of winners
	 * 
	 * @return a map Player, as key, Boolean that is true when win false
	 *         otherwise
	 */
	public Map<Player, Boolean> lastHumanDied() {
		Map<Player, Boolean> winners = new HashMap<Player, Boolean>();
		if (finishHuman())
			for (Player player : players)
				winners.put(player, true);
		return winners;
	}

	/**
	 * method invoked when last human escaped
	 * 
	 * @return a map Player, as key, Boolean that is true when win false
	 *         otherwise
	 */
	public Map<Player, Boolean> lastHumanEscaped(Player player) {
		Map<Player, Boolean> winners = new HashMap<Player, Boolean>();
		if (isLastHuman(player))
			for (Player playerToCheck : players)
				if (!playerToCheck.isHuman() && !playerToCheck.equals(player))
					winners.put(playerToCheck, false);
		return winners;
	}

	/**
	 * method invoked when there are no more hatches
	 * 
	 * @return a map Player, as key, Boolean that is true when win false
	 *         otherwise
	 */
	public Map<Player, Boolean> noMoreHatches() {
		Map<Player, Boolean> winners = new HashMap<Player, Boolean>();
		if (finishOpenHatch())
			for (Player player : players)
				if (player.isHuman() && !player.equals(currentPlayer))
					winners.put(player, false);
				else if (!player.isHuman())
					winners.put(player, true);
		return winners;
	}

}