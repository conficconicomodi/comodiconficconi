package it.polimi.ingsw.comodiconficconi.server;

import org.apache.log4j.Logger;

/**
 * This class is the Server launcher that create the listener for connection to
 * the game on RMI channel, and on Socket channel, when reached the right number
 * of players the game will start and it will all handled by a special handler
 * for the game
 *
 */
public class ServerLauncher {
	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.ServerLauncher.class);

	private ServerLauncher() {
	}

	public static void main(String[] args) {
		Thread t = null;
		try {
			ServerStarter server = ServerStarter.getInstance();
			t = new Thread(server);
			t.start();
		} catch (Exception e) {
			LOGGER.fatal(e);
			t.interrupt();
		}
	}
}
