package it.polimi.ingsw.comodiconficconi.client.userinterface.gui.objects;

import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.Hexagon;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * This class builds a new player icon depending on the identity of the player
 * (Human or Alien)
 * 
 * @author alessandro
 *
 */
public class PlayerIconFactory {

	private PlayerIconFactory() {
	}

	/**
	 * This static method returns the player icon built
	 * 
	 * @param hex
	 * @param side
	 * @param top
	 * @param name
	 * @return
	 */
	public static PlayerIcon getIcon(Hexagon hex, int side, int top, String name) {
		switch (name) {
		case ConstantPlayer.HUMAN:
			return new PlayerIcon(hex, top, side, ConstantClient.HUMANICON);
		case ConstantPlayer.ALIEN:
			return new PlayerIcon(hex, top, side, ConstantClient.ALIENICON);
		default:
			return null;
		}

	}
}
