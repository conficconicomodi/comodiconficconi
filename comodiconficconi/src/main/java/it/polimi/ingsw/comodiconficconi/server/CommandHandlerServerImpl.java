package it.polimi.ingsw.comodiconficconi.server;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskCoordMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAskMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandAttack;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandDiscardItem;
import it.polimi.ingsw.comodiconficconi.common.command.CommandEndTurn;
import it.polimi.ingsw.comodiconficconi.common.command.CommandMove;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNewPosition;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotValid;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotificationWithCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandRemoveCard;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSendCoordinates;
import it.polimi.ingsw.comodiconficconi.common.command.CommandSolveSector;
import it.polimi.ingsw.comodiconficconi.common.command.CommandTurnEnded;
import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.constants.ConstantFSM;
import it.polimi.ingsw.comodiconficconi.gamemodel.Match;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * This class dispatches all the commands incoming from client
 * 
 * @author davide
 * @author alessandro
 *
 */
public class CommandHandlerServerImpl implements CommandHandlerServer {

	private MatchHandler handler;
	private Match match;

	public CommandHandlerServerImpl(MatchHandler matchHandler, Match match) {
		this.handler = matchHandler;
		this.match = match;
	}

	private Player getCurrentPlayer() {
		return match.getCurrentPlayer();
	}

	@Override
	public void applyCommand(CommandMove move) {
		handler.setNewState(move);
		handler.notifyCurrentPlayer(new CommandAskCoordMove());
	}

	@Override
	public void applyCommand(CommandAttack attack) {
		match.attack();
		handler.setNewState(attack);
		handler.notifyCurrentPlayer(new CommandAskMove(handler
				.getPossibleActions()));
	}

	@Override
	public void applyCommand(CommandEndTurn endTurn) {
		handler.timerNotNeed();
		handler.setNewState(endTurn);
		handler.notifyCurrentPlayer(new CommandTurnEnded());
		getCurrentPlayer().resetPlayer();
		handler.setNext();
	}

	@Override
	public void applyCommand(CommandCard card) {
		if (fullItemState()) {
			handler.setNewState(card);
			match.useCard(card.getCard());
			match.checkDrawItem();
			handler.notifyCurrentPlayer(new CommandAskMove(handler
					.getPossibleActions()));
		} else if (discardState()) {
			match.removeCard(card.getCard());
			handler.notifyCurrentPlayer(new CommandRemoveCard(card.getCard()));
			match.checkDrawItem();
			handler.setNewState(card);
			handler.notifyCurrentPlayer(new CommandAskMove(handler
					.getPossibleActions()));
		} else if (getCurrentPlayer().hasThisCard(card.getCard()))
			match.useCard(card.getCard());
		else
			handler.notifyCannotUseCard();
	}

	private boolean discardState() {
		return getCurrentPlayer().getState() == ConstantFSM.DISCARD_AFTER_NOISE_ANYWHERE
				|| getCurrentPlayer().getState() == ConstantFSM.DISCARD_BEFORE_END_TURN;
	}

	private boolean fullItemState() {
		return getCurrentPlayer().getState() == ConstantFSM.FULL_ITEMS_AFTER_NOISE_ANYWHERE
				|| getCurrentPlayer().getState() == ConstantFSM.FULL_ITEMS_BEFORE_END_TURN;
	}

	@Override
	public void applyCommand(CommandSendCoordinates sendCoordinates) {
		if (coordBelongToBoard(sendCoordinates.getCoordinates())) {
			int playerState = getCurrentPlayer().getState();
			if (playerState == ConstantFSM.WAIT_MOVE_COORD)
				movePlayer(sendCoordinates.getCoordinates());
			else if (playerState == ConstantFSM.WAIT_NOISE_COORDINATES) {
				handler.setNewState(sendCoordinates);
				noiseCoordinates(sendCoordinates.getCoordinates());
			} else if (playerState == ConstantFSM.WAIT_SPOTLIGHT_COORDINATES) {
				handler.setNewState(sendCoordinates);
				coordinatesForSpotlight(sendCoordinates.getCoordinates());
			}
		} else
			handler.notifyCurrentPlayer(new CommandNotValid());
	}

	private boolean coordBelongToBoard(Coordinates coordinates) {
		return match.getBoard().getBoard().containsKey(coordinates)
				&& match.getBoard().getSector(coordinates).canMoveIn();
	}

	@Override
	public void applyCommand(CommandSolveSector solveSector) {
		Sector sector = match.getBoard().getSector(
				getCurrentPlayer().getCurrentSector());
		match.sectorEffect(sector);
	}

	private void movePlayer(Coordinates coordinates) {
		Player curPlayer = getCurrentPlayer();
		if (match.getBoard().canMove(curPlayer, coordinates)) {
			Sector sector = match.movePlayer(coordinates);
			handler.notifyCurrentPlayer(new CommandNewPosition(curPlayer
					.getCurrentSector(), curPlayer.getPastMoves()));

			if (!sector.hasToDraw())
				curPlayer.setState(ConstantFSM.SOLVED_SECTOR);
			else if (!ConstantBoard.ESCAPEHATCHSECTOR.equals(sector.toString()))
				curPlayer.setState(ConstantFSM.NEED_TO_SOLVE_SECTOR);
			else {
				match.sectorEffect(sector);
				curPlayer.setState(ConstantFSM.END_TURN);
			}
			handler.notifyCurrentPlayer(new CommandAskMove(handler
					.getPossibleActions()));
		} else
			handler.notifyCurrentPlayer(new CommandNotValid());
	}

	private void noiseCoordinates(Coordinates coordinates) {
		handler.notifyAllClients(new CommandNotificationWithCoordinates(
				"Player " + getCurrentPlayer().getPlayerId()
						+ " makes noise in: ", coordinates));
		handler.notifyCurrentPlayer(new CommandAskMove(handler
				.getPossibleActions()));
	}

	private void coordinatesForSpotlight(Coordinates coordinates) {
		Set<Coordinates> sectorsToCheck = new HashSet<Coordinates>(match
				.getBoard().getSectorNeighbors(coordinates));
		sectorsToCheck.add(coordinates);

		String playersInside = null;

		for (Coordinates coord : sectorsToCheck)
			for (Player player : match.getBoard().getSector(coord)
					.getPlayersInside()) {
				playersInside = "Player " + player.getPlayerId() + " is in ";
				handler.notifyAllClients(new CommandNotificationWithCoordinates(
						playersInside, coord));
			}
		if (playersInside == null)
			handler.notifyAllClients(new CommandNotificationWithCoordinates(
					"No one was found in: ", coordinates));
		handler.notifyCurrentPlayer(new CommandAskMove(handler
				.getPossibleActions()));
	}

	@Override
	public void applyCommand(CommandDiscardItem discardItem) {
		handler.setNewState(discardItem);
	}

}
