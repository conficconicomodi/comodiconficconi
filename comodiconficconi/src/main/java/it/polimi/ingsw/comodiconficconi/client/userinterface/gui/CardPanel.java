package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers.CardPanelListener;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class defines the panel in which will be displayed all the item cards
 * drawn by the player. Every card will be associated to a JLabel, and each
 * JLabel has a private class that handles the click from the user and notifies
 * the client controller
 * 
 * @author alessandro
 *
 */
public class CardPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private transient Set<CardLabel> cards;

	private transient List<CardPanelListener> listeners = new ArrayList<CardPanelListener>();

	/**
	 * This constructor sets a Flow layout for the cards and a background color.
	 * It also instantiates the set of JLabels which will contain the cards sent
	 * by server
	 * 
	 */
	public CardPanel() {
		setLayout(new FlowLayout());
		setBackground(Color.WHITE);
		cards = new HashSet<CardLabel>();
	}

	/**
	 * This method takes a card as parameter and builds a new JLabel that will
	 * contain the card and the image relative to the card. The image of the
	 * card will be finally displayed on the game console
	 * 
	 * @param card
	 */
	public void addCard(Card card) {
		ImageIcon cardImage = new ImageIcon(this.getClass().getResource(
				CardFactory.getCardImage(card.toString())));
		Image image = cardImage.getImage();
		Image newImg = image.getScaledInstance(getSize().width / 3 - 10,
				getSize().height - 10, Image.SCALE_SMOOTH);
		cardImage = new ImageIcon(newImg);

		CardLabel newCard = new CardLabel(cardImage, card);
		cards.add(newCard);
		add(newCard, FlowLayout.LEFT);
	}

	/**
	 * Given a card as parameter this methods finds it among the set of JLabels
	 * and deletes it, removing it from the game console
	 * 
	 * @param cardToRemove
	 */
	public void removeCard(Card cardToRemove) {
		Set<CardLabel> allCards = new HashSet<CardLabel>(this.cards);
		for (CardLabel label : allCards)
			if (label.getCard().toString().equals(cardToRemove.toString())) {
				this.cards.remove(label);
				remove(label);
				break;
			}
	}

	public void addListener(CardPanelListener listener) {
		listeners.add(listener);
	}

	/**
	 * This method is necessary to notify a click of a specific JLabel relative
	 * to a card
	 * 
	 * @param card
	 */
	private void notifyCard(CardLabel card) {
		for (CardPanelListener listener : listeners)
			listener.notify(card.getCard());
	}

	/**
	 * This class extends JLabel and stores the card that is added.
	 * 
	 * @author alessandro
	 *
	 */
	private class CardLabel extends JLabel {

		private static final long serialVersionUID = 1L;
		private Card card;

		/**
		 * The constructor of the Card Label class adds a mouse listener to the
		 * JLabel and the relative image
		 * 
		 * @param cardImage
		 * @param card
		 */
		public CardLabel(ImageIcon cardImage, Card card) {
			super(cardImage);
			this.card = card;
			MyMouseListener mouseListener = new MyMouseListener();
			addMouseListener(mouseListener);
		}

		public Card getCard() {
			return card;
		}

		/**
		 * This inner class is a mouse listeners that reacts every time the
		 * label is clicked
		 * 
		 * @author alessandro
		 *
		 */
		private class MyMouseListener extends MouseAdapter {
			@Override
			public void mouseClicked(MouseEvent e) {
				notifyCard((CardLabel) e.getSource());
			}
		}
	}
}
