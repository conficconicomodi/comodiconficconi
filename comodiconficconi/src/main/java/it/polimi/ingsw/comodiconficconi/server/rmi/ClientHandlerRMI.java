package it.polimi.ingsw.comodiconficconi.server.rmi;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.common.RemoteClientHandler;
import it.polimi.ingsw.comodiconficconi.common.RemoteClientInterface;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToClient;
import it.polimi.ingsw.comodiconficconi.common.command.CommandCloseToServer;
import it.polimi.ingsw.comodiconficconi.common.command.CommandNotValid;
import it.polimi.ingsw.comodiconficconi.server.ClientHandler;
import it.polimi.ingsw.comodiconficconi.server.utils.ObserverMatchHandler;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.apache.log4j.Logger;

/**
 * this class let to handle a single client on RMI channel
 * 
 * @author davide
 *
 */
public class ClientHandlerRMI extends ClientHandler implements
		RemoteClientHandler {

	private static final Logger LOGGER = Logger
			.getLogger(it.polimi.ingsw.comodiconficconi.server.rmi.ClientHandlerRMI.class);

	private CommandHandlerServer handler;
	private ServerStarterRMI serverStarter;
	private ObserverMatchHandler allower;
	private RemoteClientInterface client;

	public ClientHandlerRMI(ServerStarterRMI serverStarter, int code) {
		this.serverStarter = serverStarter;
		this.code = code;
		this.code = hashCode();
		closed = false;
		allower = null;
	}

	@Override
	public void run() {

		LOGGER.trace("client handler rmi on!");

	}

	@Override
	public void addAllower(ObserverMatchHandler all) {
		allower = all;
	}

	@Override
	public void addCommandHandler(CommandHandlerServer ch) {
		handler = ch;
	}

	@Override
	public void notifyNewCommand(CommandClientToServer command) {

		if (command instanceof CommandCloseToServer)
			closeByClient();
		else if (allower != null && allower.isAllowed(command, player)
				&& handler != null)
			command.useCommand(handler);
		else if (!allower.isAllowed(command, player) || allower == null) {
			try {
				LOGGER.debug("command not valid");
				sendCommand(new CommandNotValid());
			} catch (IOException e) {
				LOGGER.fatal(e);
				closeByClient();
			}
		}
	}

	@Override
	public void addClient(int port) throws RemoteException {
		try {
			Registry registry = LocateRegistry.getRegistry(port);
			client = (RemoteClientInterface) registry.lookup("Client");
			serverStarter.addClient(this);
		} catch (NotBoundException e) {
			LOGGER.fatal(e);
			serverStarter.removeWaitingClient(this);
		}
	}

	@Override
	public void sendCommand(CommandServerToClient command)
			throws RemoteException {
		LOGGER.debug("write command" + command);
		client.notifyNewCommand(command);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + code;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientHandlerRMI other = (ClientHandlerRMI) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		return true;
	}

	@Override
	public void closeByServer() {
		if (!closed) {
			LOGGER.trace("close by server");
			try {
				client.notifyNewCommand(new CommandCloseToClient());
			} catch (RemoteException e) {
				LOGGER.debug(e);
				closeByClient();
			}
		}
		closed = true;
	}

	@Override
	public void closeByClient() {
		LOGGER.trace("close by client");
		if (!closed) {
			if (allower != null)
				allower.removeClient(this);
			else
				serverStarter.removeWaitingClient(this);
		}
		closed = true;
	}

}
