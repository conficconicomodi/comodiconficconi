package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * This is the teleport card that transport the human from every postion he is
 * in his initial position
 * 
 * @author davide
 *
 */
public class Teleport extends ItemCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5569685603306178901L;

	@Override
	public void applyEffect(Visitor itemVisitor) {
		itemVisitor.visit(this);
	}

	@Override
	public String toString() {
		return ConstantCards.TELEPORT;
	}
}
