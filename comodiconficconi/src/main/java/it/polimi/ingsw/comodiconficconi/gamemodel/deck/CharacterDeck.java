package it.polimi.ingsw.comodiconficconi.gamemodel.deck;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;

/**
 * the class of the character deck
 * 
 * @author davide
 *
 */
public class CharacterDeck extends Deck {

	/**
	 * construct the deck empty or full as the default value of the game, the
	 * deck is already shuffled
	 * 
	 * @param numbPlayers
	 *            : the number of player is used to get the right number of
	 *            character card
	 * @param empty
	 *            : true if you want to build an empty deck false if you want to
	 *            get a deck with numbPlayers cards
	 * 
	 *
	 */
	public CharacterDeck(int numbPlayers, boolean empty) {
		super();
		if (!empty) {
			for (int i = 0; i < numbPlayers / 2; i++)
				cards.add(FactoryCard.getCard(ConstantCards.HUMANCODE));

			for (int i = 0; i < numbPlayers / 2 + numbPlayers % 2; i++)
				cards.add(FactoryCard.getCard(ConstantCards.ALIENCODE));
			this.shuffle();

		}
	}

}
