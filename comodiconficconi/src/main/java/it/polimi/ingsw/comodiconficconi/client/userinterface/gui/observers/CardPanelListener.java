package it.polimi.ingsw.comodiconficconi.client.userinterface.gui.observers;

import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * This interface gives a method to the listeners to notify that a card has been
 * clicked
 * 
 * @author alessandro
 *
 */
public interface CardPanelListener {

	/**
	 * This method will notify the game controller (and all the possible
	 * listeners) that a card label has been clicked
	 * 
	 * @param card
	 */
	void notify(Card card);
}
