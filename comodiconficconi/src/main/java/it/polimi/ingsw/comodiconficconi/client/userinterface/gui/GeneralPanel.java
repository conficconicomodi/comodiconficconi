package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This general panel serves to put any kind of image, especially at start and
 * end of the game. Here will be displayed an image, different for every
 * different moments of the game
 * 
 * @author alessandro
 *
 */
public class GeneralPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GeneralPanel(ImageIcon image) {
		setLayout(new BorderLayout());
		setBackground(Color.BLACK);
		JLabel bckgndImg = new JLabel(image, JLabel.CENTER);

		this.add(bckgndImg, BorderLayout.CENTER);
	}
}
