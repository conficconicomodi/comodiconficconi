package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;

/**
 * This class instantiates a new Secure sector
 * 
 * @author alessandro
 *
 */
public class SecureSector extends Sector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5901224261174523882L;

	@Override
	public boolean canMoveIn() {
		return true;
	}

	@Override
	public String toString() {
		return ConstantBoard.SECURESECTOR;
	}

	@Override
	public boolean hasToDraw() {
		return false;
	}
}