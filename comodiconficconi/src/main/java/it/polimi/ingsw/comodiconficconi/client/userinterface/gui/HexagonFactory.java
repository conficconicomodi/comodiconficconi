package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;

/**
 * This class serves as a constructor of hexagons, each one with his background
 * image
 * 
 * @author alessandro
 *
 */
public class HexagonFactory {

	/**
	 * This is a hidden constructor so that this factory class can never be
	 * instantiated
	 */
	private HexagonFactory() {
	}

	/**
	 * 
	 * Depending on the sector string a new hexagon is created with its own
	 * string containing the path of the image that has to be drawn
	 * 
	 * @param sector
	 * @param coord
	 * @param side
	 * @return Hexagon
	 */
	public static Hexagon getHexagon(String sector, Coordinates coord, int side) {
		switch (sector) {
		case ConstantBoard.DANGEROUSSECTOR:
			return new Hexagon(side, coord, ConstantClient.DANGEROUSSECTOR);
		case ConstantBoard.SECURESECTOR:
			return new Hexagon(side, coord, ConstantClient.SECURESECTOR);
		case ConstantBoard.ESCAPEHATCHSECTOR:
			return new Hexagon(side, coord, ConstantClient.HATCHSECTOR);
		case ConstantBoard.ALIENSECTOR:
			return new Hexagon(side, coord, ConstantClient.ALIENSECTOR);
		case ConstantBoard.HUMANSECTOR:
			return new Hexagon(side, coord, ConstantClient.HUMANSECTOR);
		default:
			return null;
		}
	}
}
