package it.polimi.ingsw.comodiconficconi.client.userinterface.gui;

import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * This class contains two main panels: 1) The totality of the movements that a
 * player made in the match 2) the logo of the game
 * 
 * @author alessandro
 *
 */
public class PlayerMoves extends JPanel {

	private static final long serialVersionUID = 1L;

	private Moves moves;
	private Logo logo;

	/**
	 * This constructor sets the panel with the past moves of the player and the
	 * logo of the game
	 * 
	 * @param dimension
	 */
	public PlayerMoves(Dimension dimension) {
		setLayout(new BorderLayout());
		setSize(dimension);
		setPreferredSize(dimension);

		moves = new Moves(new Dimension(getWidth() * 2 / 3, getHeight()));

		logo = new Logo();
		logo.setSize(new Dimension(getWidth() * 1 / 3, getHeight()));
		logo.setPreferredSize(new Dimension(getWidth() * 1 / 3, getHeight()));
		logo.setBackgroundImage();

		add(logo, BorderLayout.EAST);
		add(moves, BorderLayout.WEST);
	}

	public List<JLabel> getPastMoves() {
		return moves.getPastMoves();
	}

	/**
	 * This inner class is the panel containing all the past moves
	 * 
	 * @author alessandro
	 *
	 */
	private class Moves extends JPanel {

		private static final long serialVersionUID = 1L;
		private static final int TOTALMOVES = 39;
		private transient List<JLabel> pastMoves = new ArrayList<JLabel>();

		public Moves(Dimension dimension) {
			setLayout(new GridLayout(3, 13));
			setBorder(new EmptyBorder(2, 2, 2, 2));
			setPreferredSize(dimension);
			setSize(dimension);
			setBackground(Color.WHITE);
			for (int i = 1; i < TOTALMOVES + 1; i++)
				if (i < 10)
					pastMoves.add(new JLabel("0" + i + "[   ].", JLabel.LEFT));
				else
					pastMoves.add(new JLabel(i + "[   ].", JLabel.LEFT));

			for (JLabel label : pastMoves) {
				label.setFont(new Font("Consolas", Font.PLAIN, 11));
				label.setSize(new Dimension((int) (getWidth() / 13),
						getHeight() / 3));
				label.setPreferredSize(new Dimension((int) (getWidth() / 13),
						getHeight() / 3));
				add(label);
			}
		}

		public List<JLabel> getPastMoves() {
			return pastMoves;
		}
	}

	/**
	 * This class is a panel containing the logo of the game
	 * 
	 * @author alessandro
	 *
	 */
	private class Logo extends JPanel {

		private static final long serialVersionUID = 1L;
		private ImageIcon background;

		public Logo() {
			setBackground(Color.WHITE);
		}

		public void setBackgroundImage() {
			background = new ImageIcon(this.getClass().getResource(
					ConstantClient.LOGO));
			Image image = background.getImage();
			Image newImg = image.getScaledInstance(getWidth() / 2,
					getHeight() - 5, Image.SCALE_SMOOTH);
			background = new ImageIcon(newImg);
			JLabel bckgndImg = new JLabel(background, JLabel.CENTER);
			add(bckgndImg);
		}
	}
}