package it.polimi.ingsw.comodiconficconi.gamemodel.board;

/**
 * This class is an implementation of the factory pattern that instantiates all
 * the possible kind of sectors
 * 
 * @author alessandro
 *
 */
public class SectorFactory {
	public static final int SECURE = 0;
	public static final int DANGEROUS = 1;
	public static final int ALIEN = 3;
	public static final int HUMAN = 4;
	public static final int HATCH = 5;

	/** This private constructor is necessary to hide the implicit one */
	private SectorFactory() {
	}

	/**
	 * This is the method that returns all the differentkind of sectors
	 * @param type
	 *            of the Sector that has to be created.
	 * @return returns the sector that has been created.
	 * 
	 */
	public static Sector getSector(int type) {
		if (type == SECURE || type == DANGEROUS)
			return subFactoryNormalSector(type);
		else if (type == HUMAN || type == ALIEN)
			return subFactoryPlayerSector(type);
		else if (type == HATCH)
			return new EscapeHatchSector();
		else
			return new EmptySector();

	}

	private static Sector subFactoryPlayerSector(int type) {
		if (type == ALIEN)
			return new AlienSector();
		else
			return new HumanSector();
	}

	private static Sector subFactoryNormalSector(int type) {
		if (type == SECURE)
			return new SecureSector();
		else
			return new DangerousSector();
	}
}
