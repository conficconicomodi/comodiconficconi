package it.polimi.ingsw.comodiconficconi.gamemodel.card;

/**
 * This class represent the abstract object of the item card of the game
 * 
 * @author davide
 *
 */
public abstract class ItemCard extends Card {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3038073130629355296L;

}
