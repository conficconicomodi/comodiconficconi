package it.polimi.ingsw.comodiconficconi.client.userinterface.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import it.polimi.ingsw.comodiconficconi.client.ClientUtils;
import it.polimi.ingsw.comodiconficconi.client.controller.ClientController;
import it.polimi.ingsw.comodiconficconi.client.userinterface.UserInterface;
import it.polimi.ingsw.comodiconficconi.constants.ConstantClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Coordinates;
import it.polimi.ingsw.comodiconficconi.gamemodel.board.Sector;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;
import it.polimi.ingsw.comodiconficconi.gamemodel.player.Player;

/**
 * This class handles all the commands that are written in input from the user
 * and depending on the state of the reader (the state determines what input can
 * give the user and if the input is not allowed in that particular state)
 * actions can be sent to server through the chosen network interface
 * 
 * @author alessandro
 *
 */
public class CommandLineInterface implements UserInterface, InputListener {

	private static final Logger LOGGER = Logger
			.getLogger(CommandLineInterface.class);
	private ClientController gameController;
	private List<Card> items;
	private BufferedReader inKeyboard = new BufferedReader(
			new InputStreamReader(System.in));
	private Reader reader;
	private Thread readerThread;
	private int readerState;

	/**
	 * This constructor instantiates the list of cards owned by the player, a
	 * new keyboard reader which will be run by a thread, sets the listener for
	 * the reader and displays a welcome message
	 * 
	 * @param gameController
	 */
	public CommandLineInterface(ClientController gameController) {
		this.gameController = gameController;
		items = new ArrayList<Card>();
		reader = new Reader(inKeyboard);
		readerThread = new Thread(reader);
		readerThread.start();
		reader.setListener(this);
		welcomeMessage();
	}

	/**
	 * This method serves to display a welcome message to the user
	 */
	public void welcomeMessage() {
		Printer.print("Welcome to 'Escape from the Aliens in Outer Space'\nthe match will start in a few moments\n");
	}

	@Override
	public void initializeMatch(Map<Coordinates, Sector> board, Player player,
			String mapName) {
		Printer.print("The game is starting and you are:\n "
				+ player.toString().toUpperCase() + "\n");
		Printer.print("You are currently playing in " + mapName
				+ " map, and your current sector is: ");
		String position = ClientUtils.coordToString(player.getCurrentSector());
		Printer.print(position + "\n");
		Printer.println();
	}

	@Override
	public void startTurn() {
		Printer.print("----------------------------------------------------");
		Printer.println();
		Printer.println("It is your turn, choose your next action");
	}

	@Override
	public void commandNotValid() {
		Printer.println("Command not valid");
	}

	@Override
	public void removeCard(Card card) {
		Card itemToRemove = null;
		for (Card item : items)
			if (card.toString().equals(item.toString()))
				itemToRemove = item;
		items.remove(itemToRemove);
		Printer.println(card.toString() + " has been removed");
	}

	@Override
	public void addCard(Card card) {
		Printer.println("You have recieved a new card: "
				+ card.toString().toUpperCase());
		items.add(card);
	}

	@Override
	public void askNoiseCoordinates() {
		Printer.println("Choose a fake noise position");
		readerState = ConstantClient.SEND_COORDINATES_STATE;
	}

	@Override
	public void askMoveCoordinates() {
		Printer.println("Insert the coordinates you want to move into");
		readerState = ConstantClient.SEND_COORDINATES_STATE;
	}

	@Override
	public void askSpotlightCoordinates() {
		Printer.println("Choose a coordinate where to make light and see your enemies");
		readerState = ConstantClient.SEND_COORDINATES_STATE_SPOTLIGHT;
	}

	@Override
	public void notifyToAll(String notification) {
		Printer.println(notification);
	}

	@Override
	public void askMove(List<String> actions) {
		if (!actions.isEmpty()) {
			Printer.println("*********************************");
			Printer.println("You can do the following actions:");
			for (String action : actions)
				Printer.print(action + " ");
			Printer.println();
			Printer.println("*********************************");
			readerState = ConstantClient.SEND_COMMAND_STATE;
		} else
			readerState = ConstantClient.SEND_COORDINATES_STATE;
	}

	@Override
	public void turnEnded() {
		Printer.println("Your turn has ended");
		Printer.println("----------------------------------------------------");
		Printer.println();
		readerState = ConstantClient.NULL;
	}

	@Override
	public void setMovePlayer(Coordinates newPosition) {
		Printer.println("You moved in: "
				+ ClientUtils.coordToString(newPosition));
	}

	@Override
	public void setMovePlayer(Coordinates coord, List<Coordinates> pastMoves) {
		Printer.println("you are in: " + ClientUtils.coordToString(coord));
	}

	@Override
	public void death() {
		Printer.print("You have been killed\nPress any key to exit the game");
		readerState = ConstantClient.END_GAME;
	}

	@Override
	public void win() {
		Printer.print("You have won\nPress any key to exit the game");
		readerState = ConstantClient.END_GAME;
	}

	@Override
	public void lose() {
		Printer.print("You have lost\nPress any key to exit the game");
		readerState = ConstantClient.END_GAME;
	}

	@Override
	public void disconnection() {
		Printer.print("You have been disconnected from the game\nPress any key to exit the game");
		readerState = ConstantClient.END_GAME;
	}

	@Override
	public void serverOutOfOrder() {
		Printer.println("Server out of order, we apologize for the inconvenience, try again later\nPress any key to exit the game");
		readerState = ConstantClient.END_GAME;
	}

	@Override
	public void notify(String string) {
		LOGGER.debug("Switching through commands");
		switch (readerState) {
		case ConstantClient.SEND_COMMAND_STATE:
			commandHandler(string);
			break;
		case ConstantClient.SEND_COORDINATES_STATE:
			readerState = ConstantClient.SEND_COORDINATES_STATE;
			coordinatesHandler(string);
			break;
		case ConstantClient.SEND_COORDINATES_STATE_SPOTLIGHT:
			coordinatesHandler(string);
			break;
		case ConstantClient.SEND_CARD_STATE:
			cardHandler(string);
			break;
		case ConstantClient.END_GAME:
			endGameHandler();
			break;
		case ConstantClient.NULL:
			Printer.println("Command not recognized");
			break;
		default:
			Printer.println("Command not recognized");
			break;
		}
	}

	/**
	 * This method serves to display all the items that a player owns
	 */
	private void displayCards() {
		if (!items.isEmpty()) {
			Printer.println("you can use the following cards: ");
			for (Card item : items)
				Printer.println(item.toString().toUpperCase());
		} else {
			Printer.println("You don't have any items to use");
			readerState = ConstantClient.SEND_COMMAND_STATE;
		}
	}

	/**
	 * This method serves to handle the input of the item that the user wants to
	 * use. If the input is incorrect or the user do not own a particular card
	 * no card will be used.
	 * 
	 * @param input
	 */
	private void cardHandler(String input) {
		for (Card item : items) {
			if (item.toString().equalsIgnoreCase(input)) {
				readerState = ConstantClient.SEND_COMMAND_STATE;
				gameController.notify(item);
				return;
			}
		}
		Printer.println("wrong input");
	}

	/**
	 * This method serves to handle the coordinates input. If the coordinates
	 * written are wrong they are not sent to server and a message of wrong
	 * input is displayed on the console
	 * 
	 * @param input
	 */
	private void coordinatesHandler(String input) {
		if (input.matches("[a-zA-Z][0-9][0-9]"))
			gameController
					.notify(ClientUtils.stringToCoord(input.toUpperCase()));
		else
			Printer.println("wrong coordinates");
	}

	/**
	 * This method serves to handle the ending of the game. If the user inserts
	 * any key the program shuts down
	 */
	private void endGameHandler() {
		Printer.println("Thanks for playing!");
		System.exit(0);
	}

	/**
	 * This method serves to handle the commands in input from the user. If the
	 * commands are wrong or are not present in the current reader state a
	 * message of wrong input is displayed to console
	 * 
	 * @param string
	 */
	private void commandHandler(String string) {
		LOGGER.debug("Command State");
		switch (string.toLowerCase()) {
		case ConstantClient.MOVE:
			gameController.notifyMove();
			break;
		case ConstantClient.ATTACK:
			gameController.notifyAttack();
			break;
		case ConstantClient.PASS:
			gameController.notifyPassTurn();
			break;
		case ConstantClient.CARD:
			readerState = ConstantClient.SEND_CARD_STATE;
			displayCards();
			break;
		case ConstantClient.DRAW:
			gameController.notifyDrawCard();
			break;
		case ConstantClient.DISCARD:
			gameController.notifyDiscardItem();
			readerState = ConstantClient.SEND_CARD_STATE;
			displayCards();
			break;
		default:
			Printer.println("wrong input");
		}
	}
}
