package it.polimi.ingsw.comodiconficconi.gamemodel.deck;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.FactoryCard;

/**
 * the class of the hatch deck
 * 
 * @author davide
 *
 */
public class HatchDeck extends Deck {

	/**
	 * construct the deck empty or full as the default value of the game, the
	 * deck is already shuffled
	 * 
	 * @param empty
	 *            : true if you want to build an empty deck false if you want to
	 *            get a default built deck
	 */
	public HatchDeck(boolean empty) {
		super();
		if (!empty) {
			for (int i = 0; i < ConstantCards.MAXOPENHATCHCARD; i++)
				cards.add(FactoryCard.getCard(ConstantCards.OPENHATCHCODE));

			for (int i = 0; i < ConstantCards.MAXBLOCKEDHATCHCARD; i++)
				cards.add(FactoryCard.getCard(ConstantCards.BLOCKEDHATCHCODE));

			this.shuffle();
		}
	}

}
