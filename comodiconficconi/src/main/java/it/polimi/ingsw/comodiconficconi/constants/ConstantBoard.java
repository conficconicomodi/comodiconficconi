package it.polimi.ingsw.comodiconficconi.constants;

public class ConstantBoard {

	public static final String DANGEROUSSECTOR = "dangeroussector";
	public static final String SECURESECTOR = "securesector";
	public static final String EMPTYSECTOR = "emptysector";
	public static final String ESCAPEHATCHSECTOR = "hatchsector";
	public static final String ALIENSECTOR = "aliensector";
	public static final String HUMANSECTOR = "humansector";

	public static final String GALILEI = "galilei";
	public static final String FERMI = "fermi";
	public static final String GALVANI = "galvani";

	private ConstantBoard() {
	}
}
