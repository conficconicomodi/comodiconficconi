package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command thats * 
 * @author davide
 *
 */
public class CommandDiscardItem extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6512976524803390163L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {
		commandHandlerServer.applyCommand(this);
	}

}
