package it.polimi.ingsw.comodiconficconi.gamemodel.board;

import it.polimi.ingsw.comodiconficconi.constants.ConstantBoard;

/**
 * 
 * This class is used to instantiate a Human base sector
 * 
 * @author alessandro
 *
 */
public class HumanSector extends BaseSector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2211308618899261566L;

	@Override
	public String toString() {
		return ConstantBoard.HUMANSECTOR;
	}
}
