package it.polimi.ingsw.comodiconficconi.client.network;

import it.polimi.ingsw.comodiconficconi.client.network.utils.SubjectCommandToClient;
import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * This is the network interface and through this client can communicate with
 * server independently by the choice of technology
 * 
 * @author davide
 *
 */
public interface NetworkInterface extends SubjectCommandToClient {

	/**
	 * this method set on the connection, create all object needed to handle the
	 * connection
	 * 
	 * @throws Exception
	 *             : exception throws on connection failure
	 */
	void connect() throws Exception;

	/**
	 * This method send a command from Client to Server through the chosen
	 * channel
	 * 
	 * @param command
	 *            : the command you want to sent
	 * @throws Exception
	 *             : exception thrown when there are some problems with server
	 *             comunication
	 */
	void sendCommand(CommandClientToServer command) throws Exception;

	/**
	 * method that used to notify a new command received from the server to the
	 * command handler that has to translate into effective action
	 * 
	 * @param command
	 *            : the command received and notified to the handler
	 */
	void notifyNewCommand(CommandServerToClient command);

	/**
	 * method that has to close connection and release all resources used to
	 * communicate
	 */
	void close();

}
