package it.polimi.ingsw.comodiconficconi.gamemodel.player;

import it.polimi.ingsw.comodiconficconi.constants.ConstantPlayer;

/**
 * This class creates the player depending on which card has been drawn from the
 * character deck
 * 
 * @author alessandro
 *
 */
public class PlayerFactory {

	/** This private constructor is necessary to hide the implicit one */
	private PlayerFactory() {
	}

	/**
	 * This method returns the dynamic type of player that has been created
	 * 
	 * @param type
	 * @param id
	 * @return
	 */
	public static Player getPlayer(String type, int id) {
		switch (type) {
		case ConstantPlayer.HUMAN:
			return new HumanPlayer(id);
		case ConstantPlayer.ALIEN:
			return new AlienPlayer(id);
		default:
			return null;
		}
	}
}
