package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandClientToServer;
import it.polimi.ingsw.comodiconficconi.common.CommandHandlerServer;

/**
 * command to report the end of the turn
 * 
 * @author davide
 *
 */
public class CommandEndTurn extends CommandClientToServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1591128126103015540L;

	@Override
	public void useCommand(CommandHandlerServer commandHandlerServer) {

		commandHandlerServer.applyCommand(this);
	}

}
