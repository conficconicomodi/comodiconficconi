package it.polimi.ingsw.comodiconficconi.constants;

public class ConstantPlayer {
	
	public static final String HUMAN = "human";
	public static final String ALIEN = "alien";
	
	/** Max number of item cards */
	public static final int MAXITEM = 3;
	
	public static final int HUMANSTEPS = 1;
	public static final int ALIENSTEPS = 2;
	public static final int HUNGRYALIENSTEPS = 3;
	
	/** Steps with adrenaline */
	public static final int ADRENALINESTEPS = 2;

	private ConstantPlayer(){
	}
}
