package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command that notify the need of coordinates for noise any where
 * 
 * @author davide
 *
 */
public class CommandAskCoordNoiseAnywhere extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3054958344989576817L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
