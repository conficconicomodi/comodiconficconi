package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;
import it.polimi.ingsw.comodiconficconi.gamemodel.card.Card;

/**
 * command to notify the need to add a card
 * 
 * @author davide
 *
 */
public class CommandAddCard extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7568329248100320805L;

	private Card item;

	/**
	 * 
	 * @param item
	 *            : the card that has to be added
	 */
	public CommandAddCard(Card item) {
		this.item = item;
	}

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

	public Card getItem() {
		return item;
	}
}
