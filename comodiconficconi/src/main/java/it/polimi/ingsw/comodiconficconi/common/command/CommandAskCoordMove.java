package it.polimi.ingsw.comodiconficconi.common.command;

import it.polimi.ingsw.comodiconficconi.common.CommandHandlerClient;
import it.polimi.ingsw.comodiconficconi.common.CommandServerToClient;

/**
 * command that notify the need of coordinates for a move
 * 
 * @author davide
 *
 */
public class CommandAskCoordMove extends CommandServerToClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8714191976950243184L;

	@Override
	public void useCommand(CommandHandlerClient commandHandler) {
		commandHandler.applyCommand(this);
	}

}
