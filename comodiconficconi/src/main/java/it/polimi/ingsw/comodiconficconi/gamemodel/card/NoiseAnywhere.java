package it.polimi.ingsw.comodiconficconi.gamemodel.card;

import it.polimi.ingsw.comodiconficconi.constants.ConstantCards;
import it.polimi.ingsw.comodiconficconi.gamemodel.actions.Visitor;

/**
 * this class let the player to lie where he is
 * 
 * @author davide
 *
 */
public class NoiseAnywhere extends SectorCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2956097757981214256L;
	private boolean object;

	protected NoiseAnywhere(boolean obj) {
		object = obj;
	}

	protected boolean isWithObject() {
		return object;
	}

	@Override
	public void applyEffect(Visitor visitor) {
		visitor.visit(this, object);
	}

	@Override
	public String toString() {
		return ConstantCards.NOISEANYWHERE;
	}
}
